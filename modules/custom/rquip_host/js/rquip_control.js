/*
 * Live sites running jQuery 1.4 with no .on() function
 * Hacky Fudge Hack
 */
(function ($) {
    if (!$.isFunction($.fn.on)) {
        jQuery.fn.on = function (types, data, fn) {
            return this.bind(types, null, data, fn);
        }
    }
})(jQuery);
/*
 * end Hacky Fudge Hack
 */

/*
 * Method to use .on('show') or .on('hide')
 */
(function ($) {
    $.each(['show', 'hide'], function (i, ev) {
        var el = $.fn[ev];
        $.fn[ev] = function () {
            this.trigger(ev);
            return el.apply(this, arguments);
        };
    });
})(jQuery);
/**
 * Created by bruce on 2014/01/22.
 */
(function ($) {
    $(document).ready(function () {

        $('#rquip-admin-account-form #edit-type-id input[id="edit-type_id"]').click(function () {
            if ($(this).val() == 2) {
                $('#rquip-admin-account-form .form-item-engine-id').hide();
                $('#rquip-admin-account-form .form-item-engine-id input').attr('checked', false);
                $('.form-item-account-name label').html('Law Firm <span class="form-required" title="This field is required.">*</span>');
            }
            else {
                $('#rquip-admin-account-form .form-item-engine-id').show();
                $('.form-item-account-name label').html('Name (url) <span class="form-required" title="This field is required.">*</span>');
            }
        });
        $('#edit-disbursement').click(function () {
            if ($(this).is(':checked')) {
                alert('NOTE: All disbursement money values are stored INCLUDING VAT');
                $('.form-item-default-price-exvat label').html('Default inclVAT price');
            }
            else {
                alert('NOTE: All non-disbursement money values are stored EXCLUDING VAT');
                $('.form-item-default-price-exvat label').html('Default exVAT price');
            }
        });
        $('#rquip-admin-item-form #edit-is-bracketed').click(function () {
            if ($(this).is(':checked')) {
                $('.form-item-bracket').show();
            }
            else {
                $('.form-item-bracket').hide();
            }
        });
        $('.show-brackets').hover(function () {
            var id = $(this).attr('id').split('-');
            var brackets_id = '#brackets-' + id[2];
            $(brackets_id).show();
        }, function () {
            var id = $(this).attr('id').split('-');
            var brackets_id = '#brackets-' + id[2];
            $(brackets_id).hide();
        });
        $('.show-calculations').hover(function () {
            var id = $(this).attr('id').split('-');
            var calculations_id = '#calculations-' + id[2];
            $(calculations_id).show();
        }, function () {
            var id = $(this).attr('id').split('-');
            var calculations_id = '#calculations-' + id[2];
            $(calculations_id).hide();
        });
        if ($('#edit-item-input').is(':checked')) {
            $('#edit-input').show();
            $('#edit-calc').hide();
            $('#edit-title').css({'backgroundColor': '#fdd'});
            $('#edit-type').css({'backgroundColor': '#fdd'});
        }
        else {
            $('#edit-input').hide();
            $('#edit-calc').show();
            $('#edit-title').css({'backgroundColor': 'inherit'});
            $('#edit-type').css({'backgroundColor': 'inherit'});
        }

        $('#edit-item-input').change(function () {
            if ($(this).is(':checked')) {
                $('#edit-input').show();
                $('#edit-calc').hide();
                $('#edit-title').css({'backgroundColor': '#fdd'});
                $('#edit-type').css({'backgroundColor': '#fdd'});
            }
            else {
                $('#edit-input').hide();
                $('#edit-calc').show();
                $('#edit-title').css({'backgroundColor': 'inherit'});
                $('#edit-type').css({'backgroundColor': 'inherit'});
            }
        });
        ////    get a quote

        $('.form-layout-group.hide-able').hide();
        /*
         * @nbconfig
         */
        switch ($('#rquip-instruct-form input[name=service_id]').val()) {
            case '2':
                $('#form-layout-sale').show();
                break;
            case '3':
                $('#form-layout-purchase').show();
                break;
            case '4':
                $('#form-layout-remortgage').show();
                break;
            case '5':
                $('#form-layout-toe').show();
                break;
            case '6':
                $('#form-layout-sale').show();
                $('#form-layout-purchase').show();
                break;
            case '7':
                $('#form-layout-hbs').show();
                break;
            case '8':
                $('#form-layout-bsu').show();
                break;
            case '9':
                $('#form-layout-pvr').show();
                break;
        }

        /*
         * Catch all method to ensure that form elements are enabled/disabled
         * appropriately
         */
        $('.form-layout-group').on('show', function () {
            $(this).find(':input').removeAttr('disabled');
        });
        $('.form-layout-group').on('hide', function () {
            $(this).find(':input').attr('disabled', 'disabled');
        });


        /*
         * @nbconfig
         */
        $('.form-item-service-id .form-radio').click(function () {
            $('.form-layout-group.hide-able').hide();
            $(".form-layout-group.hide-able :input").attr('disabled', 'disabled');
            var service = $(this).val();
            switch (service) {
                case '2':
                    $('#form-layout-sale').show();
                    $("#form-layout-sale :input").removeAttr('disabled');
                    break;
                case '3':
                    $('#form-layout-purchase').show();
                    $("#form-layout-purchase :input").removeAttr('disabled');
                    break;
                case '4':
                    $('#form-layout-remortgage').show();
                    $("#form-layout-remortgage :input").removeAttr('disabled');
                    break;
                case '5':
                    $('#form-layout-toe').show();
                    $("#form-layout-toe :input").removeAttr('disabled');
                    break;
                case '6':
                    $('#form-layout-sale').show();
                    $('#form-layout-purchase').show();
                    $("#form-layout-sale :input").removeAttr('disabled');
                    $("#form-layout-purchase :input").removeAttr('disabled');
                    break;
                case '7':
                    $('#form-layout-hbs').show();
                    $("#form-layout-hbs :input").removeAttr('disabled');
                    break;
                case '8':
                    $('#form-layout-bsu').show();
                    $("#form-layout-bsu :input").removeAttr('disabled');
                    break;
                case '9':
                    $('#form-layout-pvr').show();
                    $("#form-layout-pvr :input").removeAttr('disabled');
                    break;
                case '11':
                    $('#form-layout-lx1').show();
                    $("#form-layout-lx1 :input").removeAttr('disabled');
                    $('#form-layout-onsite-desktop').show();
                    $('[name=lx_onsite_desktop]').removeAttr('disabled');
                    break;
                case '12':
                    $('#form-layout-lx2').show();
                    $("#form-layout-lx2 :input").removeAttr('disabled');
                    $('#form-layout-onsite-desktop').hide();
                    $('[name=lx_onsite_desktop]').attr('disabled', 'disabled');
                    break;
                case '13':
                    $('#form-layout-lx3').show();
                    $("#form-layout-lx3 :input").removeAttr('disabled');
                    $('#form-layout-onsite-desktop').hide();
                    $('[name=lx_onsite_desktop]').attr('disabled', 'disabled');
                    break;
                case '14':
                    $('#form-layout-lx3').show();
                    $("#form-layout-lx3 :input").removeAttr('disabled');
                    $('#form-layout-onsite-desktop').show();
                    $('[name=lx_onsite_desktop]').removeAttr('disabled');
                    break;
                case '15':
                    $('#form-layout-sale').show();
                    $("#form-layout-sale :input").removeAttr('disabled');
                    break;
                case '16':
                    $('#form-layout-purchase').show();
                    $("#form-layout-purchase :input").removeAttr('disabled');
                    break;
                case '17':
                    $('#form-layout-lease-creation').show();
                    $("#form-layout-lease-creation :input").removeAttr('disabled');
                    break;
                case '18':
                    $('#form-layout-lease-transfer').show();
                    $("#form-layout-lease-transfer :input").removeAttr('disabled');
                    break;
            }
        });
        $('.form-item-toe-mortgage .form-radio').click(function () {
            var service = $(this).val();
            switch (service) {
                case 'Yes':
                    $('#wrapper-toe-many-mortgages').show();
                    $('#wrapper-toe-legal-work').show();
                    if ($.isFunction($.fn.prop)) {
                        $('#edit-toe-legal-work-yes').prop('checked', true);
                    } else {
                        $('#edit-toe-legal-work-yes').attr('checked', 'checked');
                    }
                    $('#edit-toe-legal-work-no').removeAttr('checked');
                    break;
                case 'No':
                    $('#wrapper-toe-many-mortgages').hide();
                    $('#wrapper-toe-legal-work').hide();
                    if ($.isFunction($.fn.prop)) {
                        $('#edit-toe-legal-work-no').prop('checked', true);
                    } else {
                        $('#edit-toe-legal-work-no').attr('checked', 'checked');
                    }
                    $('#edit-toe-legal-work-yes').removeAttr('checked');
                    break;
            }
            $('#form-layout-toe').find(':input').removeAttr('disabled');
        });
        $('#instructing-toggle-quote').click(function () {
            if ($('#instructing-quote #quote-detail').is(':visible')) {
                $('#instructing-quote #quote-detail').hide();
                $(this).html('Show Full Quote');
            }
            else {
                $('#instructing-quote #quote-detail').show();
                $(this).html('Hide Quote Detail');
            }
        });
        var $_GET = {};
        document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
            function decode(s) {
                return decodeURIComponent(s.split("+").join(" "));
            }

            $_GET[decode(arguments[1])] = decode(arguments[2]);
        });
        /*
         * @nbconfig
         */
        if (array_key_exists('service_id', $_GET)) {
            switch ($_GET['service_id']) {
                case '2':
                    $('#edit-service-id-2').attr('checked', 'checked');
                    $('#form-layout-sale').show();
                    $('#edit-sco-price').val($_GET['sco_price']);
                    if ($_GET['sco_mortgage'] == 'Yes')
                        $('#edit-sco-mortgage-yes').attr('checked', 'checked');
                    if ($_GET['sco_mortgage'] == 'No')
                        $('#edit-sco-mortgage-no').attr('checked', 'checked');
                    if ($_GET['sco_tenure'] == 'Freehold')
                        $('#edit-sco-tenure-freehold').attr('checked', 'checked');
                    if ($_GET['sco_tenure'] == 'Leasehold')
                        $('#edit-sco-tenure-leasehold').attr('checked', 'checked');
                    if ($_GET['sco_tenure'] == 'Share of Freehold')
                        $('#edit-sco-tenure-share-of-freehold').attr('checked', 'checked');
                    break;
                case '3':
                    $('#edit-service-id-3').attr('checked', 'checked');
                    $('#form-layout-purchase').show();
                    $('#edit-pco-price').val($_GET['pco_price']);
                    if ($_GET['pco_mortgage'] == 'Yes')
                        $('#edit-pco-mortgage-yes').attr('checked', 'checked');
                    if ($_GET['pco_mortgage'] == 'No')
                        $('#edit-pco-mortgage-no').attr('checked', 'checked');
                    if ($_GET['pco_help'] == 'Yes')
                        $('#edit-pco-help-yes').attr('checked', 'checked');
                    if ($_GET['pco_help'] == 'No')
                        $('#edit-pco-help-no').attr('checked', 'checked');
                    if ($_GET['pco_tenure'] == 'Freehold')
                        $('#edit-pco-tenure-freehold').attr('checked', 'checked');
                    if ($_GET['pco_tenure'] == 'Leasehold')
                        $('#edit-pco-tenure-leasehold').attr('checked', 'checked');
                    if ($_GET['pco_tenure'] == 'Share of Freehold')
                        $('#edit-pco-tenure-share-of-freehold').attr('checked', 'checked');
                    break;
                case '4':
                    $('#edit-service-id-4').attr('checked', 'checked');
                    $('#form-layout-remortgage').show();
                    $('#edit-rem-price').val($_GET['rem_price']);
                    $('#edit-rem-borrowing').val($_GET['rem_borrowing']);
                    break;
                case '5':
                    $('#edit-service-id-5').attr('checked', 'checked');
                    $('#form-layout-toe').show();
                    $('#edit-toe-price').val($_GET['toe_price']);
                    $('#edit-toe-money').val($_GET['toe_money']);
                    if ($_GET['toe_mortgage'] == 'Yes')
                        $('#edit-toe-mortgage-yes').attr('checked', 'checked');
                    if ($_GET['toe_mortgage'] == 'No')
                        $('#edit-toe-mortgage-no').attr('checked', 'checked');
                    if ($_GET['toe_many_mortgages'] == '1')
                        $('#edit-toe-many-mortgages-1').attr('checked', 'checked');
                    if ($_GET['toe_many_mortgages'] == '2')
                        $('#edit-toe-many-mortgages-2').attr('checked', 'checked');
                    if ($_GET['toe_many_mortgages'] == '3')
                        $('#edit-toe-many-mortgages-3').attr('checked', 'checked');
                    if ($_GET['toe_legal_work'] == 'Yes')
                        $('#edit-toe-legal-work-yes').attr('checked', 'checked');
                    if ($_GET['toe_legal_work'] == 'No')
                        $('#edit-toe-legal-work-no').attr('checked', 'checked');
                    break;
                case '6':
                    $('#edit-service-id-6').attr('checked', 'checked');
                    $('#form-layout-sale').show();
                    $('#edit-sco-price').val($_GET['sco_price']);
                    if ($_GET['sco_mortgage'] == 'Yes')
                        $('#edit-sco-mortgage-yes').attr('checked', 'checked');
                    if ($_GET['sco_mortgage'] == 'No')
                        $('#edit-sco-mortgage-no').attr('checked', 'checked');
                    if ($_GET['sco_tenure'] == 'Freehold')
                        $('#edit-sco-tenure-freehold').attr('checked', 'checked');
                    if ($_GET['sco_tenure'] == 'Leasehold')
                        $('#edit-sco-tenure-leasehold').attr('checked', 'checked');
                    if ($_GET['sco_tenure'] == 'Share of Freehold')
                        $('#edit-sco-tenure-share-of-freehold').attr('checked', 'checked');
                    $('#form-layout-purchase').show();
                    $('#edit-pco-price').val($_GET['pco_price']);
                    if ($_GET['pco_mortgage'] == 'Yes')
                        $('#edit-pco-mortgage-yes').attr('checked', 'checked');
                    if ($_GET['pco_mortgage'] == 'No')
                        $('#edit-pco-mortgage-no').attr('checked', 'checked');
                    if ($_GET['pco_help'] == 'Yes')
                        $('#edit-pco-help-yes').attr('checked', 'checked');
                    if ($_GET['pco_help'] == 'No')
                        $('#edit-pco-help-no').attr('checked', 'checked');
                    if ($_GET['pco_tenure'] == 'Freehold')
                        $('#edit-pco-tenure-freehold').attr('checked', 'checked');
                    if ($_GET['pco_tenure'] == 'Leasehold')
                        $('#edit-pco-tenure-leasehold').attr('checked', 'checked');
                    if ($_GET['pco_tenure'] == 'Share of Freehold')
                        $('#edit-pco-tenure-share-of-freehold').attr('checked', 'checked');
                    break;
                case '7':
                    $('#edit-service-id-7').attr('checked', 'checked');
                    $('#form-layout-hbs').show();
                    $('#edit-hbs-postcode').val($_GET['hbs_postcode']);
                    $('#edit-hbs-property-value').val($_GET['hbs_property_value']);
                    break;
                case '8':
                    $('#edit-service-id-8').attr('checked', 'checked');
                    $('#form-layout-bsu').show();
                    $('#edit-bsu-postcode').val($_GET['bsu_postcode']);
                    $('#edit-bsu-property-value').val($_GET['bsu_property_value']);
                    break;
                case '9':
                    $('#edit-service-id-9').attr('checked', 'checked');
                    $('#form-layout-pvr').show();
                    $('#edit-pvr-postcode').val($_GET['pvr_postcode']);
                    $('#edit-pvr-property-value').val($_GET['pvr_property_value']);
                    break;
                case '11':
                    $('#edit-service-id-11').attr('checked', 'checked');
                    $('[name=lx_ground_rent]').val($_GET['lx_ground_rent']);
                    $('[name=lx_years_left]').val($_GET['lx_years_left']);
                    if ($_GET['lx_onsite_desktop'] == 'Onsite')
                        $('#edit-lx-onsite-desktop-onsite').attr('checked', 'checked');
                    if ($_GET['lx_onsite_desktop'] == 'Desktop')
                        $('#edit-lx-onsite-desktop-desktop').attr('checked', 'checked');
                    $('#form-layout-lx1').show();
                    $('#form-layout-lx1 :input').removeAttr('disabled');
                    $('#form-layout-onsite-desktop').show();
                    $('#form-layout-onsite-desktop :input').removeAttr('disabled');
                    break;
                case '12':
                    $('#edit-service-id-12').attr('checked', 'checked');
                    $('[name=lx_ground_rent]').val($_GET['lx_ground_rent']);
                    $('[name=lx_years_left]').val($_GET['lx_years_left']);
                    $('#form-layout-lx2').show();
                    $('#form-layout-lx2 :input').removeAttr('disabled');
                    break;
                case '13':
                    $('#edit-service-id-13').attr('checked', 'checked');
                    $('[name=lx_ground_rent]').val($_GET['lx_ground_rent']);
                    $('[name=lx_years_left]').val($_GET['lx_years_left']);
                    $('#form-layout-lx3').show();
                    $('#form-layout-lx3 :input').removeAttr('disabled');
                    $('#form-layout-onsite-desktop').hide();
                    break;
                case '14':
                    $('#edit-service-id-14').attr('checked', 'checked');
                    $('[name=lx_ground_rent]').val($_GET['lx_ground_rent']);
                    $('[name=lx_years_left]').val($_GET['lx_years_left']);
                    if ($_GET['lx_onsite_desktop'] == 'Onsite')
                        $('#edit-lx-onsite-desktop-onsite').attr('checked', 'checked');
                    if ($_GET['lx_onsite_desktop'] == 'Desktop')
                        $('#edit-lx-onsite-desktop-desktop').attr('checked', 'checked');
                    if ($_GET['lx_mortgage_on_prop'] == 'Yes')
                        $('#edit-lx-mortgage-on-prop-yes').attr('checked', 'checked');
                    if ($_GET['lx_mortgage_on_prop'] == 'No')
                        $('#edit-lx-mortgage-on-prop-no').attr('checked', 'checked');
                    $('#form-layout-lx3').show();
                    $('#form-layout-lx3 :input').removeAttr('disabled');
                    $('#form-layout-onsite-desktop').show();
                    $('#form-layout-onsite-desktop :input').removeAttr('disabled');
                    break;
                case '15':
                    $('#edit-service-id-15').attr('checked', 'checked');
                    $('#edit-sco-price').val($_GET['sco_price']);
                    if ($_GET['sco_mortgage'] == 'Yes')
                        $('#edit-sco-mortgage-yes').attr('checked', 'checked');
                    if ($_GET['sco_mortgage'] == 'No')
                        $('#edit-sco-mortgage-no').attr('checked', 'checked');
                    if ($_GET['sco_tenure'] == 'Leasehold')
                        $('#edit-sco-tenure-leasehold').attr('checked', 'checked');
                    if ($_GET['sco_tenure'] == 'Freehold')
                        $('#edit-sco-tenure-freehold').attr('checked', 'checked');
                    $('#form-layout-sale').show();
                    break;
                case '16':
                    $('#edit-service-id-16').attr('checked', 'checked');
                    $('#edit-pco-price').val($_GET['pco_price']);
                    if ($_GET['pco_mortgage'] == 'Yes')
                        $('#edit-pco-mortgage-yes').attr('checked', 'checked');
                    if ($_GET['pco_mortgage'] == 'No')
                        $('#edit-pco-mortgage-no').attr('checked', 'checked');
                    if ($_GET['pco_tenure'] == 'Leasehold')
                        $('#edit-pco-tenure-leasehold').attr('checked', 'checked');
                    if ($_GET['pco_tenure'] == 'Freehold')
                        $('#edit-pco-tenure-freehold').attr('checked', 'checked');
                    if ($_GET['pco_tenanted'] == 'Vacant')
                        $('#edit-pco-tenanted-vacant').attr('checked', 'checked');
                    if ($_GET['pco_tenanted'] == 'Tenanted')
                        $('#edit-pco-tenanted-tenanted').attr('checked', 'checked');
                    $('#form-layout-purchase').show();
                    break;
                case '17':
                    $('#edit-service-id-17').attr('checked', 'checked');
                    if ($_GET['lt_lease_length'] == '7 years or over')
                        $('#edit-lt-lease-length-7-years-or-over').attr('checked', 'checked');
                    if ($_GET['lt_lease_length'] == 'under 7 years')
                        $('#edit-lt-lease-length-under-7-years').attr('checked', 'checked');
                    $('#form-layout-lease-creation').show();
                    break;
                case '18':
                    $('#edit-service-id-18').attr('checked', 'checked');
                    if ($_GET['lt_business_acquisition'] == 'Yes')
                        $('#edit-lt-business-acquisition-yes').attr('checked', 'checked');
                    if ($_GET['lt_business_acquisition'] == 'No')
                        $('#edit-lt-business-acquisition-no').attr('checked', 'checked');
                    $('#form-layout-lease-transfer').show();
                    break;
            }
            /*
             * Make sure that all forms are enable/disabled as required
             * Force click event incase the service radio was preselected
             */
            $('input:radio[name=service_id]:checked').click();

            $('#edit-person-full-name').val($_GET['person_full_name']);
            $('#edit-person-telephone').val($_GET['person_telephone']);
            $('#edit-person-email').val($_GET['person_email']);
            $('#edit-ext-discount').val($_GET['ext_discount']);
        }

        $('.comparison-quote-detail-button').click(function () {
            var idarr = $(this).attr('id').split('-');
            var id = idarr[4];
            if ($('#comparison-quote-detail-' + id).is(':visible')) {
                $('.comparison-quote-detail').hide();
                $(this).parent().parent().css('backgroundColor', '#eee');
                $(this).parent().parent().css('color', '#000');
                $(this).html('View detail');
                $(this).css('color', '#167bcc');
            }
            else {
                $('.comparison-quote-detail').hide();
                $('#comparison-quote-detail-' + id).show();
                $(this).parent().parent().css('backgroundColor', '#000');
                $(this).parent().parent().css('color', '#fff');
                $(this).html('Hide detail');
                $(this).css('color', '#fff');
            }
        });
    });
})(jQuery);
function array_key_exists(key, search) {
    if (!search || (search.constructor !== Array && search.constructor !== Object)) {
        return false;
    }
    return key in search;
}

/**
 * getStyleObject Plugin for jQuery JavaScript Library
 * From: http://upshots.org/?p=112
 *
 * Copyright: Unknown, see source link
 * Plugin version by Dakota Schneider (http://hackthetruth.org)
 */
(function ($) {
    $.fn.getStyleObject = function () {
        var dom = this.get(0);
        var style;
        var returns = {};
        if (window.getComputedStyle) {
            var camelize = function (a, b) {
                return b.toUpperCase();
            };
            style = window.getComputedStyle(dom, null);
            for (var i = 0; i < style.length; i++) {
                var prop = style[i];
                var camel = prop.replace(/\-([a-z])/g, camelize);
                var val = style.getPropertyValue(prop);
                returns[camel] = val;
            }
            return returns;
        }
        if (dom.currentStyle) {
            style = dom.currentStyle;
            for (var prop in style) {
                returns[prop] = style[prop];
            }
            return returns;
        }
        return this.css();
    };
})(jQuery);
(function ($) {
    $.fn.getRealDimensions = function (outer) {
        var $this = $(this);
        if ($this.length == 0) {
            return false;
        }
        var $clone = $this.clone()
                .show()
                .css('visibility', 'hidden')
                .insertAfter($this);
        var result = {
            width: (outer) ? $clone.outerWidth() : $clone.innerWidth(),
            height: (outer) ? $clone.outerHeight() : $clone.innerHeight(),
            offsetTop: $clone.offset().top,
            offsetLeft: $clone.offset().left
        };
        $clone.remove();
        return result;
    };
})(jQuery);
(function ($) {
    $.fn.textWidth = function (text, font, size) {
        if (!$.fn.textWidth.fakeEl)
            $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
        $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css({
            'font': font || this.css('font'),
            'font-size': size || this.css('font-size'),
        });
        return $.fn.textWidth.fakeEl.width();
    };
})(jQuery);
(function ($) {

    /* =====================================================================
     * Add in thousands separator and pound symbol for user
     *
     * Do some javascript magic to force the use of the numpad on mobile devices
     * while still being able to format the value that has been input
     */


    var cloneSet = [];
    var cssCloneColor = [];
    var cssOriginalWidth = [];
    /* Create the dummy output */
    function createCurrency(original, currencyId) {
        if (cloneSet[currencyId] == undefined) {

            /*
             * Currency symbol
             */
            var currencySymbol = $('<div>&pound;</div>');
            var currencyWidth = original.textWidth('\u00A3') + 3;
            currencySymbol.attr('id', currencyId);
            currencySymbol.attr('class', 'rQuip_currencySymbol');

            var originalPaddingTop = original.css('padding-top').replace('px', '');
            var originalMarginTop = original.css('border-top-width').replace('px', '');
            var originalBorderTop = original.css('margin-top').replace('px', '');
            if (isNaN(originalPaddingTop)) {
                originalPaddingTop = 0;
            }
            if (isNaN(originalMarginTop)) {
                originalMarginTop = 0;
            }
            if (isNaN(originalBorderTop)) {
                originalBorderTop = 0;
            }
            var currencyTop = parseInt(originalPaddingTop) + parseInt(originalMarginTop) + parseInt(originalBorderTop);
            /*
             * We need to work out if we are portrait or landscape and work
             * out the positioning of the pound symbol
             */
            original.parent().css('position', 'relative');
            original.parent().addClass('clearfix');
            original.css({
                'padding-left': currencyWidth + 'px',
                'position': 'relative',
            });

            currencySymbol.css({
                'display': 'inline',
                'position': 'absolute',
                'color': original.css('color'),
                'font-size': original.css('font-size'),
                'font-family': original.css('font-family'),
                'z-index': '2',
            });

            var parent = original.parent();
            var parentLeft = $(parent).offset().left;
            var originalLeft = $(original).offset().left - parentLeft + 3;
            var originalTop = $(original).position().top + currencyTop;

            currencySymbol.css({
                'top': originalTop + 'px',
                'left': originalLeft + 'px',
            });
            currencySymbol.insertBefore(original);

            cloneSet[currencyId] = true;
        }
    }

    /*
     * Add the dummy outputs and associated events
     * Input IDs that need currency and thousands separators
     */
    $(document).ready(function () {
        
        function insertCurrencySymbol() {
            
            /*
             * We need a clearfix class to handle landscape mobile devices
             */
            $('<style>.container:before, \n\
                          .container:after {content:""; display:table;} \n\
                          .container:after {clear:both;} \n\
                          .container {zoom:1;} \n\
                   < /style>').appendTo('body');

            $('.form-layout-group').on('show.currencySymbol', function () {

                $(this).css({
                    'visibility': 'hidden',
                    'display': 'block'
                });


                $(this).find('\
                [name="bsu_property_value"],\n\
                [name="pco_price"],\n\
                [name="sco_price"],\n\
                [name="lx_ground_rent"],\n\
                [name="pvr_property_value"],\n\
                [name="rem_price"],\n\
                [name="rem_borrowing"],\n\
                [name="hbs_property_value"],\n\
                [name="toe_price"],\n\
                [name="toe_money"]').each(function () {

                    $(this).attr('pattern', '[0-9]*');

                    var originalId = $(this).attr('id');
                    if (originalId == undefined) {
                        originalId = $(this).attr('name');
                        $(this).attr('id', originalId);
                    }

                    var currencyId = originalId + '_' + $('[id="' + originalId + '"]').index(this) + '_CurrencySymbol';
                    $('[id=' + currencyId + ']').remove();
                    cloneSet[currencyId] = undefined;
                    createCurrency($(this), currencyId);

                });
                $(this).css({
                    'visibility': 'visible',
                    'display': 'block'
                });
            });

            $(window).on('resize', function () {
                $('[name="bsu_property_value"],\n\
                [name="pco_price"],\n\
                [name="sco_price"],\n\
                [name="lx_ground_rent"],\n\
                [name="pvr_property_value"],\n\
                [name="rem_price"],\n\
                [name="rem_borrowing"],\n\
                [name="hbs_property_value"],\n\
                [name="toe_price"],\n\
                [name="toe_money"]').each(function () {

                    $(this).attr('pattern', '[0-9]*');

                    var originalId = $(this).attr('id');
                    if (originalId == undefined) {
                        originalId = $(this).attr('name');
                        $(this).attr('id', originalId);
                    }

                    var currencyId = originalId + '_' + $('[id="' + originalId + '"]').index(this) + '_CurrencySymbol';
                    $('[id=' + currencyId + ']').remove();
                    cloneSet[currencyId] = undefined;
                    createCurrency($(this), currencyId);

                });
            });
        }

        if (window.operamini) {
            return;
        }
        var supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;

        /*
         * For touch devices
         */
        if (supportsTouch == true) {
            insertCurrencySymbol();
        }
        else {
            /*
             * Desktop
             */
            $('     [name="bsu_property_value"],\n\
                [name="pco_price"],\n\
                [name="sco_price"],\n\
                [name="lx_ground_rent"],\n\
                [name="ext_discount"],\n\
                [name="pvr_property_value"],\n\
                [name="rem_price"],\n\
                [name="rem_borrowing"],\n\
                [name="hbs_property_value"],\n\
                [name="toe_price"],\n\
                [name="toe_money"]').each(function () {

                if (typeof jQuery.fn.prop === 'function') {
                    jQuery(this).attr('type', 'text');

                    jQuery(this).autoNumeric('init', {
                        lZero: 'deny',
                        aSep: ',',
                        mDec: 2,
                        aSign: '\u00A3'
                    });
                }
                else {
                    insertCurrencySymbol();
                }

            });
        }

    });
})(jQuery);
/**
 * autoNumeric.js
 * @author: Bob Knothe
 * @author: Sokolov Yura
 * @version: 1.9.39 - 2015-07-17 GMT 5:00 PM / 19:00
 *
 * Created by Robert J. Knothe on 2010-10-25. Please report any bugs to https://github.com/BobKnothe/autoNumeric
 * Contributor by Sokolov Yura on 2010-11-07
 *
 * Copyright (c) 2011 Robert J. Knothe http://www.decorplanit.com/plugin/
 *
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
!function (a) {
    "use strict";
    function b(a) {
        var b = {};
        if (void 0 === a.selectionStart) {
            a.focus();
            var c = document.selection.createRange();
            b.length = c.text.length, c.moveStart("character", -a.value.length), b.end = c.text.length, b.start = b.end - b.length
        } else
            b.start = a.selectionStart, b.end = a.selectionEnd, b.length = b.end - b.start;
        return b
    }
    function c(a, b, c) {
        if (void 0 === a.selectionStart) {
            a.focus();
            var d = a.createTextRange();
            d.collapse(!0), d.moveEnd("character", c), d.moveStart("character", b), d.select()
        } else
            a.selectionStart = b, a.selectionEnd = c
    }
    function d(b, c) {
        a.each(c, function (a, d) {
            "function" == typeof d ? c[a] = d(b, c, a) : "function" == typeof b.autoNumeric[d] && (c[a] = b.autoNumeric[d](b, c, a))
        })
    }
    function e(a, b) {
        "string" == typeof a[b] && (a[b] *= 1)
    }
    function f(a, b) {
        d(a, b), b.tagList = ["b", "caption", "cite", "code", "dd", "del", "div", "dfn", "dt", "em", "h1", "h2", "h3", "h4", "h5", "h6", "ins", "kdb", "label", "li", "output", "p", "q", "s", "sample", "span", "strong", "td", "th", "u", "var"];
        var c = b.vMax.toString().split("."), f = b.vMin || 0 === b.vMin ? b.vMin.toString().split(".") : [];
        if (e(b, "vMax"), e(b, "vMin"), e(b, "mDec"), b.mDec = "CHF" === b.mRound ? "2" : b.mDec, b.allowLeading = !0, b.aNeg = b.vMin < 0 ? "-" : "", c[0] = c[0].replace("-", ""), f[0] = f[0].replace("-", ""), b.mInt = Math.max(c[0].length, f[0].length, 1), null === b.mDec) {
            var g = 0, h = 0;
            c[1] && (g = c[1].length), f[1] && (h = f[1].length), b.mDec = Math.max(g, h)
        }
        null === b.altDec && b.mDec > 0 && ("." === b.aDec && "," !== b.aSep ? b.altDec = "," : "," === b.aDec && "." !== b.aSep && (b.altDec = "."));
        var i = b.aNeg ? "([-\\" + b.aNeg + "]?)" : "(-?)";
        b.aNegRegAutoStrip = i, b.skipFirstAutoStrip = new RegExp(i + "[^-" + (b.aNeg ? "\\" + b.aNeg : "") + "\\" + b.aDec + "\\d].*?(\\d|\\" + b.aDec + "\\d)"), b.skipLastAutoStrip = new RegExp("(\\d\\" + b.aDec + "?)[^\\" + b.aDec + "\\d]\\D*$");
        var j = "-" + b.aNum + "\\" + b.aDec;
        return b.allowedAutoStrip = new RegExp("[^" + j + "]", "gi"), b.numRegAutoStrip = new RegExp(i + "(?:\\" + b.aDec + "?(\\d+\\" + b.aDec + "\\d+)|(\\d*(?:\\" + b.aDec + "\\d*)?))"), b
    }
    function g(a, b, c) {
        if (b.aSign)
            for (; a.indexOf(b.aSign) > - 1; )
                a = a.replace(b.aSign, "");
        a = a.replace(b.skipFirstAutoStrip, "$1$2"), a = a.replace(b.skipLastAutoStrip, "$1"), a = a.replace(b.allowedAutoStrip, ""), b.altDec && (a = a.replace(b.altDec, b.aDec));
        var d = a.match(b.numRegAutoStrip);
        if (a = d ? [d[1], d[2], d[3]].join("") : "", ("allow" === b.lZero || "keep" === b.lZero) && "strip" !== c) {
            var e = [], f = "";
            e = a.split(b.aDec), -1 !== e[0].indexOf("-") && (f = "-", e[0] = e[0].replace("-", "")), e[0].length > b.mInt && "0" === e[0].charAt(0) && (e[0] = e[0].slice(1)), a = f + e.join(b.aDec)
        }
        if (c && "deny" === b.lZero || c && "allow" === b.lZero && b.allowLeading === !1) {
            var g = "^" + b.aNegRegAutoStrip + "0*(\\d" + ("leading" === c ? ")" : "|$)");
            g = new RegExp(g), a = a.replace(g, "$1$2")
        }
        return a
    }
    function h(a, b) {
        if ("p" === b.pSign) {
            var c = b.nBracket.split(",");
            b.hasFocus || b.removeBrackets ? (b.hasFocus && a.charAt(0) === c[0] || b.removeBrackets && a.charAt(0) === c[0]) && (a = a.replace(c[0], b.aNeg), a = a.replace(c[1], "")) : (a = a.replace(b.aNeg, ""), a = c[0] + a + c[1])
        }
        return a
    }
    function i(a, b) {
        if (a) {
            var c = +a;
            if (1e-6 > c && c > -1)
                a = +a, 1e-6 > a && a > 0 && (a = (a + 10).toString(), a = a.substring(1)), 0 > a && a > -1 && (a = (a - 10).toString(), a = "-" + a.substring(2)), a = a.toString();
            else {
                var d = a.split(".");
                void 0 !== d[1] && (0 === +d[1] ? a = d[0] : (d[1] = d[1].replace(/0*$/, ""), a = d.join(".")))
            }
        }
        return"keep" === b.lZero ? a : a.replace(/^0*(\d)/, "$1")
    }
    function j(a, b, c) {
        return b && "." !== b && (a = a.replace(b, ".")), c && "-" !== c && (a = a.replace(c, "-")), a.match(/\d/) || (a += "0"), a
    }
    function k(a, b, c) {
        return c && "-" !== c && (a = a.replace("-", c)), b && "." !== b && (a = a.replace(".", b)), a
    }
    function l(a, b, c) {
        return"" === a || a === b.aNeg ? "zero" === b.wEmpty ? a + "0" : "sign" === b.wEmpty || c ? a + b.aSign : a : null
    }
    function m(a, b) {
        a = g(a, b);
        var c = a.replace(",", "."), d = l(a, b, !0);
        if (null !== d)
            return d;
        var e = "";
        e = 2 === b.dGroup ? /(\d)((\d)(\d{2}?)+)$/ : 4 === b.dGroup ? /(\d)((\d{4}?)+)$/ : /(\d)((\d{3}?)+)$/;
        var f = a.split(b.aDec);
        b.altDec && 1 === f.length && (f = a.split(b.altDec));
        var i = f[0];
        if (b.aSep)
            for (; e.test(i); )
                i = i.replace(e, "$1" + b.aSep + "$2");
        if (0 !== b.mDec && f.length > 1 ? (f[1].length > b.mDec && (f[1] = f[1].substring(0, b.mDec)), a = i + b.aDec + f[1]) : a = i, b.aSign) {
            var j = -1 !== a.indexOf(b.aNeg);
            a = a.replace(b.aNeg, ""), a = "p" === b.pSign ? b.aSign + a : a + b.aSign, j && (a = b.aNeg + a)
        }
        return 0 > c && null !== b.nBracket && (a = h(a, b)), a
    }
    function n(a, b) {
        a = "" === a ? "0" : a.toString(), e(b, "mDec"), "CHF" === b.mRound && (a = (Math.round(20 * a) / 20).toString());
        var c = "", d = 0, f = "", g = "boolean" == typeof b.aPad || null === b.aPad ? b.aPad ? b.mDec : 0 : +b.aPad, h = function (a) {
            var b = 0 === g ? /(\.(?:\d*[1-9])?)0*$/ : 1 === g ? /(\.\d(?:\d*[1-9])?)0*$/ : new RegExp("(\\.\\d{" + g + "}(?:\\d*[1-9])?)0*$");
            return a = a.replace(b, "$1"), 0 === g && (a = a.replace(/\.$/, "")), a
        };
        "-" === a.charAt(0) && (f = "-", a = a.replace("-", "")), a.match(/^\d/) || (a = "0" + a), "-" === f && 0 === +a && (f = ""), (+a > 0 && "keep" !== b.lZero || a.length > 0 && "allow" === b.lZero) && (a = a.replace(/^0*(\d)/, "$1"));
        var i = a.lastIndexOf("."), j = -1 === i ? a.length - 1 : i, k = a.length - 1 - j;
        if (k <= b.mDec) {
            if (c = a, g > k) {
                -1 === i && (c += ".");
                for (var l = "000000"; g > k; )
                    l = l.substring(0, g - k), c += l, k += l.length
            } else
                k > g ? c = h(c) : 0 === k && 0 === g && (c = c.replace(/\.$/, ""));
            if ("CHF" !== b.mRound)
                return 0 === +c ? c : f + c;
            "CHF" === b.mRound && (i = c.lastIndexOf("."), a = c)
        }
        var m = i + b.mDec, n = +a.charAt(m + 1), o = a.substring(0, m + 1).split(""), p = "." === a.charAt(m) ? a.charAt(m - 1) % 2 : a.charAt(m) % 2, q = !0;
        if (1 !== p && (p = 0 === p && a.substring(m + 2, a.length) > 0 ? 1 : 0), n > 4 && "S" === b.mRound || n > 4 && "A" === b.mRound && "" === f || n > 5 && "A" === b.mRound && "-" === f || n > 5 && "s" === b.mRound || n > 5 && "a" === b.mRound && "" === f || n > 4 && "a" === b.mRound && "-" === f || n > 5 && "B" === b.mRound || 5 === n && "B" === b.mRound && 1 === p || n > 0 && "C" === b.mRound && "" === f || n > 0 && "F" === b.mRound && "-" === f || n > 0 && "U" === b.mRound || "CHF" === b.mRound)
            for (d = o.length - 1; d >= 0; d -= 1)
                if ("." !== o[d]) {
                    if ("CHF" === b.mRound && o[d] <= 2 && q) {
                        o[d] = 0, q = !1;
                        break
                    }
                    if ("CHF" === b.mRound && o[d] <= 7 && q) {
                        o[d] = 5, q = !1;
                        break
                    }
                    if ("CHF" === b.mRound && q ? (o[d] = 10, q = !1) : o[d] = +o[d] + 1, o[d] < 10)
                        break;
                    d > 0 && (o[d] = "0")
                }
        return o = o.slice(0, m + 1), c = h(o.join("")), 0 === +c ? c : f + c
    }
    function o(a, b, c) {
        var d = b.aDec, e = b.mDec;
        if (a = "paste" === c ? n(a, b) : a, d && e) {
            var f = a.split(d);
            f[1] && f[1].length > e && (e > 0 ? (f[1] = f[1].substring(0, e), a = f.join(d)) : a = f[0])
        }
        return a
    }
    function p(a, b) {
        a = g(a, b), a = o(a, b), a = j(a, b.aDec, b.aNeg);
        var c = +a;
        return c >= b.vMin && c <= b.vMax
    }
    function q(b, c) {
        this.settings = c, this.that = b, this.$that = a(b), this.formatted = !1, this.settingsClone = f(this.$that, this.settings), this.value = b.value
    }
    function r(b) {
        return"string" == typeof b && (b = b.replace(/\[/g, "\\[").replace(/\]/g, "\\]"), b = "#" + b.replace(/(:|\.)/g, "\\$1")), a(b)
    }
    function s(a, b, c) {
        var d = a.data("autoNumeric");
        d || (d = {}, a.data("autoNumeric", d));
        var e = d.holder;
        return(void 0 === e && b || c) && (e = new q(a.get(0), b), d.holder = e), e
    }
    q.prototype = {init: function (a) {
            this.value = this.that.value, this.settingsClone = f(this.$that, this.settings), this.ctrlKey = a.ctrlKey, this.cmdKey = a.metaKey, this.shiftKey = a.shiftKey, this.selection = b(this.that), ("keydown" === a.type || "keyup" === a.type) && (this.kdCode = a.keyCode), this.which = a.which, this.processed = !1, this.formatted = !1
        }, setSelection: function (a, b, d) {
            a = Math.max(a, 0), b = Math.min(b, this.that.value.length), this.selection = {start: a, end: b, length: b - a}, (void 0 === d || d) && c(this.that, a, b)
        }, setPosition: function (a, b) {
            this.setSelection(a, a, b)
        }, getBeforeAfter: function () {
            var a = this.value, b = a.substring(0, this.selection.start), c = a.substring(this.selection.end, a.length);
            return[b, c]
        }, getBeforeAfterStriped: function () {
            var a = this.getBeforeAfter();
            return a[0] = g(a[0], this.settingsClone), a[1] = g(a[1], this.settingsClone), a
        }, normalizeParts: function (a, b) {
            var c = this.settingsClone;
            b = g(b, c);
            var d = b.match(/^\d/) ? !0 : "leading";
            a = g(a, c, d), "" !== a && a !== c.aNeg || "deny" !== c.lZero || b > "" && (b = b.replace(/^0*(\d)/, "$1"));
            var e = a + b;
            if (c.aDec) {
                var f = e.match(new RegExp("^" + c.aNegRegAutoStrip + "\\" + c.aDec));
                f && (a = a.replace(f[1], f[1] + "0"), e = a + b)
            }
            return"zero" !== c.wEmpty || e !== c.aNeg && "" !== e || (a += "0"), [a, b]
        }, setValueParts: function (a, b, c) {
            var d = this.settingsClone, e = this.normalizeParts(a, b), f = e.join(""), g = e[0].length;
            return p(f, d) ? (f = o(f, d, c), g > f.length && (g = f.length), this.value = f, this.setPosition(g, !1), !0) : !1
        }, signPosition: function () {
            var a = this.settingsClone, b = a.aSign, c = this.that;
            if (b) {
                var d = b.length;
                if ("p" === a.pSign) {
                    var e = a.aNeg && c.value && c.value.charAt(0) === a.aNeg;
                    return e ? [1, d + 1] : [0, d]
                }
                var f = c.value.length;
                return[f - d, f]
            }
            return[1e3, -1]
        }, expandSelectionOnSign: function (a) {
            var b = this.signPosition(), c = this.selection;
            c.start < b[1] && c.end > b[0] && ((c.start < b[0] || c.end > b[1]) && this.value.substring(Math.max(c.start, b[0]), Math.min(c.end, b[1])).match(/^\s*$/) ? c.start < b[0] ? this.setSelection(c.start, b[0], a) : this.setSelection(b[1], c.end, a) : this.setSelection(Math.min(c.start, b[0]), Math.max(c.end, b[1]), a))
        }, checkPaste: function () {
            if (void 0 !== this.valuePartsBeforePaste) {
                var a = this.getBeforeAfter(), b = this.valuePartsBeforePaste;
                delete this.valuePartsBeforePaste, a[0] = a[0].substr(0, b[0].length) + g(a[0].substr(b[0].length), this.settingsClone), this.setValueParts(a[0], a[1], "paste") || (this.value = b.join(""), this.setPosition(b[0].length, !1))
            }
        }, skipAllways: function (a) {
            var b = this.kdCode, c = this.which, d = this.ctrlKey, e = this.cmdKey, f = this.shiftKey;
            if ((d || e) && "keyup" === a.type && void 0 !== this.valuePartsBeforePaste || f && 45 === b)
                return this.checkPaste(), !1;
            if (b >= 112 && 123 >= b || b >= 91 && 93 >= b || b >= 9 && 31 >= b || 8 > b && (0 === c || c === b) || 144 === b || 145 === b || 45 === b || 224 === b)
                return!0;
            if ((d || e) && 65 === b)
                return!0;
            if ((d || e) && (67 === b || 86 === b || 88 === b))
                return"keydown" === a.type && this.expandSelectionOnSign(), (86 === b || 45 === b) && ("keydown" === a.type || "keypress" === a.type ? void 0 === this.valuePartsBeforePaste && (this.valuePartsBeforePaste = this.getBeforeAfter()) : this.checkPaste()), "keydown" === a.type || "keypress" === a.type || 67 === b;
            if (d || e)
                return!0;
            if (37 === b || 39 === b) {
                var g = this.settingsClone.aSep, h = this.selection.start, i = this.that.value;
                return"keydown" === a.type && g && !this.shiftKey && (37 === b && i.charAt(h - 2) === g ? this.setPosition(h - 1) : 39 === b && i.charAt(h + 1) === g && this.setPosition(h + 1)), !0
            }
            return b >= 34 && 40 >= b ? !0 : !1
        }, processAllways: function () {
            var a;
            return 8 === this.kdCode || 46 === this.kdCode ? (this.selection.length ? (this.expandSelectionOnSign(!1), a = this.getBeforeAfterStriped(), this.setValueParts(a[0], a[1])) : (a = this.getBeforeAfterStriped(), 8 === this.kdCode ? a[0] = a[0].substring(0, a[0].length - 1) : a[1] = a[1].substring(1, a[1].length), this.setValueParts(a[0], a[1])), !0) : !1
        }, processKeypress: function () {
            var a = this.settingsClone, b = String.fromCharCode(this.which), c = this.getBeforeAfterStriped(), d = c[0], e = c[1];
            return b === a.aDec || a.altDec && b === a.altDec || ("." === b || "," === b) && 110 === this.kdCode ? a.mDec && a.aDec ? a.aNeg && e.indexOf(a.aNeg) > -1 ? !0 : d.indexOf(a.aDec) > -1 ? !0 : e.indexOf(a.aDec) > 0 ? !0 : (0 === e.indexOf(a.aDec) && (e = e.substr(1)), this.setValueParts(d + a.aDec, e), !0) : !0 : "-" === b || "+" === b ? a.aNeg ? ("" === d && e.indexOf(a.aNeg) > -1 && (d = a.aNeg, e = e.substring(1, e.length)), d = d.charAt(0) === a.aNeg ? d.substring(1, d.length) : "-" === b ? a.aNeg + d : d, this.setValueParts(d, e), !0) : !0 : b >= "0" && "9" >= b ? (a.aNeg && "" === d && e.indexOf(a.aNeg) > -1 && (d = a.aNeg, e = e.substring(1, e.length)), a.vMax <= 0 && a.vMin < a.vMax && -1 === this.value.indexOf(a.aNeg) && "0" !== b && (d = a.aNeg + d), this.setValueParts(d + b, e), !0) : !0
        }, formatQuick: function () {
            var a = this.settingsClone, b = this.getBeforeAfterStriped(), c = this.value;
            if (("" === a.aSep || "" !== a.aSep && -1 === c.indexOf(a.aSep)) && ("" === a.aSign || "" !== a.aSign && -1 === c.indexOf(a.aSign))) {
                var d = [], e = "";
                d = c.split(a.aDec), d[0].indexOf("-") > -1 && (e = "-", d[0] = d[0].replace("-", ""), b[0] = b[0].replace("-", "")), d[0].length > a.mInt && "0" === b[0].charAt(0) && (b[0] = b[0].slice(1)), b[0] = e + b[0]
            }
            var f = m(this.value, this.settingsClone), g = f.length;
            if (f) {
                var h = b[0].split(""), i = 0;
                for (i; i < h.length; i += 1)
                    h[i].match("\\d") || (h[i] = "\\" + h[i]);
                var j = new RegExp("^.*?" + h.join(".*?")), k = f.match(j);
                k ? (g = k[0].length, (0 === g && f.charAt(0) !== a.aNeg || 1 === g && f.charAt(0) === a.aNeg) && a.aSign && "p" === a.pSign && (g = this.settingsClone.aSign.length + ("-" === f.charAt(0) ? 1 : 0))) : a.aSign && "s" === a.pSign && (g -= a.aSign.length)
            }
            this.that.value = f, this.setPosition(g), this.formatted = !0
        }};
    var t = {init: function (b) {
            return this.each(function () {
                var d = a(this), e = d.data("autoNumeric"), f = d.data(), i = d.is("input[type=text], input[type=hidden], input[type=tel], input:not([type])");
                if ("object" == typeof e)
                    return this;
                e = a.extend({}, a.fn.autoNumeric.defaults, f, b, {aNum: "0123456789", hasFocus: !1, removeBrackets: !1, runOnce: !1, tagList: ["b", "caption", "cite", "code", "dd", "del", "div", "dfn", "dt", "em", "h1", "h2", "h3", "h4", "h5", "h6", "ins", "kdb", "label", "li", "output", "p", "q", "s", "sample", "span", "strong", "td", "th", "u", "var"]}), e.aDec === e.aSep && a.error("autoNumeric will not function properly when the decimal character aDec: '" + e.aDec + "' and thousand separator aSep: '" + e.aSep + "' are the same character"), d.data("autoNumeric", e);
                var o = s(d, e);
                if (i || "input" !== d.prop("tagName").toLowerCase() || a.error('The input type "' + d.prop("type") + '" is not supported by autoNumeric()'), -1 === a.inArray(d.prop("tagName").toLowerCase(), e.tagList) && "input" !== d.prop("tagName").toLowerCase() && a.error("The <" + d.prop("tagName").toLowerCase() + "> is not supported by autoNumeric()"), e.runOnce === !1 && e.aForm) {
                    if (i) {
                        var q = !0;
                        "" === d[0].value && "empty" === e.wEmpty && (d[0].value = "", q = !1), "" === d[0].value && "sign" === e.wEmpty && (d[0].value = e.aSign, q = !1), q && "" !== d.val() && (null === e.anDefault && d[0].value === d.prop("defaultValue") || null !== e.anDefault && e.anDefault.toString() === d.val()) && d.autoNumeric("set", d.val())
                    }
                    -1 !== a.inArray(d.prop("tagName").toLowerCase(), e.tagList) && "" !== d.text() && d.autoNumeric("set", d.text())
                }
                e.runOnce = !0, d.is("input[type=text], input[type=hidden], input[type=tel], input:not([type])") && (d.on("keydown.autoNumeric", function (b) {
                    return o = s(d), o.settings.aDec === o.settings.aSep && a.error("autoNumeric will not function properly when the decimal character aDec: '" + o.settings.aDec + "' and thousand separator aSep: '" + o.settings.aSep + "' are the same character"), o.that.readOnly ? (o.processed = !0, !0) : (o.init(b), o.skipAllways(b) ? (o.processed = !0, !0) : o.processAllways() ? (o.processed = !0, o.formatQuick(), b.preventDefault(), !1) : (o.formatted = !1, !0))
                }), d.on("keypress.autoNumeric", function (a) {
                    o = s(d);
                    var b = o.processed;
                    return o.init(a), o.skipAllways(a) ? !0 : b ? (a.preventDefault(), !1) : o.processAllways() || o.processKeypress() ? (o.formatQuick(), a.preventDefault(), !1) : void(o.formatted = !1)
                }), d.on("keyup.autoNumeric", function (a) {
                    o = s(d), o.init(a);
                    var b = o.skipAllways(a);
                    return o.kdCode = 0, delete o.valuePartsBeforePaste, d[0].value === o.settings.aSign && ("s" === o.settings.pSign ? c(this, 0, 0) : c(this, o.settings.aSign.length, o.settings.aSign.length)), b ? !0 : "" === this.value ? !0 : void(o.formatted || o.formatQuick())
                }), d.on("focusin.autoNumeric", function () {
                    o = s(d);
                    var a = o.settingsClone;
                    if (a.hasFocus = !0, null !== a.nBracket) {
                        var b = d.val();
                        d.val(h(b, a))
                    }
                    o.inVal = d.val();
                    var c = l(o.inVal, a, !0);
                    null !== c && "" !== c && d.val(c)
                }), d.on("focusout.autoNumeric", function () {
                    o = s(d);
                    var a = o.settingsClone, b = d.val(), c = b;
                    a.hasFocus = !1;
                    var e = "";
                    "allow" === a.lZero && (a.allowLeading = !1, e = "leading"), "" !== b && (b = g(b, a, e), null === l(b, a) && p(b, a, d[0]) ? (b = j(b, a.aDec, a.aNeg), b = n(b, a), b = k(b, a.aDec, a.aNeg)) : b = "");
                    var f = l(b, a, !1);
                    null === f && (f = m(b, a)), (f !== o.inVal || f !== c) && (d.val(f), d.change(), delete o.inVal)
                }))
            })
        }, destroy: function () {
            return a(this).each(function () {
                var b = a(this);
                b.off(".autoNumeric"), b.removeData("autoNumeric")
            })
        }, update: function (b) {
            return a(this).each(function () {
                var c = r(a(this)), d = c.data("autoNumeric");
                "object" != typeof d && a.error("You must initialize autoNumeric('init', {options}) prior to calling the 'update' method");
                var e = c.autoNumeric("get");
                return d = a.extend(d, b), s(c, d, !0), d.aDec === d.aSep && a.error("autoNumeric will not function properly when the decimal character aDec: '" + d.aDec + "' and thousand separator aSep: '" + d.aSep + "' are the same character"), c.data("autoNumeric", d), "" !== c.val() || "" !== c.text() ? c.autoNumeric("set", e) : void 0
            })
        }, set: function (b) {
            return null !== b ? a(this).each(function () {
                var c = r(a(this)), d = c.data("autoNumeric"), e = b.toString(), f = b.toString(), g = c.is("input[type=text], input[type=hidden], input[type=tel], input:not([type])");
                return"object" != typeof d && a.error("You must initialize autoNumeric('init', {options}) prior to calling the 'set' method"), f !== c.attr("value") && f !== c.text() || d.runOnce !== !1 || (e = e.replace(",", ".")), a.isNumeric(+e) || a.error("The value (" + e + ") being 'set' is not numeric and has caused a error to be thrown"), e = i(e, d), d.setEvent = !0, e.toString(), "" !== e && (e = n(e, d)), e = k(e, d.aDec, d.aNeg), p(e, d) || (e = n("", d)), e = m(e, d), g ? c.val(e) : -1 !== a.inArray(c.prop("tagName").toLowerCase(), d.tagList) ? c.text(e) : !1
            }) : void 0
        }, get: function () {
            var b = r(a(this)), c = b.data("autoNumeric");
            "object" != typeof c && a.error("You must initialize autoNumeric('init', {options}) prior to calling the 'get' method");
            var d = "";
            return b.is("input[type=text], input[type=hidden], input[type=tel], input:not([type])") ? d = b.eq(0).val() : -1 !== a.inArray(b.prop("tagName").toLowerCase(), c.tagList) ? d = b.eq(0).text() : a.error("The <" + b.prop("tagName").toLowerCase() + "> is not supported by autoNumeric()"), "" === d && "empty" === c.wEmpty || d === c.aSign && ("sign" === c.wEmpty || "empty" === c.wEmpty) ? "" : ("" !== d && null !== c.nBracket && (c.removeBrackets = !0, d = h(d, c), c.removeBrackets = !1), (c.runOnce || c.aForm === !1) && (d = g(d, c)), d = j(d, c.aDec, c.aNeg), 0 === +d && "keep" !== c.lZero && (d = "0"), "keep" === c.lZero ? d : d = i(d, c))
        }, getString: function () {
            var b = !1, c = r(a(this)), d = c.serialize(), e = d.split("&"), f = a("form").index(c), g = a("form:eq(" + f + ")"), h = [], i = [], j = /^(?:submit|button|image|reset|file)$/i, k = /^(?:input|select|textarea|keygen)/i, l = /^(?:checkbox|radio)$/i, m = /^(?:button|checkbox|color|date|datetime|datetime-local|email|file|image|month|number|password|radio|range|reset|search|submit|time|url|week)/i, n = 0;
            return a.each(g[0], function (a, b) {
                "" === b.name || !k.test(b.localName) || j.test(b.type) || b.disabled || !b.checked && l.test(b.type) ? i.push(-1) : (i.push(n), n += 1)
            }), n = 0, a.each(g[0], function (a, b) {
                "input" !== b.localName || "" !== b.type && "text" !== b.type && "hidden" !== b.type && "tel" !== b.type ? (h.push(-1), "input" === b.localName && m.test(b.type) && (n += 1)) : (h.push(n), n += 1)
            }), a.each(e, function (c, d) {
                d = e[c].split("=");
                var g = a.inArray(c, i);
                if (g > -1 && h[g] > -1) {
                    var j = a("form:eq(" + f + ") input:eq(" + h[g] + ")"), k = j.data("autoNumeric");
                    "object" == typeof k && null !== d[1] && (d[1] = a("form:eq(" + f + ") input:eq(" + h[g] + ")").autoNumeric("get").toString(), e[c] = d.join("="), b = !0)
                }
            }), b || a.error("You must initialize autoNumeric('init', {options}) prior to calling the 'getString' method"), e.join("&")
        }, getArray: function () {
            var b = !1, c = r(a(this)), d = c.serializeArray(), e = a("form").index(c), f = a("form:eq(" + e + ")"), g = [], h = [], i = /^(?:submit|button|image|reset|file)$/i, j = /^(?:input|select|textarea|keygen)/i, k = /^(?:checkbox|radio)$/i, l = /^(?:button|checkbox|color|date|datetime|datetime-local|email|file|image|month|number|password|radio|range|reset|search|submit|time|url|week)/i, m = 0;
            return a.each(f[0], function (a, b) {
                "" === b.name || !j.test(b.localName) || i.test(b.type) || b.disabled || !b.checked && k.test(b.type) ? h.push(-1) : (h.push(m), m += 1)
            }), m = 0, a.each(f[0], function (a, b) {
                "input" !== b.localName || "" !== b.type && "text" !== b.type && "hidden" !== b.type && "tel" !== b.type ? (g.push(-1), "input" === b.localName && l.test(b.type) && (m += 1)) : (g.push(m), m += 1)
            }), a.each(d, function (c, d) {
                var f = a.inArray(c, h);
                if (f > -1 && g[f] > -1) {
                    var i = a("form:eq(" + e + ") input:eq(" + g[f] + ")"), j = i.data("autoNumeric");
                    "object" == typeof j && (d.value = a("form:eq(" + e + ") input:eq(" + g[f] + ")").autoNumeric("get").toString(), b = !0)
                }
            }), b || a.error("None of the successful form inputs are initialized by autoNumeric."), d
        }, getSettings: function () {
            var b = r(a(this));
            return b.eq(0).data("autoNumeric")
        }};
    a.fn.autoNumeric = function (b) {
        return t[b] ? t[b].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof b && b ? void a.error('Method "' + b + '" is not supported by autoNumeric()') : t.init.apply(this, arguments)
    }, a.fn.autoNumeric.defaults = {aSep: ",", dGroup: "3", aDec: ".", altDec: null, aSign: "", pSign: "p", vMax: "9999999999999.99", vMin: "-9999999999999.99", mDec: null, mRound: "S", aPad: !0, nBracket: null, wEmpty: "empty", lZero: "allow", sNumber: !0, aForm: !0, anDefault: null}
}(jQuery);