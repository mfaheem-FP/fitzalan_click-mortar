/**
 * Created by Bruce Clothier
 */

(function ($) {
    $(document).ready(function () {
        var messages = [];
        var fail = false;
        var reusableVariable = '';

        $('#rquip-quote-form').submit(function (event) {
            $('.form-error-js').remove();
            messages = [];
            fail = false;
            /*
             * @nbconfig
             */
            var service_id = $('input[name="service_id"]:checked', '#rquip-quote-form').val();
            var valid_services = ['2', '3', '4', '5', '6', '7', '8', '9', '11', '12', '13', '14', '15', '16', '17', '18'];
            if ($.inArray(service_id, valid_services) == -1) { //service_id != 2 && service_id != 3 && service_id != 6 && service_id != 5 && service_id != 4) {
                messages.push('Please choose a service');
                fail = true;
            }
            /*
             * @nbconfig
             */
            else {
                switch (service_id) {
                    case '2': // sale conveyancing
                        rquipValidateSaleInput();
                        break;
                    case '3':
                        rquipValidatePurchaseInput();
                        break;
                    case '4':
                        rquipValidateRemInput();
                        break;
                    case '5':
                        rquipValidateToeInput();
                        break;
                    case '6':
                        rquipValidateSaleInput();
                        rquipValidatePurchaseInput();
                        break;
                    case '7':
                        rquipValidateHBS();
                        break;
                    case '8':
                        rquipValidateBS();
                        break;
                    case '9':
                        rquipValidatePVR();
                        break;
                    case '11':
                    case '12':
                    case '13':
                    case '14':
                        rquipValidateLX();
                        break;
                    case '15':
                        rquipValidateCCS();
                        break;
                    case '16':
                        rquipValidateCCP();
                        break;
                    case '17':
                        rquipValidateCCLXCreation();
                        break;
                    case '18':
                        rquipValidateCCLXTransfer();
                        break;
                }
            }
            if ($('#edit-person-full-name').val().length == 0 || $('#edit-person-full-name').val() == 'e.g. Mary Smith') {
                messages.push('Please enter your name');
                fail = true;
            }
            if ($('#edit-person-telephone').val().length == 0 || $('#edit-person-telephone').val() == 'e.g. 0330 660 0643') {
                messages.push('Please enter your contact number');
                fail = true;
            }
            if ($('#edit-person-email').val().length == 0 || $('#edit-person-email').val() == 'e.g. mary.smith@gmail.com') {
                messages.push('Please enter your email address');
                fail = true;
            }

            if (fail) {
                var themessage = '<div class="form-error-js" style="padding:20px;border:1px solid #f00;color:#f00;background:#ffd1cf;"><h2 style="color:#f00;">Please check the information you have entered</h2><ul style="text-align:left;"><li>' + messages.join("</li><li>") + "</li></ul>";
                $('#rquip-quote-form').prepend(themessage);
                $('html, body').animate({scrollTop: '0px'}, 300);
                event.preventDefault();
            }
        });

        function rquipValidateSaleInput() {
            if (!$('#edit-sco-price').val().match("^£?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$") && !$('#edit-sco-price').val().match("^£?[0-9]{1,3},?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$")) {
                messages.push('Please enter the sale price');
                fail = true;
            }
            reusableVariable = $('input[name="sco_mortgage"]:checked', '#rquip-quote-form').val();
            if (!reusableVariable || (reusableVariable != 'Yes' && reusableVariable != 'No')) {
                messages.push('Please tell us if you have a mortgage on your sale property');
                fail = true;
            }
            reusableVariable = $('input[name="sco_tenure"]:checked', '#rquip-quote-form').val();
            if (!reusableVariable || (reusableVariable != 'Freehold' && reusableVariable != 'Leasehold' && reusableVariable != 'Share of Freehold')) {
                messages.push('Please tell us what the type of property the sale property is');
                fail = true;
            }
        }

        function rquipValidatePurchaseInput() {
            if (!$('#edit-pco-price').val().match("^£?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$") && !$('#edit-pco-price').val().match("^£?[0-9]{1,3},?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$")) {
                messages.push('Please enter the purchase price');
                fail = true;
            }
            reusableVariable = $('input[name="pco_mortgage"]:checked', '#rquip-quote-form').val();
            if (!reusableVariable || (reusableVariable != 'Yes' && reusableVariable != 'No')) {
                messages.push('Please tell us if you have a mortgage on your purchase property');
                fail = true;
            }
            reusableVariable = $('input[name="pco_help"]:checked', '#rquip-quote-form').val();
            if (!reusableVariable || (reusableVariable != 'Yes' && reusableVariable != 'No')) {
                messages.push('Please tell us if you will be buying with "Help to buy"');
                fail = true;
            }
            reusableVariable = $('input[name="pco_tenure"]:checked', '#rquip-quote-form').val();
            if (!reusableVariable || (reusableVariable != 'Freehold' && reusableVariable != 'Leasehold' && reusableVariable != 'Share of Freehold')) {
                messages.push('Please tell us what the type of property the purchase property is');
                fail = true;
            }
        }

        function rquipValidateRemInput() {
            if (!$('#edit-rem-price').val().match("^£?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$") && !$('#edit-rem-price').val().match("^£?[0-9]{1,3},?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$")) {
                messages.push('Please enter the property value');
                fail = true;
            }
            if (!$('#edit-rem-borrowing').val().match("^£?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$") && !$('#edit-rem-borrowing').val().match("^£?[0-9]{1,3},?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$")) {
                messages.push('Please enter the remortgage value (amount you are borrowing)');
                fail = true;
            }
        }

        function rquipValidateToeInput() {
            if (!$('#edit-toe-price').val().match("^£?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$") && !$('#edit-toe-price').val().match("^£?[0-9]{1,3},?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$")) {
                messages.push('Please enter the property value');
                fail = true;
            }
            if (!$('#edit-toe-money').val().match("^£?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$") && !$('#edit-toe-money').val().match("^£?[0-9]{1,3},?[0-9]{1,3},?[0-9]{1,3}\\.?[0-9]{2}$") && !$('#edit-toe-money').val().match("^£?0")) {
                messages.push('Please enter the amount of money changing hands');
                fail = true;
            }
            reusableVariable = $('input[name="toe_mortgage"]:checked', '#rquip-quote-form').val();
            if (!reusableVariable || (reusableVariable != 'Yes' && reusableVariable != 'No')) {
                messages.push('Please tell us if you have a mortgage on the property');
                fail = true;
            }
            reusableVariable = $('input[name="toe_many_mortgages"]:checked', '#rquip-quote-form').val();
            if (!reusableVariable || (reusableVariable != '1' && reusableVariable != '2' && reusableVariable != '3')) {
                //messages.push('Please tell us what the type of property the sale property is');
                //fail = true;
            }
            reusableVariable = $('input[name="toe_legal_work"]:checked', '#rquip-quote-form').val();
            if (!reusableVariable || (reusableVariable != 'Yes' && reusableVariable != 'No')) {
                messages.push('Please tell us if you would also like us to handle the legal work for the remortgage');
                fail = true;
            }
        }

        function rquipValidateHBS() {

            if (!$('#edit-hbs-postcode').val().match("^[A-Za-z0-9 ]+$")) {
                messages.push('Please enter a valid postcode (only A to Z, 0 to 9)');
                fail = true;
            }
            if (!$('#edit-hbs-property-value').val().match("^£?[0-9]{1,3},?([0-9]{1,3},?)+\\.?[0-9]{2}$")) {
                messages.push('Please enter the property value');
                fail = true;
            }
        }

        function rquipValidateBS() {

            if (!$('#edit-bsu-postcode').val().match("^[A-Za-z0-9 ]+$")) {
                messages.push('Please enter a valid postcode (only A to Z, 0 to 9)');
                fail = true;
            }
            if (!$('#edit-bsu-property-value').val().match("^£?[0-9]{1,3},?([0-9]{1,3},?)+\\.?[0-9]{2}$")) {
                messages.push('Please enter the property value');
                fail = true;
            }
        }

        function rquipValidatePVR() {

            if (!$('#edit-pvr-postcode').val().match("^[A-Za-z0-9 ]+$")) {
                messages.push('Please enter a valid postcode (only A to Z, 0 to 9)');
                fail = true;
            }
            if (!$('#edit-pvr-property-value').val().match("^£?[0-9]{1,3},?([0-9]{1,3},?)+\\.?[0-9]{2}$")) {
                messages.push('Please enter the property value');
                fail = true;
            }
        }

        function rquipValidateLX() {
            // get the service_id, this is used for several checks here
            var service_id = $('input[name="service_id"]:checked', '#rquip-quote-form').val();

            // specify the form to validate fields against, now we have multiple versions of some fields
            // default form_name scope to the main rquip form so we dont break anything if no form_name is specified
            var form_name = '#rquip-quote-form';
            switch (service_id) {
                case '11':
                    form_name = '#form-layout-lx1';
                    break;
                case '12':
                    form_name = '#form-layout-lx2';
                    break;
                case '13':
                case '14':
                    form_name = '#form-layout-lx3';
                    break;
            }

            // only check the valuation type on Step 1 & Step All
            if (service_id == 11 || service_id == 14) {
                if (!$('[name=lx_onsite_desktop]:checked').val()) {
                    messages.push('Please indicate if this is an onsite or desktop valuation');
                    fail = true;
                }
            }

            if (!$('[name=lx_ground_rent]', form_name).val().match("^£?[0-9]{1,3},?([0-9]{1,3},?)+(\\.?[0-9]{2}){0,1}$")) {
                messages.push('Please enter the ground rent value');
                fail = true;
            }
            if (!$('[name=lx_years_left]', form_name).val().match("^[0-9]{1,3}$")) {
                messages.push('Please enter the number of years left on the lease');
                fail = true;
            }

            // only check the mortgage on Step 3 & Step All
            if (service_id == 13 || service_id == 14) {
                if (!$('[name=lx_mortgage_on_prop]:checked', form_name).val()) {
                    messages.push('Please indicate if there is a mortgage');
                    fail = true;
                }
            }
        }


        function rquipValidateCCS() {
            if (!$('#edit-sco-price').val().match("^£?[0-9]{1,3},?([0-9]{1,3},?)+(\\.?[0-9]{2}){0,1}$")) {
                messages.push('Please enter the property value');
                fail = true;
            }
            if (!$('[name=sco_mortgage]:checked').val()) {
                messages.push('Please indicate if there is a mortgage');
                fail = true;
            }
            if (!$('[name=sco_tenure]:checked').val()) {
                messages.push('Please indicate the type of property');
                fail = true;
            }
        }

        function rquipValidateCCP() {
            if (!$('#edit-pco-price').val().match("^£?[0-9]{1,3},?([0-9]{1,3},?)+(\\.?[0-9]{2}){0,1}$")) {
                messages.push('Please enter the property value');
                fail = true;
            }
            if (!$('[name=pco_mortgage]:checked').val()) {
                messages.push('Please indicate if there is a mortgage');
                fail = true;
            }
            if (!$('[name=pco_tenure]:checked').val()) {
                messages.push('Please indicate the type of property');
                fail = true;
            }
            if (!$('[name=pco_tenanted]:checked').val()) {
                messages.push('Please indicate the type of tenancy');
                fail = true;
            }
        }

        function rquipValidateCCLXCreation() {
            if (!$('[name=lt_lease_length]:checked', '#form-layout-lease-creation').val()) {
                messages.push('Please indicate the length of lease');
                fail = true;
            }
        }

        function rquipValidateCCLXTransfer() {
            if (!$('[name=lt_business_acquisition]:checked', '#form-layout-lease-transfer').val()) {
                messages.push('Please indicate if there is a business acquisition involved');
                fail = true;
            }
        }
    });
})(jQuery);