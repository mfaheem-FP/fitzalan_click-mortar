(function ($) {
    $(document).ready(function () {
        $('#quotebuilder-menu-bar-1').click(function() {
            window.location='/quotebuilder';
        });

        /*Set defaults*/
        $('#edit-sco-mortgage-yes').attr('checked', 'checked');
        $('#edit-pco-mortgage-yes').attr('checked', 'checked');
        $('#edit-sco-tenure-freehold').attr('checked', 'checked');
        $('#edit-pco-tenure-freehold').attr('checked', 'checked');
        $('#edit-pco-help-no').attr('checked', 'checked');
        $('#edit-toe-mortgage-yes').attr('checked', 'checked');
        $('#edit-toe-many-mortgages-1').attr('checked', 'checked');

        $('#edit-toe-legal-work-no').attr('checked', 'checked'); 
        $('#edit-toe-money').val(0);
        $('#edit-rem-borrowing').val(1234);


        /*Bespoke fixes and classes for fancy inputs*/
        $(".form-type-radio").addClass("skinned-form-controls");
        $("#edit-sco-price").addClass("property-value-mask");
        $("#edit-pco-price").addClass("property-value-mask");
        $("#edit-rem-price").addClass("property-value-mask");
        $("#edit-rem-borrowing").addClass("property-value-mask");
        $("#edit-toe-price").addClass("property-value-mask");
        $("#edit-toe-money").addClass("property-value-mask");
        $("#form-layout-services h2").text("Which service can we quote you for?");
        $("#wrapper-service-id label[for='service-id']").text("Conveyancing for your");

        $('.form-type-radio label').each(function () {
            var currentTxt = $(this).html();
            var newTxt = '<span>' + currentTxt + '</span>';
            $(this).html(newTxt.replace(' Conveyancing', ''));

            var forAttr = $(this).attr('for');
            var lowerForAttr = forAttr.toLowerCase().replace(/\s/g, "-");
            $(this).attr('for', lowerForAttr);

        });

        $('.form-layout-group-inner label').each(function () {
            if (!$(this).hasClass("option")) {
                var currentTxt = $(this).html();
                //var newTxt = currentTxt + ':';
                var newTxt = currentTxt.replace('*', '<span class="form-required" title="This field is required.">*</span>');
                $(this).html(newTxt);
            }
        });

        $('#rquip-quote-form #edit-submit').addClass('green-button');

        $('.page-quotebuilder #rquip-quote-form #edit-submits').val("VIEW QUOTE");

        $('#rquip-quote-form #edit-submit').replaceWith('<div class="quote-arrow-box"><input type="submit" id="edit-submits" name="op" value="Calculate" class="green-button large-6 medium-12 small-12"><div class="quote-arrow-bg_tst"></div><div class="quote-arrow"></div><div class="quote-arrow-inner-black"></div><div class="quote-arrow-inner-green"></div><div class="quote-arrow-inner-box"></div></div><div class="privacy"><p>We guarantee your privacy.<br>Your information will not be shared.</p></div>');

        $('#rquip-instruct-form #edit-submit').addClass('green-button');

        $('#rquip-instruct-form #edit-submit').replaceWith('<div class="quote-arrow-box"><input type="submit" id="edit-submits" name="op" value="Confirm" class="green-button"><div class="quote-arrow-bg_tst"></div><div class="quote-arrow"></div><div class="quote-arrow-inner-black"></div><div class="quote-arrow-inner-green"></div><div class="quote-arrow-inner-box"></div></div><div class="privacy"><p>We guarantee your privacy.<br>Your information will not be shared.</p></div>');


      $('#wrapper-yde-telephone').append('<div class="form-item-person-telephone-gdpr gdpr form-textfield">' +
          '<span class="text">so we can call you to discuss your instruction</span>' +
          '</div>');

      $('#wrapper-yde-email').append('<div class="form-item-person-email gdpr form-textfield">' +
          '<span class="text">so we can email you regarding your instruction</span>' +
          '</div>');

    });
})(jQuery);


function addCommas(val) {
    val = val.replace(/,/g, "");
    var regEx = /(\d+)(\d{3})/;
    while (regEx.test(val)) {
        val = val.replace(regEx, '$1' + ',' + '$2');
    }
    return val;
}
;

function capitalizeMe(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}
;

