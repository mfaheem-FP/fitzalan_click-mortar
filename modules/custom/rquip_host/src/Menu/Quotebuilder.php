<?php

namespace Drupal\rquip_host\Menu;

use Drupal\Core\Controller\ControllerBase;
/**
 *
 */
class Quotebuilder extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function builderPage() {

    return [
        '#markup' => $this->t('Quotation Builder!'),
    ];
  }

  public function yourQuotePage() {

    return [
        '#markup' => $this->t('Your Quote'),
    ];
  }

}
