(function ($) {
  $(document).ready(function() {
    
    //loadIntegr8();
    
    $('.form-item-product .form-radio').click(function() {
      var item = $(this).attr('id').substr(13);
      build_form(item);
    });

/*TO ROTATE FIRST STAGE QUOTEBUILDER/OTHER SERVICES SLIDES ON LPs*/  
    $('#quotebuilder-menu-one').click(function() {
      $('#quotebuilder-menu-one').addClass('active');
      $('#quotebuilder-stepone-slideone').addClass('active');
      $('#quotebuilder-menu-two').removeClass('active');
      $('#quotebuilder-stepone-slidetwo').removeClass('active');
      $('#quotebuilder-menu-three').removeClass('active');
      $('#quotebuilder-stepone-slidethree').removeClass('active');
    });
    $('#quotebuilder-menu-two').click(function() {
      $('#quotebuilder-menu-one').removeClass('active');
      $('#quotebuilder-stepone-slideone').removeClass('active');
      $('#quotebuilder-menu-two').addClass('active');
      $('#quotebuilder-stepone-slidetwo').addClass('active');
      $('#quotebuilder-menu-three').removeClass('active');
      $('#quotebuilder-stepone-slidethree').removeClass('active');
    });
    $('#quotebuilder-menu-three').click(function() {
      $('#quotebuilder-menu-one').removeClass('active');
      $('#quotebuilder-stepone-slideone').removeClass('active');
      $('#quotebuilder-menu-two').removeClass('active');
      $('#quotebuilder-stepone-slidetwo').removeClass('active');
      $('#quotebuilder-menu-three').addClass('active');
      $('#quotebuilder-stepone-slidethree').addClass('active');
    });
    
    //var ValidPhone = $("#edit-quote-telephone");
    
    //$("#edit-phone").blur(function() {
    //  if($("#edit-phone").val().length > 10) 
    //  {
    //   IsValid($("#edit-phone").val());
    //  }
    //  else {
    //  $('#edit-phone-valid').val('false');
    //  }
    //});
    
/* TO UNHIDE EMAIL FIELD ON ASK A SOLICITOR */
    $('#edit-aas-question').click(function() {
      $('#quotebuilder-callback .form-item-aas-email').show();
      $('.quotebuilder-other-service .blue-button').css({
            'bottom' : '5px',
        });
     /* $('.page-conveyancing-solicitor #quotebuilder-callback').css({
            'height' : '280px',
        });*/
      if ($('#edit-aas-question').val() == "or type your question here") {
        $('#edit-aas-question').val('');
      }
    });
    $('#edit-aas-email').click(function() {
      if ($('#edit-aas-email').val() == "enter email address") {
        $('#edit-aas-email').val('');
      }
    });
    
    $('#edit-aas-question').click(function() {
      $('#quotebuilder-askasolicitor .form-item-aas-email').show();
      if ($('#edit-aas-question').val() == "or type your question here") {
        $('#edit-aas-question').val('');
      }
    });
    $('#edit-aas-email').click(function() {
      if ($('#edit-aas-email').val() == "enter email address") {
        $('#edit-aas-email').val('');
      }
    });

    $('#edit-aas-question--2').click(function() {
      $('#footer-contact').animate({height:'280px'}, 100);
      $('#footer .form-item-aas-email').delay(200).show();
      if ($('#edit-aas-question--2').val() == "or type your question here") {
        $('#edit-aas-question--2').val('');
      }
    });
    $('#edit-aas-email--2').click(function() {
      if ($('#edit-aas-email--2').val() == "enter email address") {
        $('#edit-aas-email--2').val('');
      }
    });
    
    /*ToE Rem quotebuilder element*/
    $('#toe-rem').hide();
    
    $("#edit-toe-mortgage").click(function () {
      if ($('input:radio[name=toe-mortgage]:nth(0)').is(':checked')) {
        $('#toe-rem').show();
      }
      else if ($('input:radio[name=toe-mortgage]:nth(1)').is(':checked')) {
        $('#toe-rem').hide();
      }
    });
    
    $('.popup').hide();
    
    $('#speed-popup-button').click(function () {
        $('#speed-popup').fadeIn();
        $('#grey-box').fadeIn();
	if ($(window).width() < 960) {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	}
    });
    
    $('#speed-popup-close').click(function () {
        $('#speed-popup').fadeOut();
        $('#grey-box').fadeOut();
    });
    
    $('#list-popup-button').click(function () {
        $('#list-popup').fadeIn();
        $('#grey-box').fadeIn();
        $("html, body").animate({ scrollTop: 0 }, "slow");
	if ($(window).width() < 960) {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	}
    });
    
    $('#list-popup-close').click(function () {
        $('#list-popup').fadeOut();
        $('#grey-box').fadeOut();
    });
    
    $('#cancel-popup-button').click(function () {
        $('#cancel-popup').fadeIn();
        $('#grey-box').fadeIn();
	if ($(window).width() < 960) {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	}
    });
    
    $('#cancel-popup-close').click(function () {
        $('#cancel-popup').fadeOut();
        $('#grey-box').fadeOut();
    });
    
    $('#priv-popup-button').click(function () {
        $('#priv-popup').fadeIn();
        $('#grey-box').fadeIn();
	if ($(window).width() < 960) {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	}
    });
    
    $('#priv-popup-close').click(function () {
        $('#priv-popup').fadeOut();
        $('#grey-box').fadeOut();
    });
    
    $('#grey-box').click(function () {
        $('#speed-popup').fadeOut();
        $('#list-popup').fadeOut();
        $('#cancel-popup').fadeOut();
        $('#priv-popup').fadeOut();
        $('#grey-box').fadeOut();
    });
    
/* QUOTEBUILDER Property Value */
/* NB - '\u00A3' is a pound sign */
$('.property-value-mask').each(function() {
    
    //Set default values
    if ($(this).val() == '') {
      $(this).val('e.g. \u00A3210,000');
    }

    if ($(this).val() == "e.g. \u00A3210,000") {
      $(this).css('color','#bbb');
    }
        
    $(this).keyup(function() {
      $(this).css('color','#000');
    });
    
    //Add pound sign
    $(this).focus(function() {
	if ($(this).val() == 'e.g. \u00A3210,000') {
	  $(this).val('');
	}
        if ($(this).val().indexOf('\u00A3') == -1) {
            $(this).val('\u00A3');
        }
    });
    
    //Numbers and "k" (as in 250k) only, NB "k" is keycode
    $(this).keydown(function(event) {
      // Allow special chars + arrows 
      if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
	  || event.keyCode == 27 || event.keyCode == 13 
	  || (event.keyCode == 65 && event.ctrlKey === true) 
	  || event.keyCode == 35 || event.keyCode == 36
	  || event.keyCode == 37 || event.keyCode == 38
	  || event.keyCode == 39 || event.keyCode == 40
	  || (event.keyCode == 75)){
	      return;
      }else {
	  // If it's not a number stop the keypress
	  if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	      event.preventDefault(); 
	  }   
      }
    });
    
    // Fancy commas
    $(this).keyup(function() {
        var newValue = addCommas($(this).val());
        $(this).val(addCommas(newValue));
        if (PropValueS.val().indexOf('\u00A3') == -1) {
            PropValueS.val('\u00A3' + PropValueS.val());
        }
    });

});

/* QUOTEBUILDER Postcode */
$('.postcode-mask').each(function() {
    
    //Set default values
    if ($(this).val() == '') {
      $(this).val('e.g. SW15');
    }

    if ($(this).val() == "e.g. SW15") {
      $(this).css('color','#bbb');
    }
        
    $(this).keyup(function() {
      $(this).css('color','#000');
    });
    
    $(this).focus(function() {
	if ($(this).val() == 'e.g. SW15') {
	  $(this).val('');
	}
    });
    
    //Letters only
//    $(this).keydown(function(event) {
//      // Allow special chars + arrows 
//      if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 32 || event.shiftKey
//	  || event.keyCode == 27 || event.keyCode == 13 
//	  || (event.keyCode == 65 && event.ctrlKey === true) 
//	  || (event.keyCode >= 35 && event.keyCode <= 39)
//	  || (event.keyCode == 75)){
//	      return;
//      }else {
//	  // If it's not a letter stop the keypress
//	  if ((event.keyCode < 65 || event.keyCode > 90)) {
//	      event.preventDefault(); 
//	  }   
//      }
//    });
    
    // Proper case
    $(this).keyup(function() {
        var newValue = capitalizeMe($(this).val());
        $(this).val(newValue.toUpperCase());
    });
    

});

/* QUOTEBUILDER Name */
$('.name-mask').each(function() {
    
    //Set default values
    if ($(this).val() == '') {
      $(this).val('e.g. Jane Cook');
    }

    if ($(this).val() == "e.g. Jane Cook") {
      $(this).css('color','#bbb');
    }
        
    $(this).keyup(function() {
      $(this).css('color','#000');
    });
    
    $(this).focus(function() {
	if ($(this).val() == 'e.g. Jane Cook') {
	  $(this).val('');
	}
    });
    
    //Letters only
    $(this).keydown(function(event) {
      // Allow special chars + arrows 
      if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 32 || event.shiftKey
	  || event.keyCode == 27 || event.keyCode == 13 
	  || (event.keyCode == 65 && event.ctrlKey === true) 
	  || event.keyCode == 35 || event.keyCode == 36
	  || event.keyCode == 37 || event.keyCode == 38
	  || event.keyCode == 39 || event.keyCode == 40
	  || (event.keyCode == 75)){
	      return;
      }else {
	  // If it's not a letter stop the keypress
	  if ((event.keyCode < 65 || event.keyCode > 90)) {
	      event.preventDefault(); 
	  }   
      }
    });
    
    // Proper case
    $(this).blur(function() {
        var newValue = capitalizeMe($(this).val());
        $(this).val(capitalizeMe(newValue));
    });
    

});

/* QUOTEBUILDER Phone */
$('.phone-mask').each(function() {
    
    //Set default values
    if ($(this).val() == '') {
      $(this).val('e.g. 0330 660 0453');
    }

    if ($(this).val() == "e.g. 0330 660 0453") {
      $(this).css('color','#bbb');
    }
        
    $(this).keyup(function() {
      $(this).css('color','#000');
    });
    
    $(this).focus(function() {
	if ($(this).val() == 'e.g. 0330 660 0453') {
	  $(this).val('');
	}
    });
    
    //Numbers only
    $(this).keydown(function(event) {
      // Allow special chars + arrows 
      if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 32 || event.shiftKey
	  || event.keyCode == 27 || event.keyCode == 13 
	  || (event.keyCode == 65 && event.ctrlKey === true) 
	  || event.keyCode == 35 || event.keyCode == 36
	  || event.keyCode == 37 || event.keyCode == 38
	  || event.keyCode == 39 || event.keyCode == 40
	  || event.keyCode == 219 || event.keyCode == 221){
	      return;
      }else {
	  // If it's not a number stop the keypress
	  if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	      event.preventDefault(); 
	  }   
      }
    });
    
});

/* QUOTEBUILDER Email */
$('.email-mask').each(function() {
    
    //Set default values
    if ($(this).val() == '') {
      $(this).val('e.g. jane@yahoo.com');
    }

    if ($(this).val() == "e.g. jane@yahoo.com") {
      $(this).css('color','#bbb');
    }
        
    $(this).keyup(function() {
      $(this).css('color','#000');
    });
    
    $(this).focus(function() {
	if ($(this).val() == 'e.g. jane@yahoo.com') {
	  $(this).val('');
	}
    });
    
});

//$('#edit-submit').mouseover(function() {
//  ////recheckphone
//  if($(".phone-mask").val().length > 10 && $(".phone-mask").val() != 'e.g. 0330 660 0453')
//  //if($("#edit-quote-telephone").val().length > 10) 
//  {
//    IsValid($(".phone-mask").val());
//  }
//  //recheckemail
//  if($(".email-mask").val().indexOf("@") != -1 && $(".email-mask").val().indexOf(".") != -1 && $(".email-mask").val() != 'e.g. jane@yahoo.com')
//  //if($("#edit-quote-email").val().indexOf("@") != -1 && $("#edit-quote-email").val().indexOf(".") != -1)
//  {
//    IsValidEmail($(".email-mask").val(),'Address');
//  }
//});
//
//$('#edit-submit').focus(function() {
//  ////recheckphone
//  if($(".phone-mask").val().length > 10 && $(".phone-mask").val() != 'e.g. 0330 660 0453')
//  //if($("#edit-quote-telephone").val().length > 10) 
//  {
//    IsValid($(".phone-mask").val());
//  }
//  //recheckemail
//  if($(".email-mask").val().indexOf("@") != -1 && $(".email-mask").val().indexOf(".") != -1 && $(".email-mask").val() != 'e.g. jane@yahoo.com')
//  //if($("#edit-quote-email").val().indexOf("@") != -1 && $("#edit-quote-email").val().indexOf(".") != -1)
//  {
//    IsValidEmail($(".email-mask").val(),'Address');
//  }
//});

//////END OF FORM VALIDATION

    
  });
  
  function build_form(item) {
    $('#edit-sale').removeClass('hide-item');
    $('#edit-purchase').removeClass('hide-item');
    $('#edit-transfer-of-equity').removeClass('hide-item');
    $('#edit-remortgage').removeClass('hide-item');
    
    $('#edit-sale').removeClass('show-item');
    $('#edit-purchase').removeClass('show-item');
    $('#edit-transfer-of-equity').removeClass('show-item');
    $('#edit-remortgage').removeClass('show-item');
    
    switch (item) {
      case '0':
        /* Sale */
        $('#edit-sale').addClass('show-item');
        $('#edit-purchase').addClass('hide-item');
        $('#edit-transfer-of-equity').addClass('hide-item');
        $('#edit-remortgage').addClass('hide-item');
        break;
      case '1':
        /* Purchase */
        $('#edit-sale').addClass('hide-item');
        $('#edit-purchase').addClass('show-item');
        $('#edit-transfer-of-equity').addClass('hide-item');
        $('#edit-remortgage').addClass('hide-item');
        break;
      case '2':
        /* Sale & Purchase */
        $('#edit-sale').addClass('show-item');
        $('#edit-purchase').addClass('show-item');
        $('#edit-transfer-of-equity').addClass('hide-item');
        $('#edit-remortgage').addClass('hide-item');
        break;
      case '3':
        /* Transfer of Equity */
        $('#edit-sale').addClass('hide-item');
        $('#edit-purchase').addClass('hide-item');
        $('#edit-transfer-of-equity').addClass('show-item');
        $('#edit-remortgage').addClass('hide-item');
        break;
      case '4':
        /* Remortgage */
        $('#edit-sale').addClass('hide-item');
        $('#edit-purchase').addClass('hide-item');
        $('#edit-transfer-of-equity').addClass('hide-item');
        $('#edit-remortgage').addClass('show-item');
        break;
    }
  }
  
	/* NEW INSTRUCT = BC */
	
	if ($('#edit-instruct-sellprop-y').is(':checked')) $('#instruct-sell-prop').hide();
	$('#edit-instruct-sellprop-y').click(function() {
		if ($('#edit-instruct-sellprop-y').is(':checked')) {
			$('#edit-instruct-sellprop-n').removeAttr('checked');
			$('#instruct-sell-prop').hide();
		}
		else {
			$('#edit-instruct-sellprop-n').attr('checked','checked');
			$('#instruct-sell-prop').show();
		}
	});
	
	$('#edit-instruct-sellprop-n').click(function() {
		if ($('#edit-instruct-sellprop-n').is(':checked')) {
			$('#edit-instruct-sellprop-y').removeAttr('checked');
			$('#instruct-sell-prop').show();
		}
		else {
			$('#edit-instruct-sellprop-y').attr('checked','checked');
			$('#instruct-sell-prop').hide();
		}
	});
  
  if ($('#edit-instruct-purprop-0').is(':checked')) $('#instruct-buy-prop').hide();
	$('#edit-instruct-purprop-0').click(function() {
		if ($('#edit-instruct-purprop-0').is(':checked')) {
			$('#edit-instruct-purprop-1').removeAttr('checked');
			$('#instruct-buy-prop').hide();
		}
		else {
			$('#edit-instruct-purprop-1').attr('checked','checked');
			$('#instruct-buy-prop').show();
		}
	});
	
	$('#edit-instruct-purprop-1').click(function() {
		if ($('#edit-instruct-purprop-1').is(':checked')) {
			$('#edit-instruct-purprop-0').removeAttr('checked');
			$('#instruct-buy-prop').show();
		}
		else {
			$('#edit-instruct-purprop-0').attr('checked','checked');
			$('#instruct-buy-prop').hide();
		}
	});
  
//loadIntegr8();
//loadIntegr8Email();
  
})(jQuery);

//function loadIntegr8() {
//  // Load the TelephoneValidation Integr8 service
//  data8.load('TelephoneValidation');
//}
//
//function IsValid(number) {
//  /// <param name="number">string</param>
//  var telephonevalidation = new data8.telephonevalidation();
//  
//  telephonevalidation.isvalid(
//    number,
//    null,
//    showIsValidResult
//  );
//}
//
//function showIsValidResult(result) {
//  // Check that the call succeeded, and show the error message if there was a problem.
//  if (!result.Status.Success) {
// }
//  else {
//jQuery('#edit-phone-valid').val(result.Result);
//  }
//}

///*Telephone Validation*/

function loadIntegr8() {
  // Load the TelephoneValidation Integr8 service
  data8.load('TelephoneValidation');
}
 
function IsValid(number) {
  /// <param name="number">string</param>
  var telephonevalidation = new data8.telephonevalidation();
   
  telephonevalidation.isvalid(
    number,
    null,
    showIsValidResult
  );
}
 
function showIsValidResult(result) {
  // Check that the call succeeded, and show the error message if there was a problem.
  if (!result.Status.Success) {
    //alert('Error: ' + result.Status.ErrorMessage);
    //jQuery('#edit-quote-valid-phone').val(result.Status.ErrorMessage);
  }
  else {
    // TODO: Process method results here.
    // Results can be extracted from the following fields:
    // result.Result
    //   Indicates if the telephone number is found to be valid.
    jQuery('#edit-phone-valid').val(result.Result);
//      if (result.Result == 'true') {
//	jQuery('#edit-quote-valid-phone').val(val + 1);
//      }
  }
}

function loadIntegr8Email() {
  // Load the TelephoneValidation Integr8 service
  data8.load('EmailValidation');
}

function IsValidEmail(email,level) {
  /// <param name="email">string</param>
  /// <param name="level">string. One of the following values: Syntax, MX, Server, Address</param>
  var emailvalidation = new data8.emailvalidation();
   
  emailvalidation.isvalid(
    email,
    level,
    null,
    showIsValidEmailResult
  );
}

function showIsValidEmailResult(result) {
  // Check that the call succeeded, and show the error message if there was a problem.
  if (!result.Status.Success) {
    //alert('Error: ' + result.Status.ErrorMessage);
    jQuery('#edit-email-valid').val(result.Status.ErrorMessage);
  }
  else {
    // TODO: Process method results here.
    // Results can be extracted from the following fields:
    // result.Result
    //   Contains a status code indicating if the email address could be validated.
    jQuery('#edit-email-valid').val(result.Result);
//      if (result.Result == 'true') {
//	jQuery('#edit-quote-valid-phone').val(val + 1);
//      }
  }
}

function addCommas(val) {
val = val.replace(/,/g, "");
var regEx = /(\d+)(\d{3})/;
while (regEx.test(val)) {
    val = val.replace(regEx, '$1' + ',' + '$2');
}
return val;
};

function capitalizeMe(str) {
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
};