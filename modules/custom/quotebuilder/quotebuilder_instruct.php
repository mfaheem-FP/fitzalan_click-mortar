<?php

/**
 * @file
 * @file
 * @file
 * .*/

/**
 *
 */
function quotebuilder_instruct($identifier = NULL, $type = NULL) {
  $GLOBALS['conf']['cache'] = FALSE;
  unset($_SESSION['quotebuilder']);
  $valid_input = quotebuilder_valid_input($identifier, $type);

  if ($valid_input && !is_null($identifier) && !is_null($type)) {
    $table = NULL;
    $fields = array();
    $title = NULL;
    switch ($type) {
      case 'toe':
        $table = 'quip_toe_quotes';
        $fields = array('qid', 'date', 'fullname', 'email', 'telephone', 'toepropvalue', 'toechangehands', 'toemortgage', 'toemanymort', 'toelegal', 'discount', 'quote', 'uid');
        $title = 'Instruct us for transfer of equity';

        break;

      case 'rem':
        $table = 'quip_rem_quotes';
        $fields = array('qid', 'date', 'fullname', 'email', 'telephone', 'renortprice', 'renortborrow', 'discount', 'quote', 'uid');
        $title = 'Instruct us for remortgage';

        break;

      case 'sco':
        $table = 'quip_con_quotes';
        $fields = array('qid', 'date', 'fullname', 'email', 'telephone', 'saleprice', 'salemortgage', 'saletenure', 'purchaseprice', 'purchasemortgage', 'purchasetenure', 'discount', 'quotedamountsale', 'quotedamountpurchase', 'uid');
        $title = 'Instruct us for sale conveyancing';

        break;

      case 'pco':
        $table = 'quip_con_quotes';
        $fields = array('qid', 'date', 'fullname', 'email', 'telephone', 'saleprice', 'salemortgage', 'saletenure', 'purchaseprice', 'purchasemortgage', 'purchasetenure', 'discount', 'quotedamountsale', 'quotedamountpurchase', 'uid');
        $title = 'Instruct us for purchase conveyancing';

        break;

      case 'spc':
        $table = 'quip_con_quotes';
        $fields = array('qid', 'date', 'fullname', 'email', 'telephone', 'saleprice', 'salemortgage', 'saletenure', 'purchaseprice', 'purchasemortgage', 'purchasetenure', 'discount', 'quotedamountsale', 'quotedamountpurchase', 'uid');
        $title = 'Instruct us for sale and purchase conveyancing';

        break;
    }

    if (is_null($table) || empty($fields)) {
      return drupal_goto('<front>');
    }

    $query = db_select($table, 't');
    $query->fields('t');
    $query->condition('qid', $identifier);
    $query->range(0, 1);
    $result = $query->execute()->fetchAssoc();

    $keys = array_keys($result);
    sort($keys);
    sort($fields);
    if ($keys != $fields) {
      return drupal_goto('<front>');
    }

    $clean_quote = quotebuilder_sanitize($result);
    $instruct_form = 'quotebuilder_instruct_1_form';

    switch ($type) {
      case 'toe':
        $summary = '<table>
                      <tr><th colspan="2">Transfer of Equity Quote Detail</th></tr>
                      <tr><td>Property Value</td><td class="feevalue">&pound;' . number_format($clean_quote['toepropvalue'], 0) . '</td></tr>
                      <tr><td>Amount of money changing hands</td><td class="feevalue">&pound;' . number_format($clean_quote['toechangehands'], 0) . '</td></tr>
                      <tr><td>Remortgage legal work include</td><td class="feevalue">' . $clean_quote['toelegal'] . '</td></tr>
                      </table>';
        break;

      case 'rem':
        $summary = '<table>
                      <tr><th colspan="2">Remortgage Quote Detail</th></tr>
                      <tr><td>Property Value</td><td class="feevalue">&pound;' . number_format($clean_quote['renortprice'], 0) . '</td></tr>
                      <tr><td>Remortgage Value</td><td class="feevalue">&pound;' . number_format($clean_quote['renortborrow'], 0) . '</td></tr>
                      </table>';
        break;

      case 'sco':
        $summary = '<table>
                      <tr><th colspan="2">Sale Conveyancing Quote Detail</th></tr>
                      <tr><td>Price</td><td class="feevalue">&pound;' . number_format($clean_quote['quotedamountsale'], 0) . '</td></tr>
                      <tr><td>Tenure</td><td class="feevalue">' . $clean_quote['saletenure'] . '</td></tr>
                      <tr><td>Mortgage</td><td class="feevalue">' . ucfirst($clean_quote['salemortgage']) . '</td></tr>
                      </table>';
        break;

      case 'pco':
        $summary = '<table>
                      <tr><th colspan="2">Purchase Conveyancing Quote Detail</th></tr>
                      <tr><td>Price</td><td class="feevalue">&pound;' . number_format($clean_quote['quotedamountpurchase'], 0) . '</td></tr>
                      <tr><td>Tenure</td><td class="feevalue">' . $clean_quote['purchasetenure'] . '</td></tr>
                      <tr><td>Mortgage</td><td class="feevalue">' . ucfirst($clean_quote['purchasemortgage']) . '</td></tr>
                      </table>';
        break;

      case 'spc':
        $summary = '<table>
                      <tr><th colspan="2">Sale Conveyancing Quote Detail</th><th colspan="2">Purchase Conveyancing Quote Detail</th></tr>
                      <tr><td>Price</td><td class="feevalue">&pound;' . number_format($clean_quote['quotedamountsale'], 0) . '</td><td class="feevalue">Price</td><td class="feevalue">&pound;' . number_format($clean_quote['quotedamountpurchase'], 0) . '</td></tr>
                      <tr><td>Tenure</td><td class="feevalue">' . $clean_quote['saletenure'] . '</td><td class="feevalue">Tenure</td><td class="feevalue">' . $clean_quote['purchasetenure'] . '</td></tr>
                      <tr><td>Mortgage</td><td class="feevalue">' . ucfirst($clean_quote['salemortgage']) . '</td><td class="feevalue">Mortgage</td><td class="feevalue">' . ucfirst($clean_quote['purchasemortgage']) . '</td></tr>
                      </table>
                      ';
        break;
    }

    $_SESSION['quotebuilder']['quoteform']['quote'] = $clean_quote;
    $_SESSION['quotebuilder']['quoteform']['type'] = $type;

    $output = '<div class="instruct-page">';
    $output .= drupal_render(drupal_get_form($instruct_form, $clean_quote, $summary, $title, $type));
    $output .= '</div>';

    $output .= '<div id="quotebuilder-steptwo-other-services" class="columns large-4 medium-4 small-12">
            <div id="quotebuilder-whychoose" class="light-blue columns large-12 medium-12 small-12">
          <h2>Your quote includes:</h2>
          <ul>
            <li>Local expertise</li>
            <li>All legal fees</li>
            <li class="popup-button">Completion to your timetable</li>
            <li>No completion, no fee</li>
            <li>Fixed fee</li>
          </ul>
            </div>
            <div id="quotebuilder-needspeed" class="green-bordered">
        <div id="needspeed-inner">
          <p class="block-header">Need to complete<br />
          <span>faster?</span></p>
          <span id="speed-popup-button" class="black-button">What you need to do</span>
        </div>
            </div>
            <div id="quotebuilder-callback" class="quotebuilder-other-service">
        <div id="quotebuilder-callback-inner">
          <p class="block-header">Prefer to Talk?<br />
          <span><a href="tel:08000386699">0800 038 6699</a></span></p>
          <a href="/contact" class="black-button hide-for-small hide-for-medium">Request a Callback</a>
          <a class="black-button hide-for-large" data-reveal-id="myModal" href="#">Request a Callback <span>»</span></a>
        </div>
            </div><div class="hide-for-small hide-for-medium">
            <div id="quotebuilder-askasolicitor" class="quotebuilder-other-service">
        <p class="block-header">Ask a Solicitor</p>
        ' . drupal_render(drupal_get_form('quotebuilder_askasolicitor_form')) . '
            </div><div>
          </div>';
    return $output;
  }
  else {
    return drupal_goto('<front>');
  }
}

/**
 * .
 */
function quotebuilder_valid_input($identifier, $type) {
  $id_bool = preg_match('|(?m-Usi)^[A-Za-z0-9-]*$|', $identifier);
  $type_bool = preg_match('/(?m-Usi)^toe$|^rem$|^sco$|^pco$|^spc$/', $type);
  if ($id_bool && $type_bool) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * .
 */
function quotebuilder_sanitize($array) {
  foreach ($array as $key => $value) {
    $array[$key] = check_plain($value);
  }
  return $array;
}

/**
 * Forms.
 */
function quotebuilder_instruct_1_form($form, &$form_state, $clean_quote, $summary, $title, $type) {
  $form = array();

  $form['test'] = array(
    '#markup' => '<h1>' . $title . '</h1>
			<div class="instruct-summary light-green-bordered">
				' . $summary . '
			</div>
			<h2>When would you like to complete your move by?</h2>',
  );

  $format = 'Y-m-d';
  $form['instruct_completiondate'] = array('#prefix' => '<div class="subsection light-green-bordered">', '#type' => 'date_popup', '#title' => t('Enter preferred completion date'), '#date_format' => $format, '#default_value' => date("Y-m-d", (time() + (60 * 60 * 24 * 14))), '#date_year_range' => '0:+3', '#size' => 20, '#required' => TRUE, '#suffix' => '</div>');

  $name = explode(' ', $clean_quote['fullname']);
  $first = $name[0];
  unset($name[0]);
  $last = implode(' ', $name);

  $form['instruct_name1'] = array(
      '#prefix' => '<h2>Contact Details</h2><div class="subsection light-green-bordered">',
      '#type' => 'textfield',
      '#title' => t('First name'),
      '#default_value' => $first,
      '#size' => 60,
      '#required' => TRUE
  );

  $form['instruct_surname1'] = array(
      '#type' => 'textfield',
      '#title' => t('Last name'),
      '#default_value' => $last,
      '#size' => 60,
      '#required' => TRUE
  );

  $form['instruct_number1'] = array(
      '#type' => 'textfield',
      '#title' => t('Preferred contact number'),
      '#default_value' => $clean_quote['telephone'],
      '#size' => 60,
      '#required' => TRUE
  );

  $form['instruct_number2'] = array(
      '#type' => 'textfield',
      '#title' => t('Alternative contact number'),
      '#default_value' => NULL,
      '#size' => 60
  );

  $form['instruct_email1'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#default_value' => $clean_quote['email'],
      '#size' => 60,
      '#required' => TRUE,
      '#suffix' => '</div>'
  );

  $form['h_information2'] = array(
      '#markup' => '<h2>Your Current Address</h2><div class="subsection light-green-bordered">'
  );

  $form['instruct_address1_0'] = array(
      '#type' => 'textfield',
      '#title' => t('Flat number'),
      '#default_value' => NULL,
      '#size' => 10
  );

  $form['instruct_address1_1'] = array(
      '#type' => 'textfield',
      '#title' => t('House name or number'),
      '#default_value' => NULL,
      '#size' => 60,
      '#required' => TRUE
  );

  $form['instruct_address1_2'] = array(
      '#type' => 'textfield',
      '#title' => t('Street/Road'),
      '#default_value' => NULL,
      '#size' => 60,
      '#required' => TRUE
  );

  $form['instruct_address1_3'] = array(
      '#type' => 'textfield',
      '#title' => t('Town/City'),
      '#default_value' => NULL,
      '#size' => 60,
      '#required' => TRUE
  );

  $form['instruct_address1_4'] = array(
      '#type' => 'textfield',
      '#title' => t('County'),
      '#default_value' => NULL,
      '#size' => 60,
      '#required' => TRUE
  );

  $form['instruct_postcode1'] = array(
      '#type' => 'textfield',
      '#title' => t('Postcode'),
      '#default_value' => NULL,
      '#size' => 10,
      '#required' => TRUE,
      '#suffix' => '</div>'
  );

  if ($type == 'sco' || $type == 'spc') {
    $options = array('Y' => 'Yes', 'N' => 'No');
    $form['instruct_sellprop'] = array(
        '#type' => 'checkboxes',
        '#options' => $options,
        '#title' => 'Is this the property you are <span class="emph">selling</span>?',
        '#default_value' => array('Y'),
        '#attributes' => array('class' => array('skinned-form-controls'))
    );
  }

  if ($type == 'sco' || $type == 'spc') {
    $form['h_information56'] = array('#markup' => '<div id="instruct-sell-prop"><h2>About the property you are selling</h2><div class="subsection light-green-bordered">');

    $flatnumber = NULL;
    $house = NULL;
    $street = NULL;
    $town = NULL;
    $county = NULL;
    $postcode = NULL;

    $form['instruct_sp_address1_0'] = array(
        '#type' => 'textfield',
        '#title' => t('Flat number'),
        '#default_value' => $flatnumber,
        '#size' => 10
    );

    $form['instruct_sp_address1_1'] = array(
        '#type' => 'textfield',
        '#title' => t('House name or number'),
        '#default_value' => $house,
        '#size' => 60,
        '#required' => FALSE
    );

    $form['instruct_sp_address1_2'] = array(
        '#type' => 'textfield',
        '#title' => t('Street/Road'),
        '#default_value' => $street,
        '#size' => 60,
        '#required' => FALSE
    );

    $form['instruct_sp_address1_3'] = array(
        '#type' => 'textfield',
        '#title' => t('Town/City'),
        '#default_value' => $town,
        '#size' => 60,
        '#required' => FALSE
    );

    $form['instruct_sp_address1_4'] = array(
        '#type' => 'textfield',
        '#title' => t('County'),
        '#default_value' => $county,
        '#size' => 60,
        '#required' => FALSE
    );

    $form['instruct_sp_postcode1'] = array(
        '#type' => 'textfield',
        '#title' => t('Postcode'),
        '#default_value' => $postcode,
        '#size' => 10,
        '#required' => FALSE
    );

    $form['h_information57b'] = array('#markup' => '</div></div>');

    $holds = array(
        'freehold' => t('Freehold'),
        'leasehold' => t('Leasehold'),
        'sharehold' => t('Share of Freehold')
    );

    if ($type == 'sco') {
      $form['h_information57'] = array('#markup' => '<h2>Additional information</h2><div class="subsection light-green-bordered">');

      $form['instruct_sale_anythingelse'] = array('#type' => 'textarea', '#title' => t('Anything else you need us to know?'), '#cols' => 22, '#rows' => 5, '#resizable' => FALSE, '#suffix' => '</div>');
    }
  }
  if ($type == 'pco' || $type == 'spc') {
    $form['instruct_purprop'] = array('#type' => 'checkboxes', '#options' => array('<span> I\'m not sure</span>', '<span> Yes</span>'), '#title' => t('Do you know the full address of the property you are <span class="emph">buying</span>?'), '#default_value' => array(0), '#attributes' => array('class' => array('skinned-form-controls')));
    // }.
    // If ($type == 'pco' || $type == 'spc') {.
    $form['h_information58'] = array(
        '#markup' => '<div id="instruct-buy-prop"><h2>About the property you are buying</h2><div class="subsection light-green-bordered">'
    );

    $form['instruct_pp_address1_0'] = array(
        '#type' => 'textfield',
        '#title' => t('Flat number'),
        '#default_value' => NULL,
        '#size' => 10
    );

    $form['instruct_pp_address1_1'] = array(
        '#type' => 'textfield',
        '#title' => t('House name or number'),
        '#default_value' => NULL,
        '#size' => 60,
        '#required' => FALSE
    );

    $form['instruct_pp_address1_2'] = array(
        '#type' => 'textfield',
        '#title' => t('Street/Road'),
        '#default_value' => NULL,
        '#size' => 60,
        '#required' => FALSE
    );

    $form['instruct_pp_address1_3'] = array(
        '#type' => 'textfield',
        '#title' => t('Town/City'),
        '#default_value' => NULL,
        '#size' => 60,
        '#required' => FALSE
    );

    $form['instruct_pp_address1_4'] = array(
        '#type' => 'textfield',
        '#title' => t('County'),
        '#default_value' => NULL,
        '#size' => 60,
        '#required' => FALSE
    );

    $form['instruct_pp_postcode1'] = array(
        '#type' => 'textfield',
        '#title' => t('Postcode'),
        '#default_value' => NULL,
        '#size' => 10,
        '#required' => FALSE
    );

    $form['h_information57c'] = array('#markup' => '</div></div>');

    $holds = array(
        'freehold' => t('Freehold'),
        'leasehold' => t('Leasehold'),
        'sharehold' => t('Share of Freehold')
    );

    $form['h_information59'] = array(
        '#markup' => '<h2>Additional information</h2><div class="subsection light-green-bordered">'
    );

    $form['instruct_purchase_anythingelse'] = array(
        '#type' => 'textarea',
        '#title' => t('Anything else you need us to know?'),
        '#cols' => 22,
        '#rows' => 5,
        '#resizable' => FALSE,
        '#suffix' => '</div>'
    );
  }

  $format = 'Y-m-d H:i';
  $default_callback = date("Y-m-d H:i", ((time() - (date('i') * 60)) + (60 * 60)));
  $form['instruct_callbackdate'] = array('#prefix' => '<h2>When would you like us to call you?</h2><div class="subsection light-green-bordered">', '#type' => 'date_popup', '#title' => t('Enter date and time'), '#default_value' => $default_callback, '#date_year_range' => '0:+3', '#size' => 20, '#required' => TRUE, '#suffix' => '</div>');

  $tandc = '/disclaimer';

  if ($type == 'sco' || $type == 'pco' || $type == 'spc') {
    $form['instruct_confirm'] = array(
        '#type' => 'checkbox',
        '#required' => TRUE,
        '#title' => t('I accept the <span id="sollis-button-tandcs">terms and conditions</span>'),
        '#attributes' => array('class' => array('skinned-form-controls'))
    );
  }

  $form['next'] = array(
    '#type' => 'submit',
    '#value' => 'Instruct',
    '#prefix' => '<div class="quote-arrow-box">',
    '#attributes' => array('class' => array('green-button')),
    '#suffix' => '<div class="quote-arrow-bg"></div>
                  <div class="quote-arrow"></div><div class="quote-arrow-inner-black"></div>
                  <div class="quote-arrow-inner-green"></div><div class="quote-arrow-inner-box"></div></div>
                  <div class="privacy"><a target="_blank" href="/privacy-statement-and-data-protection"><p>We guarantee your privacy.<br />Your information will not be shared.</p></a></div>
			<h2>Terms</h2>
			<p>Homeward Legal comply with, and are subject to, the Solicitors\' Code of Conduct 2011 published by the Solicitors Regulation Authority (SRA). Solicitors belonging to the Homeward Legal panel are regulated by the SRA. This quote is based on the information you supply, so if information is incorrect or changes you should let us know immediately. Quotes are valid for 30 days from the date the quote was obtained. Quotes are only valid for properties in England and Wales. Your panel Solicitor will require a non refundable deposit of one hundred and forty pounds inclusive of vat to begin work on your file. Our \'No Completion No Fee\' guarantee means that in the unlikely event you fail to complete, you will only pay your Solicitor for disbursement costs incurred on your behalf. Your payment on account will not be refunded, and the deposit can be used on any future conveyance with the same solicitor. This quote is subject to our standard terms and conditions. Homeward Legal is a trading name of Fitzalan Partners Limited. A full list of offices including our registered and head office are available on our \'About Us\' page.</p>
    		</div>',
  );

  return $form;
}

/**
 *
 */
function quotebuilder_instruct_1_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['instruct_email1'])) {
    form_set_error('instruct_email1', "Please give a valid email address");
  }
  if (empty($form_state['values']['instruct_number1'])) {
    form_set_error('instruct_number1', "Please enter a telephone number");
  }
  if (isset($form_state['values']['instruct_confirm']) && !$form_state['values']['instruct_confirm']) {
    form_set_error('instruct_confirm', "<p><b>Please confirm you have read and understood the terms and conditions, and understood that you are instructing Fridays to undertake chargeable work on your property transaction(s).</b></p>");
  }
}

/**
 *
 */
function quotebuilder_instruct_1_form_submit($form, &$form_state) {
  $_SESSION['quotebuilder']['instructform_1'] = $form_state['values'];
  if ($_SESSION['quotebuilder']['quoteform']['type'] == 'sco' || $_SESSION['quotebuilder']['quoteform']['type'] == 'pco' || $_SESSION['quotebuilder']['quoteform']['type'] == 'spc') {
    drupal_goto('instruct/complete');
  }
  if ($_SESSION['quotebuilder']['quoteform']['type'] == 'toe' || $_SESSION['quotebuilder']['quoteform']['type'] == 'rem') {
    drupal_goto('instruct/instruct_remtoe');
  }
}

/**
 *
 */
function quotebuilder_instruct_4() {
  $strVendorTxCode = $_SESSION['quotebuilder']['quoteform']['quote']['qid'] . '-i';
  // Logic to populate db with current address if property selling = yes.
  if (isset($_SESSION['quotebuilder']['instructform_1']['instruct_sellprop']) && $_SESSION['quotebuilder']['instructform_1']['instruct_sellprop']['Y'] === 'Y') {
    $sellingflatnumber = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0']);
    $sellinghousenumber = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1']);
    $sellingstreet = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2']);
    $sellingtown = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3']);
    $sellingcounty = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4']);
    $sellingpostcode = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1']);
  }
  elseif (isset($_SESSION['quotebuilder']['instructform_1']['instruct_sp_address1_1']) && isset($_SESSION['quotebuilder']['instructform_1']['instruct_sp_postcode1'])) {
    $sellingflatnumber = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_sp_address1_0']);
    $sellinghousenumber = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_sp_address1_1']);
    $sellingstreet = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_sp_address1_2']);
    $sellingtown = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_sp_address1_3']);
    $sellingcounty = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_sp_address1_4']);
    $sellingpostcode = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_sp_postcode1']);
  }
  else {
    $sellingflatnumber = '';
    $sellinghousenumber = '';
    $sellingstreet = '';
    $sellingtown = '';
    $sellingcounty = '';
    $sellingpostcode = '';
  }

  if (isset($_SESSION['quotebuilder']['instructform_1']['instruct_pp_address1_0']) && isset($_SESSION['quotebuilder']['instructform_1']['instruct_pp_postcode1'])) {
    $buyingflatnumber = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_pp_address1_0']);
    $buyinghousenumber = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_pp_address1_1']);
    $buyingstreet = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_pp_address1_2']);
    $buyingtown = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_pp_address1_3']);
    $buyingcounty = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_pp_address1_4']);
    $buyingpostcode = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_pp_postcode1']);
  }
  else {
    $buyingflatnumber = '';
    $buyinghousenumber = '';
    $buyingstreet = '';
    $buyingtown = '';
    $buyingcounty = '';
    $buyingpostcode = '';
  }
  // End db population logic if current address matches property selling.
  $output = '<div id="coninstruct-results"><h2>A copy of this has been emailed to ' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</h2>';
  if ($_SESSION['quotebuilder']['quoteform']['type'] == 'spc') {
    $output .= '<p>Thank you for instructing a Homeward Legal solicitor. We will shortly contact you with the details of the solicitor who will be acting on your behalf to complete the conveyancing for the sale of ' . $sellinghousenumber . ' ' . $sellingstreet . ' and the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . '.</p>
      <h2>Your Details</h2>
      <p>Please check the following details and contact us on <a href="tel:03306600453">0330 660 0453</a> (local call rate from a land line or mobile) if there are any errors, or this information changes:</p>
      <div class="tablebg">
        <table>
          <tbody>
            <tr>
              <td class="tabletitle"><b>Client name acting for:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '</td>
            </tr>
            
            <tr>
              <td><b>Tel no:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_number1'] . '</td>
            </tr>
            <tr>
              <td><b>2nd Tel No:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_number2'] . '</td>
            </tr>
            <tr>
              <td><b>Email:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</td>
            </tr>
            <tr>
              <td><b>Correspondence address:</b></td>
              <td valign="top">' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'] . '</td>
            </tr>
            <tr>
              <td><b>Property being sold:</b></td>
              <td valign="top">' . $sellingflatnumber . '<br />
                            ' . $sellinghousenumber . '<br />
                            ' . $sellingstreet . '<br />
                            ' . $sellingtown . '<br />
                            ' . $sellingcounty . '<br />
                            ' . $sellingpostcode . '</td>
            </tr>
            <tr>
              <td><b>Property value:</b></td>
              <td>&pound;' . $_SESSION['quotebuilder']['quoteform']['quote']['saleprice'] . '</td>
            </tr>
            <tr>
              <td><b>Property tenure:</b></td>
              <td>' . $_SESSION['quotebuilder']['quoteform']['quote']['saletenure'] . '</td>
            </tr>
            <tr>
              <td><b>Mortgage to be redeemed:</b></td>
              <td>' . $_SESSION['quotebuilder']['quoteform']['quote']['salemortgage'] . '</td>
            </tr>
            <tr>
              <td><b>Property being purchased:</b></td>
              <td valign="top">' . $buyingflatnumber . '<br />
                            ' . $buyinghousenumber . '<br />
                            ' . $buyingstreet . '<br />
                            ' . $buyingtown . '<br />
                            ' . $buyingcounty . '<br />
                            ' . $buyingpostcode . '</td>
            </tr>
            <tr>
              <td><b>Property value:</b></td>
              <td>&pound;' . $_SESSION['quotebuilder']['quoteform']['quote']['purchaseprice'] . '</td>
            </tr>
            <tr>
              <td><b>Property tenure:</b></td>
              <td>' . $_SESSION['quotebuilder']['quoteform']['quote']['purchasetenure'] . '</td>
            </tr>
            <tr>
              <td><b>Buying with a mortgage:</b></td>
              <td>' . $_SESSION['quotebuilder']['quoteform']['quote']['purchasemortgage'] . '</td>
            </tr>
            <tr>
              <td><b>Fees:</b></td>
              <td>Fixed as per your quote</td>
            </tr>
            <tr>
              <td><b>No Completion, No Fee:</b></td>
              <td>Yes</td>
            </tr>
            <tr>
              <td><b>SearchPlus Protection included:</b></td>
              <td>Included (free of charge)</td>
            </tr>
            <tr>
              <td><b>Additional notes:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_purchase_anythingelse'] . '</td>
            </tr>
          </tbody>
        </table>
      </div>
      <h2>What happens next?</h2>
      <ol>
      <li>We will contact you shortly to confirm the name and direct contact details of the solicitor handling your case.  Your solicitor will require an deposit of &pound;160 inc VAT.</li>
      <li>Your solicitor will explain the purchase conveyancing process and answer any questions.</li>
      <li>The seller\'s estate agent will send out the Sales Memorandum (details of the selling price, the property and the seller).</li>
      <li>Upon receipt of the sales memorandum, the seller\'s solicitor will issue draft contracts and the conveyancing process will commence.</li>
      <li>You will receive property and ID forms via email that you will need to complete and return to as soon as possible.</li>
      </ol>
      <h2>What can you do to get your sale and purchase off to the fastest possible start?</h2>
      <p>Completing the property forms as soon as possible and returning them promptly to the solicitor is the best way that you can help speed up your sale. The buyer\'s solicitor will not be able to move forward until they receive this information.</p>
      <p>The buyer\'s solicitor will ask many questions about the property, so supplying your solicitor with as much information as you can now will save time later in the transaction. Useful information includes certificates for buildings insurance, FENSA, NHBC, EPC.</p>
      <p>If you are taking out a mortgage on your purchase, you should also confirm funding arrangements with your lender; and pass your solicitor\'s details on to your mortgage lender as soon as we have confirmed these with you.</p>
      <p>Your solicitor will advise you on the searches you will need for your purchase. These will be based on the location of the property, or the requirements of the lender. We would encourage you to get your searches ordered at the earliest possible time to avoid delays in your purchase. Some local councils can take weeks to return these searches.</p>
      <p>SearchPlus Protection is included with this quote at no additional cost. If for any reason your property purchase falls through, SearchPlus Protection means the same searches will be carried out on the alternative property you buy completely FREE of charge.</p>
      <p>Now that the legal work for the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . ' is underway, we want to reassure you that Homeward Legal is still here for you should you need us. Should you ever need to speak to us regarding your solicitor, please call us on 0845 6435 785 (local call rate) or 0330 6600 286 (local call rate from a mobile).</p>
      ';
  }
  if ($_SESSION['quotebuilder']['quoteform']['type'] == 'pco') {
    $output .= '<p>Thank you for instructing a Homeward Legal solicitor. We will shortly contact you with the details of the solicitor who will be acting on your behalf to complete the conveyancing for the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . '.</p>
      <h2>Your Details</h2>
      <p>Please check the following details and contact us on <a href="tel:08000386699">0800 038 6699</a> (local call rate from a land line or mobile) if there are any errors, or this information changes:</p>
      <div class="tablebg">
        <table>
          <tbody>
            <tr>
              <td class="tabletitle"><b>Client name acting for:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '</td>
            </tr>
            
            <tr>
              <td><b>Tel no:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_number1'] . '</td>
            </tr>
            <tr>
              <td><b>2nd Tel No:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_number2'] . '</td>
            </tr>
            <tr>
              <td><b>Email:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</td>
            </tr>
            <tr>
              <td><b>Correspondence address:</b></td>
              <td valign="top">' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'] . '</td>
            </tr>
            <tr>
              <td><b>Property being purchased:</b></td>
              <td valign="top">' . $buyingflatnumber . '<br />
                            ' . $buyinghousenumber . '<br />
                            ' . $buyingstreet . '<br />
                            ' . $buyingtown . '<br />
                            ' . $buyingcounty . '<br />
                            ' . $buyingpostcode . '</td>
            </tr>
            <tr>
              <td><b>Property value:</b></td>
              <td>&pound;' . $_SESSION['quotebuilder']['quoteform']['quote']['purchaseprice'] . '</td>
            </tr>
            <tr>
              <td><b>Property tenure:</b></td>
              <td>' . $_SESSION['quotebuilder']['quoteform']['quote']['purchasetenure'] . '</td>
            </tr>
            <tr>
              <td><b>Buying with a mortgage:</b></td>
              <td>' . $_SESSION['quotebuilder']['quoteform']['quote']['purchasemortgage'] . '</td>
            </tr>
            <tr>
              <td><b>Fees:</b></td>
              <td>Fixed as per your quote</td>
            </tr>
            <tr>
              <td><b>No Completion, No Fee:</b></td>
              <td>Yes</td>
            </tr>
            <tr>
              <td><b>SearchPlus Protection included:</b></td>
              <td>Included (free of charge)</td>
            </tr>
            <tr>
              <td><b>Additional notes for your purchase:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_purchase_anythingelse'] . '</td>
            </tr>
          </tbody>
        </table>
      </div>
      <h2>What happens next?</h2>
      <ol>
      <li>We will contact you shortly to confirm the name and direct contact details of the solicitor handling your case.  Your solicitor will require deposits of &pound;160 inc VAT for both your sale and purchase.</li>
      <li>Your solicitor will explain the sale and purchase conveyancing process and answer any questions.</li>
      <li>The estate agents will send sales memoranda of the property you are selling and buying to all respective solicitors.</li>
      <li>Upon receipt of the sales memoranda, your solicitor will write to your buyer\'s solicitor to confirm instruction and issue draft contracts on your sale and await draft contracts from the seller\'s solicitor on your purchase.</li>
      <li>You will receive property forms via email that you will need to complete and return as soon as possible.</li>
      </ol>
      <h2>What can you do to get the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . ' off to the fastest possible start?</h2>
      <p>Completing the property and ID forms as soon as possible, and returning them promptly to your solicitor, is the best way that you can help speed up your purchase. You should also confirm funding arrangements with your lender; and pass your solicitor\'s details on to your mortgage lender as soon as we have confirmed these with you.</p>
      <p>Your solicitor will advise you on the searches you will need for your purchase. These will be based on the location of the property, or the requirements of the lender. We would encourage you to get your searches ordered at the earliest possible time to avoid delays in your purchase. Some local councils can take weeks to return these searches.</p>
      <p>SearchPlus Protection is included with this quote at no additional cost. If for any reason your property purchase falls through, SearchPlus Protection means the same searches will be carried out on the alternative property you buy completely FREE of charge.</p>
      <p>Now that the legal work for the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . ' is underway, we want to reassure you that Homeward Legal is still here for you should you need us. Should you ever need to speak to us regarding your solicitor, please call us on 0845 6435 785 (local call rate) or 0330 6600 286 (local call rate from a mobile).</p>
      ';
  }
  if ($_SESSION['quotebuilder']['quoteform']['type'] == 'sco') {
    $output .= '<p>Thank you for instructing a Homeward Legal solicitor. We will shortly contact you with the details of the solicitor who will be acting on your behalf to complete the conveyancing for the sale of ' . $sellinghousenumber . ' ' . $sellingstreet . '.</p>
      <h2>Your Details</h2>
      <p>Please check the following details and contact us on 
  <a href="tel:08000386699">0800 038 6699</a> (local call rate from a land line or mobile) if there are any errors, or this information changes:</p>
      <div class="tablebg">
        <table>
          <tbody>
            <tr>
              <td class="tabletitle"><b>Client name acting for:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '</td>
            </tr>
            
            <tr>
              <td><b>Tel no:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_number1'] . '</td>
            </tr>
            <tr>
              <td><b>2nd Tel No:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_number2'] . '</td>
            </tr>
            <tr>
              <td><b>Email:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</td>
            </tr>
            <tr>
              <td><b>Correspondence address:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'] . '</td>
            </tr>
            <tr>
              <td><b>Property being sold:</b></td>
              <td>' . $sellingflatnumber . '<br />
                            ' . $sellinghousenumber . '<br />
                            ' . $sellingstreet . '<br />
                            ' . $sellingtown . '<br />
                            ' . $sellingcounty . '<br />
                            ' . $sellingpostcode . '</td>
            </tr>
            <tr>
              <td><b>Property value:</b></td>
              <td>&pound;' . $_SESSION['quotebuilder']['quoteform']['quote']['saleprice'] . '</td>
            </tr>
            <tr>
              <td><b>Property tenure:</b></td>
              <td>' . $_SESSION['quotebuilder']['quoteform']['quote']['saletenure'] . '</td>
            </tr>
            <tr>
              <td><b>Mortgage to be redeemed:</b></td>
              <td>' . $_SESSION['quotebuilder']['quoteform']['quote']['salemortgage'] . '</td>
            </tr>
            <tr>
              <td><b>Fees:</b></td>
              <td>Fixed as per your quote</td>
            </tr>
            <tr>
              <td><b>No Completion, No Fee:</b></td>
              <td>Yes</td>
            </tr>
            <tr>
              <td><b>Additional notes for your sale:</b></td>
              <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_sale_anythingelse'] . '</td>
            </tr>
          </tbody>
        </table>
      </div>
      <h2>What happens next?</h2>
      <ol>
      <li>We will contact you shortly to confirm the name and direct contact details of the solicitor handling your case.  Your solicitor will require an deposit of &pound;160 inc VAT.</li>
      <li>Your solicitor will explain the sale conveyancing process and answer any questions.</li>
      <li>The estate agent will send the Sales Memorandum (details of the selling price, the property and your buyer) to your solicitor.</li>
      <li>Upon receipt of the sales memorandum, your solicitor will write to the other side\'s solicitor to confirm instruction and issue draft contracts.</li>
      <li>You will receive property forms via email that you will need to complete and return as soon as possible.</li>
      </ol>
      <h2>What can you do to get the sale of ' . $sellinghousenumber . ' ' . $sellingstreet . ' off to the fastest possible start?</h2>
      <p>Completing the property forms as soon as possible and returning them promptly to the solicitor is the best way that you can help speed up your sale. The buyer\'s solicitor will not be able to move forward until they receive this information.</p>
      <p>The buyer\'s solicitor will ask many questions about the property, so supplying your solicitor with as much information as you can now will save time later in the transaction. Useful information includes certificates for buildings insurance, FENSA, NHBC, EPC.</p>
      <p>Now that the legal work for the sale of ' . $sellinghousenumber . ' ' . $sellingstreet . ' is underway, we want to reassure you that Homeward Legal is still here for you should you need us. Should you ever need to speak to us regarding your solicitor, please call us on <b>0845 6435 785.</b></p>
      ';
  }

  $completiondate = $_SESSION['quotebuilder']['instructform_1']['instruct_completiondate'];
  $callbackdate = $_SESSION['quotebuilder']['instructform_1']['instruct_callbackdate'];
  if (isset($_SESSION['quotebuilder']['instructform_1']['instruct_sale_anythingelse'])) {
    $saleanyelse = $_SESSION['quotebuilder']['instructform_1']['instruct_sale_anythingelse'];
  }
  else {
    $saleanyelse = '';
  }

  if (isset($_SESSION['quotebuilder']['instructform_1']['instruct_purchase_anythingelse'])) {
    $puranyelse = $_SESSION['quotebuilder']['instructform_1']['instruct_purchase_anythingelse'];
  }
  else {
    $puranyelse = '';
  }

  // Table name no longer needs {}.
  $nid = db_insert('quip_conv_instructs')
    ->fields(array('instruct_name1' => $_SESSION['quotebuilder']['instructform_1']['instruct_name1'], 'instruct_surname1' => $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'], 'instruct_number1' => $_SESSION['quotebuilder']['instructform_1']['instruct_number1'], 'instruct_number2' => $_SESSION['quotebuilder']['instructform_1']['instruct_number2'], 'instruct_email1' => $_SESSION['quotebuilder']['instructform_1']['instruct_email1'], 'instruct_address1_0' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'], 'instruct_address1_1' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'], 'instruct_address1_2' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'], 'instruct_address1_3' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'], 'instruct_address1_4' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'], 'instruct_postcode1' => $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'], 'instruct_sp_address1_0' => $sellingflatnumber, 'instruct_sp_address1_1' => $sellinghousenumber, 'instruct_sp_address1_2' => $sellingstreet, 'instruct_sp_address1_3' => $sellingtown, 'instruct_sp_address1_4' => $sellingcounty, 'instruct_sp_postcode1' => $sellingpostcode, 'instruct_sale_anythingelse' => $saleanyelse, 'instruct_pp_address1_0' => $buyingflatnumber, 'instruct_pp_address1_1' => $buyinghousenumber, 'instruct_pp_address1_2' => $buyingstreet, 'instruct_pp_address1_3' => $buyingtown, 'instruct_pp_address1_4' => $buyingcounty, 'instruct_pp_postcode1' => $buyingpostcode, 'instruct_purchase_anythingelse' => $puranyelse, 'instruct_confirm' => 1, 'qid' => $_SESSION['quotebuilder']['quoteform']['quote']['qid'], 'date' => REQUEST_TIME, 'completiondate' => $completiondate, 'callbackdate' => $callbackdate))->execute();

  // Con Instruct Leadtracker.
  $qid = $_SESSION['quotebuilder']['quoteform']['quote']['qid'];
  $prefix = $_SESSION['quotebuilder']['quoteform']['type'];
  $date = time();
  // preg_replace( "/[^a-z0-9 ]/i", "", $string );.
  $instruct_name1 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_name1']);
  $instruct_surname1 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_surname1']);
  $instruct_number1 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_number1']);
  $instruct_number2 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_number2']);
  $instruct_email1 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_email1']);
  $instruct_address1_0 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0']);
  $instruct_address1_1 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1']);
  $instruct_address1_2 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2']);
  $instruct_address1_3 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3']);
  $instruct_address1_4 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4']);
  $instruct_postcode1 = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1']);

  $instruct_pp_address1_0 = preg_replace("/[^A-Za-z0-9 ]/i", "", $buyingflatnumber);
  $instruct_pp_address1_1 = preg_replace("/[^A-Za-z0-9 ]/i", "", $buyinghousenumber);
  $instruct_pp_address1_2 = preg_replace("/[^A-Za-z0-9 ]/i", "", $buyingstreet);
  $instruct_pp_address1_3 = preg_replace("/[^A-Za-z0-9 ]/i", "", $buyingtown);
  $instruct_pp_address1_4 = preg_replace("/[^A-Za-z0-9 ]/i", "", $buyingcounty);
  $instruct_pp_postcode1 = preg_replace("/[^A-Za-z0-9 ]/i", "", $buyingpostcode);
  $instruct_anythingelse = check_plain($saleanyelse) . ' ' . check_plain($puranyelse);
  $instruct_anythingelse = preg_replace("/[^A-Za-z0-9 ]/i", "", $instruct_anythingelse);
  $instruct_completiondate = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_completiondate']);
  $instruct_callbackdate = preg_replace("/[^A-Za-z0-9 ]/i", "", $_SESSION['quotebuilder']['instructform_1']['instruct_callbackdate']);


  // NEW LT SUBMIT.
  $con = mysql_connect("81.201.129.92", "fitzalan_user", "7.nPy]Z#]T2w");
  mysql_select_db('fitzalan_db', $con);
  $result = mysql_query("INSERT INTO raw_fm_instruct (qid,type,date,title,firstname,lastname,phone,phone2,email,cor_address1_0,cor_address1_1,cor_address1_2,cor_address1_3,cor_address1_4,cor_postcode1,sp_address1_0,sp_address1_1,sp_address1_2,sp_address1_3,sp_address1_4,sp_postcode1,pp_address1_0,pp_address1_1,pp_address1_2,pp_address1_3,pp_address1_4,pp_postcode1,agency,agent_person,agent_phone,agent_email,notes,prefered_completion_date,callback_date,cllsite,cllservice)
                  VALUES (
              '$qid',
                    '$prefix',
                    $date,
        '',	
        '$instruct_name1',
        '$instruct_surname1',
        '$instruct_number1',
        '$instruct_number2',
        '$instruct_email1',
        '$instruct_address1_0',
        '$instruct_address1_1',
        '$instruct_address1_2',
        '$instruct_address1_3',
        '$instruct_address1_4',
        '$instruct_postcode1',
        '$sellingflatnumber',
        '$sellinghousenumber',
        '$sellingstreet',
        '$sellingtown',
        '$sellingcounty',
        '$sellingpostcode',
        '$instruct_pp_address1_0',
        '$instruct_pp_address1_1',
        '$instruct_pp_address1_2',
        '$instruct_pp_address1_3',
        '$instruct_pp_address1_4',
        '$instruct_pp_postcode1',
        '',
        '',
        '',
        '',
        '$instruct_anythingelse',
        '$completiondate',
        '$callbackdate',
        'FM',
        'CON'
                  )");

  if (!$result) {
    dsm(mysql_error($con));
  }

  $mainoutput = '<div class="instruct-page">';
  $output .= '</div>';
  $mainoutput .= $output;
  $mainoutput .= '</div>';

  // Email
  // .
  if ($_SERVER['HTTP_HOST'] == 'www.homewardlegal.co.uk') {
    $onlive = TRUE;
  }
  else {
    $onlive = FALSE;
  }

  if ($onlive) {
    $from = 'clientservices@homewardlegal.co.uk';

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $from . "\r\n";
    if ($_SESSION['quotebuilder']['quoteform']['type'] == 'spc') {
      $message = '
        <html>
        <head>
        <title>Contact through Website</title>
        </head>
        <body>
        <p><span style="font-family: helvetica; font-size:10pt;">Dear ' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . ',</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">Thank you for instructing a Homeward Legal solicitor. We will shortly contact you with the details of the solicitor who will be acting on your behalf to complete the conveyancing for the sale of ' . $sellinghousenumber . ' ' . $sellingstreet . ' and the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . '.</span></p>
        <h2><span style="font-family: helvetica; font-size:14pt;">Your Details</span></h2>
        <p><span style="font-family: helvetica; font-size:10pt;">Please check the following details and contact us on 
  <a href="tel:08000386699">0800 038 6699</a> (local call rate from a land line or mobile) if there are any errors, or this information changes:</span></p>
        <table border="0">
          <tbody>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Client name acting for:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '</span></td>
            </tr>
            
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Tel no:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_number1'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>2nd Tel No:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_number2'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Email:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</span></td>
            </tr>
            <tr>
              <td valign="top"><span style="font-family: helvetica; font-size:10pt;"><strong>Correspondence address:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'] . '</span></td>
            </tr>
            <tr>
              <td valign="top"><span style="font-family: helvetica; font-size:10pt;"><strong>Property being sold:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $sellingflatnumber . '<br />
                            ' . $sellinghousenumber . '<br />
                            ' . $sellingstreet . '<br />
                            ' . $sellingtown . '<br />
                            ' . $sellingcounty . '<br />
                            ' . $sellingpostcode . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Property value:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">&pound;' . $_SESSION['quotebuilder']['quoteform']['quote']['saleprice'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Property tenure:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['quoteform']['quote']['saletenure'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Mortgage to be redeemed:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['quoteform']['quote']['salemortgage'] . '</span></td>
            </tr>
            <tr>
              <td valign="top"><span style="font-family: helvetica; font-size:10pt;"><strong>Property being purchased:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $buyingflatnumber . '<br />
                            ' . $buyinghousenumber . '<br />
                            ' . $buyingstreet . '<br />
                            ' . $buyingtown . '<br />
                            ' . $buyingcounty . '<br />
                            ' . $buyingpostcode . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Property value:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">&pound;' . $_SESSION['quotebuilder']['quoteform']['quote']['purchaseprice'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Property tenure:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['quoteform']['quote']['purchasetenure'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Buying with a mortgage:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['quoteform']['quote']['purchasemortgage'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Fees:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">Fixed as per your quote</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>No Completion, No Fee:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">Yes</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>SearchPlus Protection included:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">Included (free of charge)</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Additional notes:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_purchase_anythingelse'] . '</span></td>
            </tr>
          </tbody>
        </table>
        <h2><span style="font-family: helvetica; font-size:14pt;">What happens next?</span></h2>
        <ol>
        <li><span style="font-family: helvetica; font-size:10pt;">We will contact you shortly to confirm the name and direct contact details of the solicitor handling your case.  Your solicitor will require an deposit of &pound;160 inc VAT.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">Your solicitor will explain the purchase conveyancing process and answer any questions.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">The seller\'s estate agent will send out the Sales Memorandum (details of the selling price, the property and the seller).</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">Upon receipt of the sales memorandum, the seller\'s solicitor will issue draft contracts and the conveyancing process will commence.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">You will receive property and ID forms via email that you will need to complete and return to as soon as possible.</span></li>
        </ol>
        <h2><span style="font-family: helvetica; font-size:14pt;">What can you do to get your sale and purchase off to the fastest possible start?</span></h2>
        <p><span style="font-family: helvetica; font-size:10pt;">Completing the property forms as soon as possible and returning them promptly to the solicitor is the best way that you can help speed up your sale. The buyer\'s solicitor will not be able to move forward until they receive this information.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">The buyer\'s solicitor will ask many questions about the property, so supplying your solicitor with as much information as you can now will save time later in the transaction. Useful information includes certificates for buildings insurance, FENSA, NHBC, EPC.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">If you are taking out a mortgage on your purchase, you should also confirm funding arrangements with your lender; and pass your solicitor\'s details on to your mortgage lender as soon as we have confirmed these with you.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">Your solicitor will advise you on the searches you will need for your purchase. These will be based on the location of the property, or the requirements of the lender. We would encourage you to get your searches ordered at the earliest possible time to avoid delays in your purchase. Some local councils can take weeks to return these searches.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">SearchPlus Protection is included with this quote at no additional cost. If for any reason your property purchase falls through, SearchPlus Protection means the same searches will be carried out on the alternative property you buy completely FREE of charge.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">Now that the legal work for the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . ' is underway, we want to reassure you that Homeward Legal is still here for you should you need us. Should you ever need to speak to us regarding your solicitor, please call us on 0845 6435 785 (local call rate) or 0330 6600 286 (local call rate from a mobile).</span></p>
        </body>
        </html>
        ';
    }
    elseif ($_SESSION['quotebuilder']['quoteform']['type'] == 'pco') {
      $message = '
        <html>
        <head>
        <title>Contact through Website</title>
        </head>
        <body>
        <p><span style="font-family: helvetica; font-size:10pt;">Dear ' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . ',</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">Thank you for instructing a Homeward Legal solicitor. We will shortly contact you with the details of the solicitor who will be acting on your behalf to complete the conveyancing for the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . '.</span></p>
        <h2><span style="font-family: helvetica; font-size:14pt;">Your Details</span></h2>
        <p><span style="font-family: helvetica; font-size:10pt;">Please check the following details and contact us on 
      <a href="tel:08000386699">0800 038 6699</a> (local call rate from a land line or mobile) if there are any errors, or this information changes:</span></p>
        <table border="0">
          <tbody>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Client name acting for:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '</span></td>
            </tr>
            
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Tel no:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_number1'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>2nd Tel No:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_number2'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Email:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</span></td>
            </tr>
            <tr>
              <td valign="top"><span style="font-family: helvetica; font-size:10pt;"><strong>Correspondence address:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'] . '</span></td>
            </tr>
            <tr>
              <td valign="top"><span style="font-family: helvetica; font-size:10pt;"><strong>Property being purchased:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $buyingflatnumber . '<br />
                            ' . $buyinghousenumber . '<br />
                            ' . $buyingstreet . '<br />
                            ' . $buyingtown . '<br />
                            ' . $buyingcounty . '<br />
                            ' . $buyingpostcode . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Property value:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">&pound;' . $_SESSION['quotebuilder']['quoteform']['quote']['purchaseprice'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Property tenure:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['quoteform']['quote']['purchasetenure'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Buying with a mortgage:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['quoteform']['quote']['purchasemortgage'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Fees:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">Fixed as per your quote</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>No Completion, No Fee:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">Yes</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>SearchPlus Protection included:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">Included (free of charge)</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Additional notes for your purchase:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_purchase_anythingelse'] . '</span></td>
            </tr>
          </tbody>
        </table>
        <h2><span style="font-family: helvetica; font-size:14pt;">What happens next?</span></h2>
        <ol>
        <li><span style="font-family: helvetica; font-size:10pt;">We will contact you shortly to confirm the name and direct contact details of the solicitor handling your case.  Your solicitor will require deposits of &pound;160 inc VAT for both your sale and purchase</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">Your solicitor will explain the sale and purchase conveyancing process and answer any questions.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">The estate agents will send sales memoranda of the property you are selling and buying to all respective solicitors.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">Upon receipt of the sales memoranda, your solicitor will write to your buyer\'s solicitor to confirm instruction and issue draft contracts on your sale and await draft contracts from the seller\'s solicitor on your purchase.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">You will receive property forms via email that you will need to complete and return as soon as possible.</span></li>
        </ol>
        <h2><span style="font-family: helvetica; font-size:14pt;">What can you do to get the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . ' off to the fastest possible start?</span></h2>
        <p><span style="font-family: helvetica; font-size:10pt;">Completing the property and ID forms as soon as possible, and returning them promptly to your solicitor, is the best way that you can help speed up your purchase. You should also confirm funding arrangements with your lender; and pass your solicitor\'s details on to your mortgage lender as soon as we have confirmed these with you.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">Your solicitor will advise you on the searches you will need for your purchase. These will be based on the location of the property, or the requirements of the lender. We would encourage you to get your searches ordered at the earliest possible time to avoid delays in your purchase. Some local councils can take weeks to return these searches.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">SearchPlus Protection is included with this quote at no additional cost. If for any reason your property purchase falls through, SearchPlus Protection means the same searches will be carried out on the alternative property you buy completely FREE of charge.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">Now that the legal work for the purchase of ' . $buyinghousenumber . ' ' . $buyingstreet . ' is underway, we want to reassure you that Homeward Legal is still here for you should you need us. Should you ever need to speak to us regarding your solicitor, please call us on 0845 6435 785 (local call rate) or 0330 6600 286 (local call rate from a mobile).</span></p>
        </body>
        </html>
        ';
    }
    elseif ($_SESSION['quotebuilder']['quoteform']['type'] == 'sco') {
      $message = '
        <html>
        <head>
        <title>Contact through Website</title>
        </head>
        <body>
        <p><span style="font-family: helvetica; font-size:10pt;">Dear ' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . ',</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">Thank you for instructing a Homeward Legal solicitor. We will shortly contact you with the details of the solicitor who will be acting on your behalf to complete the conveyancing for the sale of ' . $sellinghousenumber . ' ' . $sellingstreet . '.</span></p>
        <h2><span style="font-family: helvetica; font-size:14pt;">Your Details</span></h2>
        <p><span style="font-family: helvetica; font-size:10pt;">Please check the following details and contact us on 
  <a href="tel:08000386699">0800 038 6699</a> (local call rate from a land line or mobile) if there are any errors, or this information changes:</span></p>
        <table border="0">
          <tbody>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Client name acting for:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '</span></td>
            </tr>
            
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Tel no:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_number1'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>2nd Tel No:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_number2'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Email:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</span></td>
            </tr>
            <tr>
              <td valign="top"><span style="font-family: helvetica; font-size:10pt;"><strong>Correspondence address:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'] . '<br />
                            ' . $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'] . '</span></td>
            </tr>
            <tr>
              <td valign="top"><span style="font-family: helvetica; font-size:10pt;"><strong>Property being sold:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $sellingflatnumber . '<br />
                            ' . $sellinghousenumber . '<br />
                            ' . $sellingstreet . '<br />
                            ' . $sellingtown . '<br />
                            ' . $sellingcounty . '<br />
                            ' . $sellingpostcode . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Property value:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">&pound;' . $_SESSION['quotebuilder']['quoteform']['quote']['saleprice'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Property tenure:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['quoteform']['quote']['saletenure'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Mortgage to be redeemed:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['quoteform']['quote']['salemortgage'] . '</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Fees:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">Fixed as per your quote</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>No Completion, No Fee:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">Yes</span></td>
            </tr>
            <tr>
              <td><span style="font-family: helvetica; font-size:10pt;"><strong>Additional notes for your sale:</strong></span></td>
              <td><span style="font-family: helvetica; font-size:10pt;">' . $_SESSION['quotebuilder']['instructform_1']['instruct_sale_anythingelse'] . '</span></td>
            </tr>
          </tbody>
        </table>
        <h2><span style="font-family: helvetica; font-size:14pt;">What happens next?</span></h2>
        <ol>
        <li><span style="font-family: helvetica; font-size:10pt;">We will contact you shortly to confirm the name and direct contact details of the solicitor handling your case.  Your solicitor will require an deposit of &pound;160 inc VAT.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">Your solicitor will explain the sale conveyancing process and answer any questions.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">The estate agent will send the Sales Memorandum (details of the selling price, the property and your buyer) to your solicitor.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">Upon receipt of the sales memorandum, your solicitor will write to the other side\'s solicitor to confirm instruction and issue draft contracts.</span></li>
        <li><span style="font-family: helvetica; font-size:10pt;">You will receive property forms via email that you will need to complete and return as soon as possible.</span></li>
        </ol>
        <h2><span style="font-family: helvetica; font-size:14pt;">What can you do to get the sale of ' . $sellinghousenumber . ' ' . $sellingstreet . ' off to the fastest possible start?</span></h2>
        <p><span style="font-family: helvetica; font-size:10pt;">Completing the property forms as soon as possible and returning them promptly to the solicitor is the best way that you can help speed up your sale. The buyer\'s solicitor will not be able to move forward until they receive this information.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">The buyer\'s solicitor will ask many questions about the property, so supplying your solicitor with as much information as you can now will save time later in the transaction. Useful information includes certificates for buildings insurance, FENSA, NHBC, EPC.</span></p>
        <p><span style="font-family: helvetica; font-size:10pt;">Now that the legal work for the sale of ' . $sellinghousenumber . ' ' . $sellingstreet . ' is underway, we want to reassure you that Homeward Legal is still here for you should you need us. Should you ever need to speak to us regarding your solicitor, please call us on <b>0845 6435 785.</b></span></p>
        </body>
        </html>
        ';
    }
    else {
      $message = '
        <html> <head> <title>Contact through Website</title>
          <style>
          body {
              padding:0px;
              margin:0px;
              font-family:Arial, Helvetica, sans-serif;
              color:#555;
              font-size:8pt;
          }
          #wrapper {
            width:600px;
          }
          #yourquote {
              width:600px;
          }
          #mainquote {
              padding:0px 10px;
          }
          h1 {
              font-size:1.4em;
              color:#003366;
          }
          h2 {
              font-size:1.2em;
              color:#003366;
          }
          h3 {
              font-size:1.1em;
              color:#003366;
          }
          h4 {
              padding:0;
              margin:0;
          }
          h5 {
              padding:0;
              margin:0;
              font-weight:normal;
              font-size:1.3em;
          }
          table#wrapper {
              margin:0px;
              padding:0px;
              border-collapse: collapse;
              width:600px;
          }
          
          #wrapper td {
              margin:0px;
              padding: 0px;
          }
          
          table {
              margin:0px;
              padding:0px;
              border-collapse: collapse;
              width:580px;
              font-size:1em;
          }
          
          table p {
              padding:0px;
              margin:0px;
          }
          table#yourresults th {
              background-color:#cccccc;
              color:#ffffff;
              text-align:center;
          }
          table#othercosts th {
              background-color:#eeeeee;
              text-align:left;
              border-bottom:1px solid #999;
          }
          table#othercosts td {
            border-bottom:1px solid #eeeeee;
            vertical-align:top;
          }
          #callus {
              font-size:0.9em;
          }
          #wrapper table td {
              margin:0px;
              padding: 5px;
          }
          #yourresults th.itemtotal {
              background-color:#5588cc;
          }
          .itemtotal1, .itemtotal2, .itemtotal3 {
              background-color:#6699cc;
              color:#ffffff;
          }
          .itemdescription1, .itemfee1, .itemvat1 {
              
          }
          .itemdescription2, .itemfee2, .itemvat2, .itemdescription3, .itemfee3, .itemvat3 {
              background-color:#eeeeee;
          }
          .itemfee1, .itemvat1, .itemtotal1, .itemfee2, .itemvat2, .itemtotal2, .itemfee3, .itemvat3, .itemtotal3 {
              text-align:center;
          }
          #declaration {
              font-weight:bold;
          }
          #contactdetails {border:1px solid #f00;padding:10px;width:600px;}
          #footer td {
              background-color:#003366;
              color:#ffffff;
              text-align:center;
              padding:3px 0;
              margin:10px 0 0 0;
          }
          #quoteextrainfo {
              padding:10px;
              font-size:0.8em;
          }
          table#button {
            width:300px;
          }
          
          table#button td a {
            color:#ffffff;
          }
          table#button td {
              background-color:#d90000;
              color:#ffffff;
              padding:10px;
              font-size:1.2em;
              text-align:center;
              font-weight:bold;
          }
          </style>
          </head>
          <body>
              <table id="wrapper"><tr><td>
              <div id="yourquote">
                  <div id="mainquote">
                      ' . $thanks . '
                      <table id="yourresults">
                          ' . $table . '
                      </table>
                  </div>
              </div>
              </td></tr></table>
          </body>
          </html>
        
        ';
    }

    $salesinfo = '<table>
        <tr>
        <td>Callback date/time:</td>
        <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_callbackdate'] . '</td>
        </tr>
        <tr>
        <td>Prefered completion date:</td>
        <td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_completiondate'] . '</td>
        </tr>
        </table>';

    $messagetosales = $salesinfo . $message;

    $subject = 'Homeward Legal Instruction ' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '/' . $strVendorTxCode;

    require $_SERVER['DOCUMENT_ROOT'] . "/phpmailer/class.phpmailer.php";

    $mail = new PHPMailer();
    // Set mailer to use SMTP.
    $mail->IsSMTP();
    $mail->IsHTML(TRUE);
    // Specify main and backup server.
    $mail->Host = "secure.emailsrvr.com";
    // Turn on SMTP authentication.
    $mail->SMTPAuth = TRUE;
    // SMTP username.
    $mail->Username = "clientservices@homewardlegal.co.uk";
    // SMTP password.
    $mail->Password = "H0m3l3g4l2013";
    $mail->From = $from;
    $mail->FromName = "Homeward Legal";
    $mail->AddAddress('backup@fitzalanpartners.co.uk');
    // $mail->AddCC($cc_email);
    $mail->Subject = $subject;
    $mail->Body = $messagetosales;
    $mail->Send();
    $subject = 'Homeward Legal Instruction';

    $mail = new PHPMailer();
    // Set mailer to use SMTP.
    $mail->IsSMTP();
    $mail->IsHTML(TRUE);
    // Specify main and backup server.
    $mail->Host = "secure.emailsrvr.com";
    // Turn on SMTP authentication.
    $mail->SMTPAuth = TRUE;
    // SMTP username.
    $mail->Username = "clientservices@homewardlegal.co.uk";
    // SMTP password.
    $mail->Password = "H0m3l3g4l2013";
    $mail->From = $from;
    $mail->FromName = "Homeward Legal";
    $mail->AddAddress($_SESSION['quotebuilder']['instructform_1']['instruct_email1']);
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->Send();
  }

  $mainoutput .= '<div id="quotebuilder-steptwo-other-services" class="columns large-4 medium-4 small-12">
          <div id="quotebuilder-whychoose" class="light-blue">
        <h2>Your quote includes:</h2>
        <ul>
          <li>Local expertise</li>
          <li>All legal fees</li>
          <li class="popup-button">Completion to your timetable</li>
          <li>No completion, no fee</li>
          <li>Fixed fee</li>
        </ul>
          </div>
          <div id="quotebuilder-needspeed" class="green-bordered">
      <div id="needspeed-inner">
        <p class="block-header">Need to complete<br />
        <span>faster?</span></p>
        <span id="speed-popup-button" class="black-button">What you need to do</span>
      </div>
          </div>
          <div id="quotebuilder-callback" class="quotebuilder-other-service">
      <div id="quotebuilder-callback-inner">
        <p class="block-header">Prefer to Talk?<br />
        <span><a href="tel:08000386699">0800 038 6699</a></span></p>
        <a href="/contact" class="black-button hide-for-small hide-for-medium">Request a Callback</a>
        <a class="black-button hide-for-large" data-reveal-id="myModal" href="#">Request a Callback <span>»</span></a>
      </div>
          </div><div class="hide-for-small hide-for-medium">
          <div id="quotebuilder-askasolicitor" class="quotebuilder-other-service">
      <p class="block-header">Ask a Solicitor</p>
      ' . drupal_render(drupal_get_form('quotebuilder_askasolicitor_form')) . '
          </div></div>
        </div>';

  return $mainoutput;
}

/**
 *
 */
function quotebuilder_instruct_remtoe() {
  $output = '<div class="instruct-page">';
  $output .= drupal_render(drupal_get_form('quotebuilder_instruct_remtoe_form'));
  $output .= '</div>';
  $output .= '<div id="quotebuilder-steptwo-other-services" class="columns">
		      <div id="quotebuilder-whychoose" class="light-blue">
			  <h2>Your quote includes:</h2>
			  <ul>
			    <li>Local expertise</li>
			    <li>All legal fees</li>
			    <li class="popup-button">Completion to your timetable</li>
			    <li>No completion, no fee</li>
			    <li>Fixed fee</li>
			  </ul>
		      </div>
		      <div id="quotebuilder-needspeed" class="green-bordered">
			<div id="needspeed-inner">
			  <p class="block-header">Need to complete<br />
			  <span>faster?</span></p>
			  <span id="speed-popup-button" class="black-button">What you need to do</span>
			</div>
		      </div>
		      <div id="quotebuilder-callback" class="quotebuilder-other-service">
			<div id="quotebuilder-callback-inner">
			  <p class="block-header">Prefer to Talk?<br />
			  <span><a href="tel:08000386699">0800 038 6699</a></span></p>
			  <a href="/contact" class="black-button hide-for-small hide-for-medium">Request a Callback</a>
			  <a class="black-button hide-for-large" data-reveal-id="myModal" href="#">Request a Callback <span>»</span></a>
			</div>
		      </div><div class="hide-for-small hide-for-medium">
		      <div id="quotebuilder-askasolicitor" class="quotebuilder-other-service">
			<p class="block-header">Ask a Solicitor</p>
			' . drupal_render(drupal_get_form('quotebuilder_askasolicitor_form')) . '
		      </div></div>
		    </div>';
  return $output;
}

/**
 *
 */
function quotebuilder_instruct_remtoe_form() {
  $titles = array();
  $titles[1] = 'concerned with';
  $titles[2] = 'Details';
  if ($_SESSION['quotebuilder']['quoteform']['type'] == 'rem') {
    $titles[1] = 'remotgaging';
    $titles[2] = 'Remortgaging';
  }
  elseif ($_SESSION['quotebuilder']['quoteform']['type'] == 'toe') {
    $titles[1] = 'transferring equity on';
    $titles[2] = 'Equity Transfer';
  }

  $form['h_information2'] = array('#markup' => '<h2>About the property you are ' . $titles[1] . '</h2>');

  $form['instruct_pp_address1_0'] = array('#type' => 'textfield', '#title' => t('Flat number'), '#default_value' => NULL, '#size' => 10);

  $form['instruct_pp_address1_1'] = array('#type' => 'textfield', '#title' => t('House name or number'), '#default_value' => NULL, '#size' => 30, '#required' => TRUE);

  $form['instruct_pp_address1_2'] = array('#type' => 'textfield', '#title' => t('Street/Road'), '#default_value' => NULL, '#size' => 30, '#required' => TRUE);

  $form['instruct_pp_address1_3'] = array('#type' => 'textfield', '#title' => t('Town/City'), '#default_value' => NULL, '#size' => 30, '#required' => TRUE);

  $form['instruct_pp_address1_4'] = array('#type' => 'textfield', '#title' => t('County'), '#default_value' => NULL, '#size' => 30, '#required' => TRUE);

  $form['instruct_pp_postcode1'] = array('#type' => 'textfield', '#title' => t('Postcode'), '#default_value' => NULL, '#size' => 30, '#required' => TRUE);

  if ($_SESSION['quotebuilder']['quoteform']['type'] == 'rem') {
    $form['h_information1'] = array(
      '#markup' => '<div id="instructsale-quoteformblock" class="quoteformblock"><h2>' . $titles[2] . '</h2>
											<table>
    											<tr><td><b>Property Value</b></td><td>&pound;' . number_format($_SESSION['quotebuilder']['quoteform']['quote']['renortprice'], 0) . '</td></tr>
    											<tr><td><b>Remortgage Value</b></td><td>&pound;' . number_format($_SESSION['quotebuilder']['quoteform']['quote']['renortborrow'], 0) . '</td></tr>
											</table>
    										</div>',
    );
  }
  elseif ($_SESSION['quotebuilder']['quoteform']['type'] == 'toe') {
    $form['h_information1'] = array(
      '#markup' => '<div id="instructsale-quoteformblock" class="quoteformblock"><h2>' . $titles[2] . '</h2>
											<table>
    											<tr><td><b>Property Value</b></td><td>&pound;' . number_format($_SESSION['quotebuilder']['quoteform']['quote']['toepropvalue'], 0) . '</td></tr>
    											<tr><td><b>Amount of money changing hands</b></td><td>&pound;' . number_format($_SESSION['quotebuilder']['quoteform']['quote']['toechangehands'], 0) . '</td></tr>
    											<tr><td><b>Remortgage legal work include</b></td><td>' . $_SESSION['quotebuilder']['quoteform']['quote']['toelegal'] . '</td></tr>
											</table>
    										</div>',
    );
  }

  $form['h_information4'] = array('#markup' => '<h2>Anything else?</h2>');

  $form['instruct_purchase_anythingelse'] = array('#type' => 'textarea', '#title' => t('Anything else you need us to know?'), '#cols' => 22, '#rows' => 5, '#resizable' => FALSE);

  $tandc = '1';

  $form['instruct_confirm'] = array('#type' => 'checkbox', '#required' => TRUE, '#title' => t('I have read and accept the <span class="emph fakelink" onclick="openfloatwindow(\'' . base_path() . 'node/' . $tandc . '\')">terms and conditions</span> and am happy to proceed with Homeward Legal\'s Solicitor'), '#attributes' => array('class' => array('skinned-form-controls')));

  $form['next'] = array(
    '#type' => 'submit',
    '#value' => 'Instruct',
    '#prefix' => '<div class="quote-arrow-box">',
    '#attributes' => array('class' => array('green-button')),
    '#suffix' => '<div class="quote-arrow-bg"></div>
                  <div class="quote-arrow"></div><div class="quote-arrow-inner-black"></div>
                  <div class="quote-arrow-inner-green"></div><div class="quote-arrow-inner-box"></div></div>
                  <div class="privacy"><p>We guarantee your privacy.<br />Your information will not be shared.</p></div>
                      ',
  );

  return $form;
}

/**
 *
 */
function quotebuilder_instruct_remtoe_form_submit($form, &$form_state) {
  $_SESSION['quotebuilder']['instructform_remtoe'] = $form_state['values'];
  drupal_goto('instruct/instruct_remtoe/complete');
}

/**
 *
 */
function quotebuilder_instruct_remtoe_complete() {
  $strVendorTxCode = $_SESSION['quotebuilder']['quoteform']['quote']['qid'] . '-i';

  $table = '<table id="instructtable">';
  $table .= '<tr><th colspan="2">Personal Details</th></tr>';
  $table .= '<tr><td>First Name</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . '</td></tr>';
  $table .= '<tr><td>Last Name</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '</td></tr>';
  $table .= '<tr><td>Flat Number</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'] . '</td></tr>';
  $table .= '<tr><td>House Number/Name</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'] . '</td></tr>';
  $table .= '<tr><td>Street</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'] . '</td></tr>';
  $table .= '<tr><td>Town</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'] . '</td></tr>';
  $table .= '<tr><td>County</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'] . '</td></tr>';
  $table .= '<tr><td>Postcode</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'] . '</td></tr>';
  $table .= '<tr><td>Contact Number 1</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_number1'] . '</td></tr>';
  $table .= '<tr><td>Contact Number 2</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_number2'] . '</td></tr>';
  $table .= '<tr><td>Email</td><td>' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</td></tr>';

  $type = 'work';
  if (!empty($_SESSION['quotebuilder']['instructform_remtoe']) and $_SESSION['quotebuilder']['quoteform']['type'] == 'rem') {
    $table .= '<tr><th colspan="2">Remortgage Property Details</th></tr>';
    $table .= '<tr><td>Flat Number</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_0'] . '</td></tr>';
    $table .= '<tr><td>House Number/Name</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_1'] . '</td></tr>';
    $table .= '<tr><td>Street</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_2'] . '</td></tr>';
    $table .= '<tr><td>Town/City</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_3'] . '</td></tr>';
    $table .= '<tr><td>County</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_4'] . '</td></tr>';
    $table .= '<tr><td>Postcode</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_postcode1'] . '</td></tr>';
    $table .= '<tr><td>Property Value</td><td>&pound;' . number_format($_SESSION['quotebuilder']['quoteform']['quote']['renortprice'], 0) . '</td></tr>';
    $table .= '<tr><td>Remortgage Value</td><td>&pound;' . number_format($_SESSION['quotebuilder']['quoteform']['quote']['renortborrow'], 0) . '</td></tr>';
    $type = 'remortgaging';
  }

  if (!empty($_SESSION['quotebuilder']['instructform_remtoe']) and $_SESSION['quotebuilder']['quoteform']['type'] == 'toe') {
    $table .= '<tr><th colspan="2">Transfer of Equity Property Details</th></tr>';
    $table .= '<tr><td>Flat Number</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_0'] . '</td></tr>';
    $table .= '<tr><td>House Number/Name</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_1'] . '</td></tr>';
    $table .= '<tr><td>Street</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_2'] . '</td></tr>';
    $table .= '<tr><td>Town/City</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_3'] . '</td></tr>';
    $table .= '<tr><td>County</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_4'] . '</td></tr>';
    $table .= '<tr><td>Postcode</td><td>' . $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_postcode1'] . '</td></tr>';
    $table .= '<tr><td>Property Value</td><td>&pound;' . number_format($_SESSION['quotebuilder']['quoteform']['quote']['toepropvalue'], 0) . '</td></tr>';
    $table .= '<tr><td>Money Changing Hands</td><td>&pound;' . number_format($_SESSION['quotebuilder']['quoteform']['quote']['toechangehands'], 0) . '</td></tr>';
    $table .= '<tr><td>Require Legal Work Done</td><td>' . ucfirst($_SESSION['quotebuilder']['quoteform']['quote']['toelegal']) . '</td></tr>';
    $type = 'transfer of equity';
  }

  $table .= '</table>';

  $thanks = '
			<h2 class="title">A copy of this has been emailed to:  ' . $_SESSION['quotebuilder']['instructform_1']['instruct_email1'] . '</h2>
			<h1 class="title">Thank you for your instruction to handle ' . $type . ' for the following</h1>
			<ul>';

  if (!empty($_SESSION['quotebuilder']['instructform_remtoe'])) {
    $sl = array();
    $line1 = '';
    if ($_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_0']) {
      $line1 .= $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_0'] . ' ';
    }
    if ($_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_1']) {
      $line1 .= $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_1'] . ' ';
    }
    if ($_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_2']) {
      $line1 .= $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_2'];
    }
    $sl[] = $line1;
    if ($_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_3']) {
      $sl[] = $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_3'];
    }
    if ($_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_4']) {
      $sl[] = $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_4'];
    }
    if ($_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_postcode1']) {
      $sl[] = $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_postcode1'];
    }
    $thanks .= '<li>' . implode(', ', $sl) . '</li>';
  }

  $thanks .= '</ul>
			<h1 class="title">What we do next:</h1>
			<p>We will now take the reins in ensuring your move gets off to the fastest possible start - maximising the likelihood of a trouble free move for you.</p>
			<h2 class="title">Your Solicitor</h2>
			<p>Your Solicitor is now setting up your file and obtaining information from the Land Registry and our database of recent transactions in your area.   They will call you shortly to introduce themselves.</p>
			<h1 class="title">Help us help you:</h1>
			<p>You will shortly receive an email containing the legal documents that need to be completed and returned to your Solicitor.    We would urge you to complete these as soon as possible to enable us
			to get your move underway without delay.</p>
			<p>Please call us  on 
	<a href="tel:08000386699">0800 038 6699</a> (local call rate from a land line or mobile) if the email does not arrive within 30 minutes.</p>';
  $thanks .= '<h1 class="title">The following are the details you entered on Instruction. </h1>
		<h2>If there are any errors please let us know immediately.</h2>';

  $output = $thanks . $table;

  $mainoutput = '<div class="instruct-page">';
  $mainoutput .= $output;
  $mainoutput .= '</div>';

  // Table name no longer needs {}.
  $nid = db_insert('quip_remtoe_instructs')
    ->fields(array('instruct_name1' => $_SESSION['quotebuilder']['instructform_1']['instruct_name1'], 'instruct_surname1' => $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'], 'instruct_number1' => $_SESSION['quotebuilder']['instructform_1']['instruct_number1'], 'instruct_number2' => $_SESSION['quotebuilder']['instructform_1']['instruct_number2'], 'instruct_email1' => $_SESSION['quotebuilder']['instructform_1']['instruct_email1'], 'instruct_address1_0' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_0'], 'instruct_address1_1' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_1'], 'instruct_address1_2' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_2'], 'instruct_address1_3' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_3'], 'instruct_address1_4' => $_SESSION['quotebuilder']['instructform_1']['instruct_address1_4'], 'instruct_postcode1' => $_SESSION['quotebuilder']['instructform_1']['instruct_postcode1'], 'instruct_pp_address1_0' => $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_0'], 'instruct_pp_address1_1' => $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_1'], 'instruct_pp_address1_2' => $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_2'], 'instruct_pp_address1_3' => $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_3'], 'instruct_pp_address1_4' => $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_address1_4'], 'instruct_pp_postcode1' => $_SESSION['quotebuilder']['instructform_remtoe']['instruct_pp_postcode1'], 'instruct_purchase_anythingelse' => $_SESSION['quotebuilder']['instructform_remtoe']['instruct_purchase_anythingelse'], 'instruct_confirm' => 1, 'qid' => $_SESSION['quotebuilder']['quoteform']['quote']['qid'], 'date' => REQUEST_TIME, 'completiondate' => $_SESSION['quotebuilder']['instructform_1']['instruct_completiondate'], 'callbackdate' => $_SESSION['quotebuilder']['instructform_1']['instruct_callbackdate']))->execute();

  // Email.
  if ($_SERVER['HTTP_HOST'] == 'www.homewardlegal.co.uk') {
    $onlive = TRUE;
  }
  else {
    $onlive = FALSE;
  }

  if ($onlive) {
    $from = 'clientservices@homewardlegal.co.uk';

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: ' . $from . "\r\n" . 'Reply-To: ' . $from . "\r\n";

    $message = '
		<html> <head> <title>Contact through Website</title>
			<style>
			body {
					padding:0px;
					margin:0px;
					font-family:Arial, Helvetica, sans-serif;
					color:#555;
					font-size:8pt;
			}
			#wrapper {
				width:600px;
			}
			#yourquote {
					width:600px;
			}
			#mainquote {
					padding:0px 10px;
			}
			h1 {
					font-size:1.4em;
					color:#003366;
			}
			h2 {
					font-size:1.2em;
					color:#003366;
			}
			h3 {
					font-size:1.1em;
					color:#003366;
			}
			h4 {
					padding:0;
					margin:0;
			}
			h5 {
					padding:0;
					margin:0;
					font-weight:normal;
					font-size:1.3em;
			}
			table#wrapper {
					margin:0px;
					padding:0px;
					border-collapse: collapse;
					width:600px;
			}
			
			#wrapper td {
					margin:0px;
					padding: 0px;
			}
			
			table {
					margin:0px;
					padding:0px;
					border-collapse: collapse;
					width:580px;
					font-size:1em;
			}
			
			table p {
					padding:0px;
					margin:0px;
			}
			table#yourresults th {
					background-color:#cccccc;
					color:#ffffff;
					text-align:center;
			}
			table#othercosts th {
					background-color:#eeeeee;
					text-align:left;
					border-bottom:1px solid #999;
			}
      table#othercosts td {
        border-bottom:1px solid #eeeeee;
        vertical-align:top;
      }
			#callus {
					font-size:0.9em;
			}
			#wrapper table td {
					margin:0px;
					padding: 5px;
			}
			#yourresults th.itemtotal {
					background-color:#5588cc;
			}
			.itemtotal1, .itemtotal2, .itemtotal3 {
					background-color:#6699cc;
					color:#ffffff;
			}
			.itemdescription1, .itemfee1, .itemvat1 {
					
			}
			.itemdescription2, .itemfee2, .itemvat2, .itemdescription3, .itemfee3, .itemvat3 {
					background-color:#eeeeee;
			}
			.itemfee1, .itemvat1, .itemtotal1, .itemfee2, .itemvat2, .itemtotal2, .itemfee3, .itemvat3, .itemtotal3 {
					text-align:center;
			}
			#declaration {
					font-weight:bold;
			}
			#contactdetails {border:1px solid #f00;padding:10px;width:600px;}
			#footer td {
					background-color:#003366;
					color:#ffffff;
					text-align:center;
					padding:3px 0;
					margin:10px 0 0 0;
			}
			#quoteextrainfo {
					padding:10px;
					font-size:0.8em;
			}
			table#button {
				width:300px;
			}
			
			table#button td a {
				color:#ffffff;
			}
			table#button td {
					background-color:#d90000;
					color:#ffffff;
					padding:10px;
					font-size:1.2em;
					text-align:center;
          font-weight:bold;
			}
			</style>
			</head>
			<body>
			<table>
				<tr align="center" style="background-color: #FFFFFF">
					<td >
						<img src="http://www.homewardlegal.co.uk/sites/default/files/hl-logo.png" alt="Homeward Legal Quote" />
					</td>
				</tr>
			</table>
					<table id="wrapper"><tr><td>
					<div id="yourquote">
							<div id="mainquote">
									' . $thanks . '
									<table id="yourresults">
											' . $table . '
									</table>
							</div>
					</div>
					</td></tr></table>
			</body>
			</html>
		
		';

    $cc_email = "backup@fitzalanpartners.co.uk";
    $subject = 'Homeward Legal Instruction ' . $_SESSION['quotebuilder']['instructform_1']['instruct_name1'] . ' ' . $_SESSION['quotebuilder']['instructform_1']['instruct_surname1'] . '/' . $strVendorTxCode;

    require $_SERVER['DOCUMENT_ROOT'] . "/phpmailer/class.phpmailer.php";

    $mail = new PHPMailer();
    // Set mailer to use SMTP.
    $mail->IsSMTP();
    $mail->IsHTML(TRUE);
    // Specify main and backup server.
    $mail->Host = "secure.emailsrvr.com";
    // Turn on SMTP authentication.
    $mail->SMTPAuth = TRUE;
    // SMTP username.
    $mail->Username = "clientservices@homewardlegal.co.uk";
    // SMTP password.
    $mail->Password = "H0m3l3g4l2013";
    $mail->From = $from;
    $mail->FromName = "Homeward Legal";
    $mail->AddAddress("backup@fitzalanpartners.co.uk");
    // $mail->AddCC($cc_email);
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->Send();

    $subject = 'Homeward Legal Instruction';

    $mail = new PHPMailer();
    // Set mailer to use SMTP.
    $mail->IsSMTP();
    $mail->IsHTML(TRUE);
    // Specify main and backup server.
    $mail->Host = "secure.emailsrvr.com";
    // Turn on SMTP authentication.
    $mail->SMTPAuth = TRUE;
    // SMTP username.
    $mail->Username = "clientservices@homewardlegal.co.uk";
    // SMTP password.
    $mail->Password = "H0m3l3g4l2013";
    $mail->From = $from;
    $mail->FromName = "Homeward Legal";
    $mail->AddAddress($_SESSION['quotebuilder']['instructform_1']['instruct_email1']);
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->Send();
  }

  $mainoutput .= '<div id="quotebuilder-steptwo-other-services" class="columns large-12">
	      <div id="quotebuilder-whychoose" class="light-blue">
		  <h2>Your quote includes:</h2>
		  <ul>
		    <li>Local expertise</li>
		    <li>All legal fees</li>
		    <li class="popup-button">Completion to your timetable</li>
		    <li>No completion, no fee</li>
		    <li>Fixed fee</li>
		  </ul>
	      </div>
	      <div id="quotebuilder-needspeed" class="green-bordered">
		<div id="needspeed-inner">
		  <p class="block-header">Need to complete<br />
		  <span>faster?</span></p>
		  <span id="speed-popup-button" class="black-button">What you need to do</span>
		</div>
	      </div>
	      <div id="quotebuilder-callback" class="quotebuilder-other-service">
		<div id="quotebuilder-callback-inner">
		  <p class="block-header">Prefer to Talk?<br />
		  <span><a href="tel:08000386699">0800 038 6699</a></span></p>
		  <a href="/contact" class="black-button hide-for-small hide-for-medium">Request a Callback</a>
		  <a class="black-button hide-for-large" data-reveal-id="myModal" href="#">Request a Callback <span>»</span></a>
		</div>
	      </div><div class="hide-for-small hide-for-medium">
	      <div id="quotebuilder-askasolicitor" class="quotebuilder-other-service">
		<p class="block-header">Ask a Solicitor</p>
		' . drupal_render(drupal_get_form('quotebuilder_askasolicitor_form')) . '
	      </div></div>
	    </div>';

  return $mainoutput;
}
