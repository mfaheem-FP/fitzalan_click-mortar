<?php

/**
 * @file
 * @file
 * @file
 * .*/

/**
 *
 *
 */
 function quotebuilder_step_one_form($form, &$form_state) {
  $form = array();
  $form['product'] = array(
      '#type' => 'radios',
      '#title' => t('Select your service'),
      '#options' => array(
        'Sale', 'Purchase',
        'Sale & Purchase',
        'Transfer of Equity',
        'Remortgage'
      )
  );
  $form['#action'] = '/quotebuilder';
  $form['submit'] = array('#type' => 'submit', '#value' => 'Get a Quote');
  return $form;
}

/**
 *
 */
function quotebuilder_step_two_form($form, &$form_state) {
  $form = array();

  // Get location.
  if (strpos($_SERVER['HTTP_REFERER'], 'http://www.homewardlegal.co.uk/conveyancing-solicitor/') !== FALSE) {
    $location = ucwords(str_replace('-', ' ', str_replace('http://www.homewardlegal.co.uk/conveyancing-solicitor/', '', $_SERVER['HTTP_REFERER'])));
    $_SESSION['referer']['internalreferer'] = $location;
  }
  else {
    $_SESSION['referer']['internalreferer'] = $_SERVER['HTTP_REFERER'];
  }

  $there = $_SERVER['HTTP_REFERER'];
  $here = 'http://www.fridaysmove.com/';
  $keys = array();
  $ppc = FALSE;
  $site = 'local';

  if (substr($there, 0, strlen($here)) !== $here) {
    $fpla = '';
    $fplx = '';
    if (isset($_GET['fpla'])) {
      $fpla = $_GET['fpla'];
    }
    if (isset($_GET['fplx'])) {
      $fplx = $_GET['fplx'];
    }

    if (isset($_GET['gclid'])) {
      $ppc = TRUE;
    }
    if (isset($_GET['utm_medium']) && $_GET['utm_medium'] == 'cpc') {
      $ppc = TRUE;
    }

    $siteurl = explode('/', substr($there, 7));

    $_SESSION['referer']['sitereferersite'] = $siteurl[0];
    $_SESSION['referer']['siterefererppc'] = $ppc;
    $_SESSION['referer']['sitereferer'] = $there;
    $_SESSION['referer']['fpla'] = $fpla;
    $_SESSION['referer']['fplx'] = $fplx;

    $bits = explode('&', array_pop(explode('?', $there)));
    foreach ($bits as $var) {
      if (substr($var, 0, 2) === 'q=') {
        $keys = explode('+', substr($var, 2));
      }
      if (empty($keys) && substr($var, 0, 2) === 'p=') {
        $keys = explode('+', substr($var, 2));
      }
    }
    $_SESSION['referer']['siterefererkeys'] = $keys;
  }

  $default = NULL;
  if (isset($_REQUEST['product'])) {
    $default = check_plain($_REQUEST['product']);
  }

  switch ($default) {
    case '0':
      $sale = 'show-item';
      $purchase = 'hide-item';
      $toe = 'hide-item';
      $remortgage = 'hide-item';

      break;

    case '1':
      $sale = 'hide-item';
      $purchase = 'show-item';
      $toe = 'hide-item';
      $remortgage = 'hide-item';

      break;

    case '2':
      $sale = 'show-item';
      $purchase = 'show-item';
      $toe = 'hide-item';
      $remortgage = 'hide-item';

      break;

    case '3':
      $sale = 'hide-item';
      $purchase = 'hide-item';
      $toe = 'show-item';
      $remortgage = 'hide-item';

      break;

    case '4':
      $sale = 'hide-item';
      $purchase = 'hide-item';
      $toe = 'hide-item';
      $remortgage = 'show-item';

      break;

    default:
      $sale = 'show-item';
      $purchase = 'hide-item';
      $toe = 'hide-item';
      $remortgage = 'hide-item';

      break;
  }

  $form['completion'] = array('#type' => 'hidden', '#value' => NULL);

  if (isset($_REQUEST['completion'])) {
    $form['completion']['#value'] = check_plain($_REQUEST['completion']);
  }

  $form['location'] = array('#type' => 'hidden', '#value' => 'the UK');

  if (isset($_REQUEST['location'])) {
    $form['location']['#value'] = check_plain($_REQUEST['location']);
  }

  $form['productTitle'] = array('#type' => 'fieldset', '#title' => 'Select solicitors\' service');
  $form['markup1'] = array('#type' => 'markup', '#markup' => '<div class="subsection light-green-bordered">');
  $form['product'] = array('#type' => 'radios', '#required' => TRUE, '#title' => t('Conveyancing for your'), '#options' => array(0 => t('Sale'), 1 => t('Purchase'), 2 => t('Sale & Purchase'), 3 => t('Transfer of Equity'), 4 => t('Remortgage')), '#default_value' => $default, '#attributes' => array('class' => array('skinned-form-controls')));
  $form['markup1end'] = array('#type' => 'markup', '#markup' => '</div>');
  $form['sale'] = array('#type' => 'fieldset', '#title' => 'Your Sale', '#attributes' => array('class' => array($sale)));
  $form['sale']['markupS'] = array('#type' => 'markup', '#markup' => '<div class="subsection light-green-bordered">');
  $form['sale']['sale-price'] = array('#type' => 'textfield', '#title' => t('Sale price'), '#attributes' => array('class' => array('currency property-value-mask')));

  if (isset($_REQUEST['sale-price'])) {
    $form['sale']['sale-price']['#value'] = check_plain($_REQUEST['sale-price']);
  }

  if (isset($_REQUEST['sale-mortgage'])) {
    $saleMortgage = check_plain($_REQUEST['sale-mortgage']);
  }
  else {
    $saleMortgage = 'yes';
  }
  $form['sale']['sale-mortgage'] = array('#type' => 'radios', '#title' => t('Mortgage'), '#options' => array('yes' => 'Yes', 'no' => 'No'), '#default_value' => $saleMortgage, '#attributes' => array('class' => array('skinned-form-controls')));
  $form['sale']['sale-tenure'] = array('#type' => 'select', '#prefix' => '<span class="select-title">Tenure</span>', '#options' => array('freehold' => 'Freehold', 'leasehold' => 'Leasehold', 'sharehold' => 'Share of Freehold'));

  if (isset($_REQUEST['sale-tenure'])) {
    $form['sale']['sale-tenure']['#value'] = check_plain($_REQUEST['sale-tenure']);
  }

  $form['sale']['markupSend'] = array('#type' => 'markup', '#markup' => '</div>');
  $form['purchase'] = array('#type' => 'fieldset', '#title' => 'Your Purchase', '#attributes' => array('class' => array($purchase)));
  $form['purchase']['markupP'] = array('#type' => 'markup', '#markup' => '<div class="subsection light-green-bordered">');
  $form['purchase']['purchase-price'] = array('#type' => 'textfield', '#title' => t('Purchase price'), '#attributes' => array('class' => array('currency property-value-mask')));

  if (isset($_REQUEST['purchase-price'])) {
    $form['purchase']['purchase-price']['#value'] = check_plain($_REQUEST['purchase-price']);
  }

  if (isset($_REQUEST['purchase-mortgage'])) {
    $purchaseMortgage = check_plain($_REQUEST['purchase-mortgage']);
  }
  else {
    $purchaseMortgage = 'yes';
  }
  $form['purchase']['purchase-mortgage'] = array('#type' => 'radios', '#title' => t('Mortgage'), '#options' => array('yes' => 'Yes', 'no' => 'No'), '#default_value' => $purchaseMortgage, '#attributes' => array('class' => array('skinned-form-controls')));

  if (isset($_REQUEST['purchase-helptobuy'])) {
    $purchaseHelptobuy = check_plain($_REQUEST['purchase-helptobuy']);
  }
  else {
    $purchaseHelptobuy = 'no';
  }
  $form['purchase']['purchase-helptobuy'] = array('#type' => 'radios', '#title' => t('Are you buying with "Help to Buy"?'), '#options' => array('yes' => 'Yes', 'no' => 'No'), '#default_value' => $purchaseHelptobuy, '#attributes' => array('class' => array('skinned-form-controls')));

  if (isset($_REQUEST['purchase-tenure'])) {
    $purchaseTenure = check_plain($_REQUEST['purchase-tenure']);
  }
  else {
    $purchaseTenure = 'freehold';
  }

  $form['purchase']['purchase-tenure'] = array('#type' => 'select', '#prefix' => '<span class="select-title">Tenure</span>', '#options' => array('freehold' => 'Freehold', 'leasehold' => 'Leasehold', 'sharehold' => 'Share of Freehold'));
  if (isset($_REQUEST['purchase-tenure'])) {
    $form['sale']['purchase-tenure']['#value'] = check_plain($_REQUEST['purchase-tenure']);
  }

  $form['purchase']['markupPend'] = array('#type' => 'markup', '#markup' => '</div>');

  $form['transfer-of-equity'] = array('#type' => 'fieldset', '#title' => 'Your Transfer of Equity', '#attributes' => array('class' => array($toe)));

  $form['transfer-of-equity']['markupT'] = array('#type' => 'markup', '#markup' => '<div class="subsection light-green-bordered">');

  $form['transfer-of-equity']['toe-propertyValue'] = array('#type' => 'textfield', '#title' => t('Property value'), '#attributes' => array('class' => array('currency property-value-mask')));
  if (isset($_REQUEST['toe-propertyValue'])) {
    $form['transfer-of-equity']['toe-propertyValue']['#value'] = check_plain($_REQUEST['toe-propertyValue']);
  }

  $form['transfer-of-equity']['toe-quantity'] = array('#type' => 'textfield', '#title' => t('How much money has changed hands?'), '#attributes' => array('class' => array('currency property-value-mask')));
  if (isset($_REQUEST['toe-quantity'])) {
    $form['transfer-of-equity']['toe-quantity']['#value'] = check_plain($_REQUEST['toe-quantity']);
  }

  if (isset($_REQUEST['toe-mortgage'])) {
    $toeMortgage = check_plain($_REQUEST['toe-mortgage']);
  }
  else {
    $toeMortgage = 'no';
  }
  $form['transfer-of-equity']['toe-mortgage'] = array('#type' => 'radios', '#title' => t('Is there a mortgage on the property?'), '#default_value' => $toeMortgage, '#options' => array('yes' => t('Yes'), 'no' => t('No')), '#attributes' => array('class' => array('skinned-form-controls')));

  $form['transfer-of-equity']['markupTR'] = array('#type' => 'markup', '#markup' => '<div id="toe-rem">');

  $form['transfer-of-equity']['toe-many-mortgages'] = array(
    '#type' => 'select',
    '#prefix' => '<span class="select-title">Number of mortgages?</span>',
    '#options' => array(1 => t('1'), 2 => t('2'), 3 => t('3')),
  );
  if (isset($_REQUEST['toe-many-mortgages'])) {
    $form['transfer-of-equity']['toe-many-mortgages']['#value'] = check_plain($_REQUEST['toe-many-mortgages']);
  }

  if (isset($_REQUEST['toe-legal'])) {
    $toeLegal = check_plain($_REQUEST['toe-legal']);
  }
  else {
    $toeLegal = 'no';
  }

  $form['transfer-of-equity']['toe-legal'] = array('#type' => 'radios', '#title' => 'Would you like us to handle legal<br />work for the remortgage as well?', '#default_value' => $toeLegal, '#options' => array('yes' => t('Yes'), 'no' => t('No')), '#attributes' => array('class' => array('skinned-form-controls')), '#prefix' => '<div id="quote-legal-wrapper">', '#suffix' => '</div>');
  $form['transfer-of-equity']['markupTRend'] = array('#type' => 'markup', '#markup' => '</div>');
  $form['transfer-of-equity']['markupTend'] = array('#type' => 'markup', '#markup' => '</div>');
  $form['remortgage'] = array('#type' => 'fieldset', '#title' => 'Your Remortgage', '#attributes' => array('class' => array($remortgage)));
  $form['remortgage']['markupR'] = array('#type' => 'markup', '#markup' => '<div class="subsection light-green-bordered">');
  $form['remortgage']['rem-propertyValue'] = array('#type' => 'textfield', '#title' => t('Property value'), '#attributes' => array('class' => array('currency property-value-mask')));
  if (isset($_REQUEST['rem-propertyValue'])) {
    $form['remortgage']['rem-propertyValue']['#value'] = check_plain($_REQUEST['rem-propertyValue']);
  }

  $form['remortgage']['rem-value'] = array('#type' => 'textfield', '#title' => t('Remortgage value?'), '#description' => t('(amount you are borrowing)'), '#attributes' => array('class' => array('currency property-value-mask')));
  if (isset($_REQUEST['rem-value'])) {
    $form['remortgage']['rem-value']['#value'] = check_plain($_REQUEST['rem-value']);
  }

  $form['remortgage']['markupRend'] = array('#type' => 'markup', '#markup' => '</div>');
  $form['details'] = array('#type' => 'fieldset', '#title' => 'Your Details');
  $form['details']['markup2'] = array('#type' => 'markup', '#markup' => '<div class="subsection light-green-bordered">');
  $form['details']['full-name'] = array('#type' => 'textfield', '#required' => TRUE, '#title' => t('Full name'), '#attributes' => array('class' => array('name-mask')));

  if (isset($_REQUEST['full-name'])) {
    $form['details']['full-name']['#value'] = check_plain($_REQUEST['full-name']);
  }

  $form['details']['phone'] = array('#type' => 'textfield', '#required' => TRUE, '#title' => t('Telephone'), '#attributes' => array('class' => array('phone-mask')));
  if (isset($_REQUEST['phone'])) {
    $form['details']['phone']['#value'] = check_plain($_REQUEST['phone']);
  }

  $form['details']['email'] = array('#type' => 'textfield', '#title' => t('Email address'), '#required' => TRUE, '#description' => t('Your quote is sent to this address'), '#attributes' => array('class' => array('email-mask')));
  if (isset($_REQUEST['email'])) {
    $form['details']['email']['#value'] = check_plain($_REQUEST['email']);
  }

  $form['details']['markup2end'] = array('#type' => 'markup', '#markup' => '</div>');

  global $user;
  if (in_array('salesman', $user->roles)) {
    $form['blanket-discount'] = array('#type' => 'textfield', '#title' => t('Blanket discount'), '#default_value' => NULL, '#size' => 30, '#required' => FALSE, '#prefix' => '<div id="agentextra" class="quoteformblock"><h3>Agent Extras</h3>', '#suffix' => '</div>');
  }

  $submit_value = 'Calculate';

  $form['submit'] = array(
    '#prefix' => '<div class="quote-arrow-box ">',
    '#type' => 'submit',
    '#value' => $submit_value,
    '#attributes' => array('class' => array('green-button large-6 medium-6 small-12')),
    '#suffix' => '<div class="quote-arrow-bg"></div>
                  <div class="quote-arrow"></div><div class="quote-arrow-inner-black"></div>
                  <div class="quote-arrow-inner-green"></div><div class="quote-arrow-inner-box"></div></div>
                  <div class="privacy"><p>We guarantee your privacy.<br />Your information will not be shared.</p></div>
                      ',
  );

  // Validate form.
  if (isset($_REQUEST['invalid'])) {
    $invalid = explode('--', check_plain($_REQUEST['invalid']));
    $contactError = FALSE;
    if (in_array('product', $invalid)) {
      form_set_error('product', 'Please choose the service you require');
    }

    if (in_array('sale-price', $invalid)) {
      form_set_error('sale-price', "If you are selling a property valued below &pound;1000, please call for an accurate quote.");
    }

    if (in_array('purchase-price', $invalid)) {
      form_set_error('purchase-price', "If you are purchasing a property valued below &pound;1000, please call for an accurate quote.");
    }

    if (in_array('toe-propertyValue', $invalid)) {
      form_set_error('toe-propertyValue', "For property valued below &pound;1000, please call for an accurate quote.");
    }

    if (in_array('rem-propertyValue', $invalid)) {
      form_set_error('rem-propertyValue', "For property valued below &pound;1000, please call for an accurate quote.");
    }

    if (in_array('full-name', $invalid)) {
      form_set_error('full-name', 'Please enter your full name.');
      $contactError = TRUE;
    }

    if (in_array('telephone', $invalid)) {
      form_set_error('phone', 'Please enter an accurate telephone number.');
      $contactError = TRUE;
    }

    if (in_array('email', $invalid)) {
      form_set_error('email', 'Please enter a valid email address. A copy of your quote will be sent to you for your reference.');
      $contactError = TRUE;
    }

    if ($contactError == TRUE) {
      drupal_set_message(t('Please note that quotes are only valid with correct contact details.'), 'error');
    }
  }

  // End of validation.
  $form['#action'] = '/your-quote';
  return $form;
}

/**
 *
 */
function quotebuilder_lp_form($form, &$form_state) {
  $pid = navtax_get_uri();
  $location = navtax_get_page_location($pid);
  $form = array();
  $form['location'] = array('#type' => 'hidden', '#value' => $location);
  $form['product'] = array(
    '#prefix' => '<span>Please quote me for:</span><br/>',
    '#type' => 'select',
    '#options' => array(
      'default' => t('--select service--'),
      0 => t('Sale'),
      1 => t('Purchase'),
      4 => t('Sale & Purchase')
    ),
    '#attributes' => array('class' => array('skinned-form-controls')),
    '#default_value' => 'default',
  );
  $form['completion'] = array(
    '#prefix' => '<span>I would like to complete:</span><br/>',
    '#type' => 'select',
    '#options' => array('default' => t('--select timescale--'), 0 => t('within 4 weeks'), 1 => t('in 6 to 8 weeks'), 2 => t('in 8 to 12 weeks'), 4 => t('in over 12 weeks')),
  );
  $submit_value = 'Get a Quote';
  $form['submit'] = array('#prefix' => '<div class="quote-arrow-box">', '#type' => 'submit', '#value' => $submit_value, '#attributes' => array('class' => array('green-button')), '#suffix' => '<div class="quote-arrow"></div><div class="quote-arrow-inner-black"></div><div class="quote-arrow-inner-green"></div><div class="quote-arrow-inner-box"></div></div>');
  return $form;
}

/**
 *
 */
function quotebuilder_lp_form_submit($form, &$form_state) {
  if (isset($form_state['values']['product'])) {
    $select = $form_state['values']['product'];
  }
  else {
    $select = 0;
  }

  if (isset($form_state['values']['completion'])) {
    $completion = $form_state['values']['completion'];
  }
  else {
    $completion = 0;
  }

  if (isset($form_state['values']['location'])) {
    $location = $form_state['values']['location'];
  }
  else {
    $location = 0;
  }

  drupal_goto('quotebuilder', array('query' => array('product' => $select, 'completion' => $completion, 'location' => $location)));
}

/**
 *
 */
function quotebuilder_footer_form($form, &$form_state) {
  $pid = navtax_get_uri();
  $location = navtax_get_page_location($pid);
  $form = array();
  $form['footerLocation'] = array('#type' => 'hidden', '#value' => $location);
  $form['footerProduct'] = array(
    '#prefix' => '<span style="margin-left: 15px;"><b>Please quote me for:</b></span>',
    '#type' => 'select',
    '#options' => array('default' => t('--select service--'), 0 => t('Sale'), 1 => t('Purchase'), 4 => t('Sale & Purchase')),
    '#attributes' => array('class' => array('skinned-form-controls')),
    '#default_value' => 0);

  $form['footerCompletion'] = array('#prefix' => '<span>I would like to complete</span>', '#type' => 'select', '#options' => array('default' => t('--select timescale--'), 0 => t('within 4 weeks'), 1 => t('in 6 to 8 weeks'), 2 => t('in 8 to 12 weeks'), 4 => t('in over 12 weeks')));
  $submit_value = 'Get a Quote';
  $form['submit'] = array('#prefix' => '<div class="quote-arrow-box">', '#type' => 'submit', '#value' => $submit_value, '#attributes' => array('class' => array('green-button')), '#suffix' => '<div class="quote-arrow"></div><div class="quote-arrow-inner-black"></div><div class="quote-arrow-inner-green"></div><div class="quote-arrow-inner-box"></div></div>');
  return $form;
}

/**
 *
 */
function quotebuilder_footer_form_submit($form, &$form_state) {
  if (isset($form_state['values']['footerProduct'])) {
    $select = $form_state['values']['footerProduct'];
  }
  else {
    $select = 0;
  }

  if (isset($form_state['values']['footerCompletion'])) {
    $completion = $form_state['values']['footerCompletion'];
  }
  else {
    $completion = 0;
  }

  drupal_goto('quotebuilder', array('query' => array('product' => $select, 'completion' => $completion)));
}

/**
 *
 */
function quotebuilder_rbg_form($form, &$form_state) {
  $pid = navtax_get_uri();
  $location = navtax_get_page_location($pid);
  $form = array();
  $form['location'] = array('#type' => 'hidden', '#value' => $location);
  $form['product'] = array('#prefix' => '<span style="margin-left: 15px;"><b>Please quote me for:</b></span>',
    '#type' => 'radios',
    '#options' => array(
      0 => t('Sale'),
      1 => t('Purchase'),
      4 => t('Sale & Purchase'),
      2 => t('Transfer of Equity'),
      3 => t('Remortgage')
    ),
    '#attributes' => array('class' => array('skinned-form-controls')),
    '#default_value' => 1
  );
  $submit_value = 'Get a Quote';
  $form['submit'] = array(
    '#prefix' => '<div class="quote-arrow-box text-center">',
    '#type' => 'submit',
    '#value' => $submit_value,
    '#attributes' => array(
      'class' => array('green-button button'),
    ),
    '#suffix' => '<div class="quote-arrow"></div>
                  <div class="quote-arrow-inner-black"></div>
                  <div class="quote-arrow-inner-green"></div>
                  <div class="quote-arrow-inner-box"></div>
                  </div>',
  );
  return $form;
}

/**
 *
 */
function quotebuilder_rbg_form_submit($form, &$form_state) {
  if (isset($form_state['values']['product'])) {
    $select = $form_state['values']['product'];
  }
  else {
    $select = 0;
  }
  drupal_goto('quotebuilder', array('query' => array('product' => $select)));
}

/**
 *
 */
function quotebuilder_rbg_home_form($form, &$form_state) {
  $pid = navtax_get_uri();
  $location = navtax_get_page_location($pid);
  $form = array();
  $form['location'] = array(
    '#type' => 'hidden',
    '#value' => $location,
  );

  $form['product'] = array(
    '#prefix' => '<span class="columns" style="margin-left: 15px;"><b>Please quote me for:</b></span>',
    '#type' => 'radios',
    '#options' => array(
      0 => t('Sale'),
      1 => t('Purchase'),
      4 => t('Sale & Purchase'),
    ),
    '#attributes' => array('class' => array('skinned-form-controls', 'columns small-offset-1 small-7 medium-offset-1 medium-7 large-offset-1 large-7')),
    '#default_value' => 'Sale',
  );
  $submit_value = 'Get a Quote';
  $form['submit'] = array('#prefix' => '<div class="columns small-4 medium-4 large-4">', '#type' => 'submit', '#value' => $submit_value, '#attributes' => array('class' => array('green-button front-quote-button')), '#suffix' => '<div class="quote-arrow"></div><div class="quote-arrow-inner-black"></div><div class="quote-arrow-inner-green"></div><div class="quote-arrow-inner-box"></div></div>');
  return $form;
}

/**
 *
 */
function quotebuilder_rbg_home_form_submit($form, &$form_state) {
  if (isset($form_state['values']['product'])) {
    $select = $form_state['values']['product'];
  }
  else {
    $select = 0;
  }

  drupal_goto('quotebuilder', array('query' => array('product' => $select)));
}

/**
 *
 */
function quotebuilder_askasolicitor_form() {
  $form = array();

  $form['aas_question'] = array(
    '#type' => 'textarea',
    '#default_value' => 'or type your question here',
    '#placeholder' => 'type your message here',
  );

  $form['aas_email'] = array(
    '#type' => 'textfield',
    '#default_value' => 'enter email address',
    '#placeholder' => 'enter email address',
//    '#description' => 'so we can email you a response',
  );

  $form['aas_spam'] = array(
    '#type' => 'textfield',
    '#default_value' => 'spam',
    '#placeholder' => 'spam',
    '#prefix' => '<div class="mhidden">',
    '#suffix' => '</div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Ask',
    '#attributes' => array('class' => array('blue-button')),
  );

  $form['#prefix'] = '<div id="contact-us-form-wrapper" class="contact-us-form-wrapper">';
  $form['#suffix'] = '</div>';

  return $form;
}

/**
 *
 */
function quotebuilder_askasolicitor_form_validate($form, $form_state) {
  if (!valid_message($form_state['values']['aas_question'])) {
    form_set_error('aas_question', 'Please enter a valid message without links');
  }
  if (!valid_email_address($form_state['values']['aas_email'])) {
    form_set_error('aas_email', 'Please enter a valid email address');
  }
}

/**
 *
 */
function quotebuilder_askasolicitor_form_submit($form, $form_state) {

  $spam_a = "false";

  // Regular Expression filter for urls.
  $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
  $question = $form_state['values']['aas_question'];

  // Check if there is a url in the text.
  if (preg_match($reg_exUrl, $question)) {
    // If url exist assume the request is spam.
    drupal_set_message("found a match");
    $spam_a = "true";
  }

  if ($form_state['values']['aas_spam'] != 'spam') {
    // If honey pot triggered the request is spam.
    $spam_a = "true";
  }

  if ($spam_a == 'true') {
    drupal_goto('/');
  }
  else {
    $aas_email = check_plain($form_state['values']['aas_email']);
    $aas_question = check_plain($form_state['values']['aas_question']);
    $fontFamily = "'Palatino Linotype', 'Book Antiqua', Palatino, serif";
    $msg = '<html>
        <body>
          <table align="center" style="background-color: #EEEEEE; font-family: Arial, Helvetica, sans-serif" width="100%">
        <tr>
          <td>
            <table cellspacing="0" align="center" style="font-family: Arial, Helvetica, sans-serif;" style="background-color: #FFFFFF; font-family: Arial, Helvetica, sans-serif" width="600">
              <tr align="left" style="background-color: #FFFFFF">
                <td>
                  <p><font color="#000" size="2" style="font-family:' . $fontFamily . ';">Lead email address: ' . $aas_email . '</font></p>
                </td>
              </tr>
              <tr align="left" style="background-color: #FFFFFF">
                <td>
                  <p><font color="#000" size="2" style="font-family:' . $fontFamily . ';">Lead question: ' . $aas_question . '</font></p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
          </body>
      </html>';

    $from = "ClientServices.Home@homewardlegal.co.uk";
    $to = 'clientservices@homewardlegal.co.uk';

    $module = 'quotebuilder_forms';
    $key = 'contact_us';

    $message['subject'] = 'Client Services';
    $message['body'] = array();
    $message['body'][] = $msg;
    $message['from'] = $from; // XXX
    $message['to'] = $to; // XXX
    $message['headers'] = array(
        'Content-Type' => 'text/plain; charset=UTF-8;', // XXX
        'From' => $from,
        'Sender' => $from,
        'Return-Path' => $from,
    );

    // Retrieve the responsible implementation for this message.
    $system = drupal_mail_system($module, $key);

    // Format the message body.
    $message = $system->format($message);

    // Send e-mail.
    $message['result'] = $system->mail($message);
    
    //send the message, check for errors
    if ($message['result']) {
      watchdog('quotebuilder_forms', 'Email sent.', array(), WATCHDOG_INFO);
    }
    drupal_goto('ask-solicitor');
  }
}

/**
 *
 */
function quotebuilder_direct_callback_form() {
  $form = array();

  $form['#attributes'] = array('class' => 'direct_callback_form');

  $form['yourfullname'] = array(
    '#type' => 'textfield',
    '#placeholder' => 'Your full name',
  );

  $form['yournumber'] = array(
    '#type' => 'textfield',
    '#placeholder' => 'Your phone number',
  );

  $form['youremail'] = array(
    '#type' => 'textfield',
    '#placeholder' => 'Your Email',
  );

  $form['message'] = array(
    '#type' => 'textfield',
    '#placeholder' => 'Any comments?',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Call Me Back',
    '#attributes' => array('class' => array('blue-button')),
  );

  return $form;
}

/**
 *
 */
function quotebuilder_direct_callback_form_submit($form, $form_state) {
  require_once $_SERVER['DOCUMENT_ROOT'] . "/phpmailer/class.phpmailer.php";

  $yournumber = check_plain($form_state['values']['yournumber']);
  $yourname = check_plain($form_state['values']['yourfullname']);
  $youremail = check_plain($form_state['values']['youremail']);
  $yourquestion = check_plain($form_state['values']['message']);

  $fontFamily = "'Palatino Linotype', 'Book Antiqua', Palatino, serif";
  $message = '<html>
    <body>
      <table align="center" style="background-color: #EEEEEE; font-family: Arial, Helvetica, sans-serif" width="100%">
    <tr>
      <td>
        <table cellspacing="0" align="center" style="font-family: Arial, Helvetica, sans-serif;" style="background-color: #FFFFFF; font-family: Arial, Helvetica, sans-serif" width="600">
          <tr align="center" style="background-color: #FFFFFF">
            <td>
              <a href="http://www.homewardlegal.co.uk">
              <font color="#000" size="1"><b>Having trouble viewing this email? Click here to return to Homeward Legal.</b></font></a>
            </td>
          </tr>
          <tr align="center" style="background-color: #FFFFFF">
            <td >
              <img src="/sites/default/files/hl-logo.png" alt="Homeward Legal Quote" />
            </td>
          </tr>
          <tr style="background-color: #FFFFFF"><td>&nbsp;<br></td></tr>
          <tr style="background-color: #FFFFFF"><td>&nbsp;<br></td></tr>
          <tr style="background-color: #FFFFFF"><td>&nbsp;<br></td></tr>
          <tr align="left" style="background-color: #FFFFFF">
            <td>
              <p><font color="#000" size="5" style="font-family:' . $fontFamily . ';">Thank you for your query.</font></p>
            </td>
          </tr>
          <tr align="left" style="background-color: #FFFFFF">
            <td>
              <p><font color="#000" size="2" style="font-family:' . $fontFamily . ';">If you wish to speak to our property legal team to correct any of this information, or to discuss a conveyancing matter, transfer of equity, remortgage or lease extension, please call <a href="tel:08000386699">0800 038 6699</a>.</font></p>
            </td>
          </tr>
          <tr style="background-color: #FFFFFF"><td>&nbsp;<br></td></tr>
          <tr align="left" style="background-color: #FFFFFF">
            <td>
              <p><font color="#000" size="2" style="font-family:' . $fontFamily . ';"><b>Your name:</b> ' . $yourname . '</font></p>
            </td>
          </tr>
          <tr style="background-color: #FFFFFF"><td>&nbsp;<br></td></tr>
          <tr align="left" style="background-color: #FFFFFF">
            <td>
              <p><font color="#000" size="2" style="font-family:' . $fontFamily . ';"><b>Your number:</b> ' . $yournumber . '</font></p>
            </td>
          </tr>
          <tr style="background-color: #FFFFFF"><td>&nbsp;<br></td></tr>
          <tr style="background-color: #FFFFFF"><td>&nbsp;<br></td></tr>
          <tr style="background-color: #FFFFFF"><td>&nbsp;<br></td></tr>

          <tr align="left" style="background-color: #FFFFFF">
            <td>
              <p><font color="#000" size="1">All solicitors introduced by Homeward Legal are regulated by the SRA Solicitor Regulation Authority and have Law Society Conveyancing Quality Scheme (CQS) accreditation which provides a recognised quality standard for residential conveyancing practices. Homeward Legal comply with, and are subject to, the Solicitors\' Code of Conduct 2011 published by the Solicitors Regulation Authority (SRA).</font></p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
      </body>
    </html>';

  $pid = '/' . arg(0) . '/' . arg(1) . '';
  $vid = 2;
  $tid = db_query("SELECT tid FROM {navtax_page_terms} WHERE vid = :vid AND pid = :pid", array(':vid' => $vid, ':pid' => $pid))->fetchField();
  $location = db_query("SELECT name FROM {taxonomy_term_data} WHERE tid = :tid", array(':tid' => $tid))->fetchField();

  db_insert('callback_request')
    ->fields(array(
      'date' => time(),
      'location' => $location,
      'name' => $yourname,
      'phone' => $yournumber,
      'email' => $youremail,
      'message' => $yourquestion,
    ))
    ->execute();

  // send aas to LT.
  $date = time();
  $phone = check_plain($yournumber);
  $name = check_plain($yourname);
  $email = check_plain($youremail);
  $question = check_plain($yourquestion);

  // Send aas to NEW LT.
  $con = mysql_connect("81.201.129.92", "fitzalan_user", "7.nPy]Z#]T2w");
  mysql_select_db('fitzalan_db', $con);
  $result = mysql_query("INSERT INTO raw_callback (cllsite,name,phone,email,description,created,cllservice,type)
    VALUES (
      'HL',
      '$name',
      '$phone',
      '$email',
      '$question',
      '$date',
      'CBA',
      'cba'
    )");

  if (!$result) {
    dsm(mysql_error($con));
  }
  // drupal_set_message(t('Thank you for your question - You should receive a confirmation email shortly.'));.
  drupal_goto('callback-request');
}
