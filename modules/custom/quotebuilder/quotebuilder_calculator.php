<?php

/**
 * @file
 * @file
 * @file
 * .*/

/**
 *
 *
 */
 function quotebuilder_calculate_quote($variables) {
  $quote = array();
  $quote['sale'] = array();
  $quote['purchase'] = array();
  $quote['remortgage'] = array();
  $quote['toe'] = array();

  if ($variables['product'] == 0 || $variables['product'] == 2) {
    // Sale.
    $quote['sale']['sale_mortgage'] = 0;
    $quote['sale']['leasehold_supplement'] = 0;
    $quote['sale']['copy_lease'] = 0;
    $quote['sale']['onlineidfee'] = 0;
    $quote['sale']['admin_fee'] = 0;
    $quote['sale']['tt_fee'] = 0;
    $quote['sale']['oc_fee'] = 0;
    $quote['sale']['fee'] = 0;
    $quote['sale']['oc_fee'] = variable_get('qv_sale_oc_fee', 6);
    if ($variables['sale-mortgage'] == 'yes') {
      $quote['sale']['sale_mortgage'] = variable_get('qv_sale_mortgage', 60);
    }

    switch ($variables['sale-tenure']) {
      case 'leasehold':
      case 'sharehold':
        $quote['sale']['leasehold_supplement'] = variable_get('qv_sale_leasehold_supplement', 110);
        $quote['sale']['copy_lease'] = variable_get('qv_sale_copy_lease', 0);
        $quote['sale']['oc_fee'] = 12;

        break;
    }

    $quote['sale']['onlineidfee'] = variable_get('qv_online_id', 0);
    $quote['sale']['admin_fee'] = variable_get('qv_sale_admin_fee', 0);
    $quote['sale']['tt_fee'] = variable_get('qv_sale_tt_fee', 0);

    if (_quotemaker_inrange($variables['sale-price'], 0, 100000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_000_100', 375);
    }
    if (_quotemaker_inrange($variables['sale-price'], 100000, 200000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_100_200', 375);
    }
    if (_quotemaker_inrange($variables['sale-price'], 200000, 300000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_200_300', 375);
    }
    if (_quotemaker_inrange($variables['sale-price'], 300000, 400000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_300_400', 435);
    }
    if (_quotemaker_inrange($variables['sale-price'], 400000, 500000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_400_500', 435);
    }
    if (_quotemaker_inrange($variables['sale-price'], 500000, 600000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_500_600', 435);
    }
    if (_quotemaker_inrange($variables['sale-price'], 600000, 700000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_600_700', 435);
    }
    if (_quotemaker_inrange($variables['sale-price'], 700000, 800000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_700_800', 535);
    }
    if (_quotemaker_inrange($variables['sale-price'], 800000, 900000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_800_900', 635);
    }
    if (_quotemaker_inrange($variables['sale-price'], 900000, 1000000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_900_1000', 635);
    }
    if (_quotemaker_inrange($variables['sale-price'], 1000000, 2000000)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_1000_2000', 935);
    }
    if (_quotemaker_inrange($variables['sale-price'], 2000000, -1)) {
      $quote['sale']['fee'] = variable_get('qv_sale_fee_2000_up', 1305);
    }
    $quote['sale']['total_ex_vat'] = $quote['sale']['fee'] + $quote['sale']['admin_fee'] + $quote['sale']['copy_lease'] + $quote['sale']['tt_fee'] + $quote['sale']['leasehold_supplement'] + $quote['sale']['onlineidfee'] + $quote['sale']['sale_mortgage'];
    $quote['sale']['total_vat'] = _quotemaker_vat($quote['sale']['total_ex_vat']);
  } // End sale

  if ($variables['product'] == 1 || $variables['product'] == 2) {
    // Purchase.
    $quote['purchase']['purchase_mortgage'] = 0;
    $quote['purchase']['purchase_helptobuy'] = 0;
    $quote['purchase']['leasehold_supplement'] = 0;
    $quote['purchase']['leasehold_fee'] = 0;
    $quote['purchase']['onlineidfee'] = 0;
    $quote['purchase']['admin_fee'] = 0;
    $quote['purchase']['elec_bank'] = 0;
    $quote['purchase']['land_search_fee'] = 0;
    $quote['purchase']['bankruptcy_fee'] = 0;
    $quote['purchase']['fee'] = 0;
    $quote['purchase']['land_reg_charge'] = 0;
    $quote['purchase']['stamp_duty'] = 0;
    $quote['purchase']['land_reg_docs'] = 6;
    $quote['purchase']['search_bundle'] = 0;

    if ($variables['purchase-mortgage'] == 'yes') {
      $quote['purchase']['purchase_mortgage'] = variable_get('qv_purchase_mortgage', 60);
    }
    if ($variables['purchase-helptobuy'] == 'yes') {
      $quote['purchase']['purchase_helptobuy'] = 100;
    }

    switch ($variables['purchase-tenure']) {
      case 'leasehold':
      case 'sharehold':
        $quote['purchase']['leasehold_supplement'] = variable_get('qv_purchase_leasehold_supplement', 150);
        // $quote['purchase']['land_reg_docs'] = 12;.
        break;
    }

    $quote['purchase']['onlineidfee'] = variable_get('qv_online_id', 0);
    $quote['purchase']['admin_fee'] = variable_get('qv_purchase_admin_fee', 0);
    $quote['purchase']['elec_bank'] = variable_get('qv_purchase_elec_bank', 0);
    $quote['purchase']['land_search_fee'] = variable_get('qv_purchase_land_search_fee', 6);
    $quote['purchase']['bankruptcy_fee'] = variable_get('qv_purchase_bankruptcy_fee', 0);

    if (_quotemaker_inrange($variables['purchase-price'], 0, 100000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_000_100', 391);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 100000, 200000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_100_200', 391);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 200000, 300000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_200_300', 391);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 300000, 400000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_300_400', 451);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 400000, 500000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_400_500', 451);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 500000, 600000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_500_600', 451);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 600000, 700000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_600_700', 451);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 700000, 800000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_700_800', 551);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 800000, 900000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_800_900', 651);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 900000, 1000000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_900_1000', 651);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 1000000, 2000000)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_1000_2000', 951);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 2000000, -1)) {
      $quote['purchase']['fee'] = variable_get('qv_purchase_fee_2000_up', 1401);
    }

    if (_quotemaker_inrange($variables['purchase-price'], 0, 50000)) {
      $quote['purchase']['land_reg_charge'] = variable_get('qv_purchase_land_reg_charge_0_50', 20);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 50000, 80000)) {
      $quote['purchase']['land_reg_charge'] = variable_get('qv_purchase_land_reg_charge_50_80', 20);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 80000, 100000)) {
      $quote['purchase']['land_reg_charge'] = variable_get('qv_purchase_land_reg_charge_80_100', 40);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 100000, 200000)) {
      $quote['purchase']['land_reg_charge'] = variable_get('qv_purchase_land_reg_charge_100_200', 95);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 200000, 500000)) {
      $quote['purchase']['land_reg_charge'] = variable_get('qv_purchase_land_reg_charge_200_500', 135);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 500000, 1000000)) {
      $quote['purchase']['land_reg_charge'] = variable_get('qv_purchase_land_reg_charge_500_1000', 270);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 1000000, -1)) {
      $quote['purchase']['land_reg_charge'] = variable_get('qv_purchase_land_reg_charge_1000_up', 455);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 0, 125001)) {
      $quote['purchase']['stamp_duty'] = (variable_get('qv_purchase_stamp_duty_0_175', 0) * $variables['purchase-price']);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 125001, 250001)) {
      $quote['purchase']['stamp_duty'] = (variable_get('qv_purchase_stamp_duty_175_250', 0.01) * $variables['purchase-price']);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 250001, 500001)) {
      $quote['purchase']['stamp_duty'] = (variable_get('qv_purchase_stamp_duty_250_500', 0.03) * $variables['purchase-price']);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 500001, 1000001)) {
      $quote['purchase']['stamp_duty'] = (variable_get('qv_purchase_stamp_duty_500_up', 0.04) * $variables['purchase-price']);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 1000001, 2000001)) {
      $quote['purchase']['stamp_duty'] = (variable_get('qv_purchase_stamp_duty_1000_up', 0.05) * $variables['purchase-price']);
    }
    if (_quotemaker_inrange($variables['purchase-price'], 2000001, -1)) {
      $quote['purchase']['stamp_duty'] = (variable_get('qv_purchase_stamp_duty_1000_up', 0.07) * $variables['purchase-price']);
    }
    $quote['purchase']['total_ex_vat'] = $quote['purchase']['fee'] + $quote['purchase']['bankruptcy_fee'] + $quote['purchase']['admin_fee'] + $quote['purchase']['elec_bank'] + $quote['purchase']['onlineidfee'] + $quote['purchase']['leasehold_supplement'] + $quote['purchase']['purchase_mortgage'];
    $quote['purchase']['total_vat'] = _quotemaker_vat($quote['purchase']['total_ex_vat'] + $quote['purchase']['purchase_helptobuy']);
  }

  if ($variables['product'] == 3) {
    // Transfer of equity.
    $quote['toe']['tt_fee'] = 0;
    $quote['toe']['onlineidfee'] = 0;
    $quote['toe']['admin_fee'] = 0;
    $quote['toe']['oc_fee'] = 0;
    $quote['toe']['search_fee'] = 0;
    $quote['toe']['fee'] = 0;
    $quote['toe']['land_reg_charge'] = 0;

    $quote['toe']['admin_fee'] = variable_get('qv_remortgage_admin_fee', 0);
    $quote['toe']['onlineidfee'] = variable_get('qv_remortgage_id_fee', 0);
    $quote['toe']['tt_fee'] = variable_get('qv_remortgage_tt_fee', 0);
    $quote['toe']['oc_fee'] = variable_get('qv_remortgage_oc_fee', 7);

    if ($variables['toe-legal'] == 'yes') {
      $quote['toe']['search_fee'] = variable_get('qv_remortgage_search_fee', 20);
    }
    else {
      $quote['toe']['search_fee'] = 0;
    }

    $quote['toe']['fee'] = 0;
    $quote['toe']['land_reg_charge'] = variable_get('qv_remortgage_land_fee', 40);

    if ($variables['toe-legal'] != 'yes') {
      if (_quotemaker_inrange($variables['toe-propertyValue'], 0, 50000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_000_050', 259);
        $quote['toe']['land_reg_charge'] = 20;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 50000, 100000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_050_100', 259);
        $quote['toe']['land_reg_charge'] = 20;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 100000, 200000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_100_200', 259);
        $quote['toe']['land_reg_charge'] = 30;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 200000, 300000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_fee_200_300', 259);
        $quote['toe']['land_reg_charge'] = 40;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 300000, 400000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_fee_300_400', 259);
        $quote['toe']['land_reg_charge'] = 40;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 400000, 500000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_400_500', 259);
        $quote['toe']['land_reg_charge'] = 40;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 500000, 600000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_500_600', 259);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 600000, 700000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_600_700', 259);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 700000, 800000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_700_800', 259);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 800000, 900000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_fee_800_900', 259);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 900000, 1000000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_900_1000', 259);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 1000000, 2000000)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_1000_2000', -1);
        $quote['toe']['land_reg_charge'] = 125;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 2000000, -1)) {
        $quote['toe']['fee'] = variable_get('qv_toe_fee_fee_2000_up', -1);
        $quote['toe']['land_reg_charge'] = 125;
      }
      // ToE search indemnity.
      if (_quotemaker_inrange($variables['toe-propertyValue'], 0, 250000)) {
        $quote['toe']['search_fee'] = '25.25';
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 250001, 500000)) {
        $quote['toe']['search_fee'] = '38.36';
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 500001, 750000)) {
        $quote['toe']['search_fee'] = '58.55';
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 750001, 1000000)) {
        $quote['toe']['search_fee'] = '70.67';
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 1000001, 1250000)) {
        $quote['toe']['search_fee'] = '90.86';
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 1250001, 1500000)) {
        $quote['toe']['search_fee'] = '111.05';
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 1500001, 1750000)) {
        $quote['toe']['search_fee'] = '131.24';
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 1750001, -1)) {
        $quote['toe']['search_fee'] = '151.43';
      }
    }
    else {
      if (_quotemaker_inrange($variables['toe-propertyValue'], 0, 50000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_000_050', 325);
        $quote['toe']['land_reg_charge'] = 20;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 50000, 100000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_050_100', 325);
        $quote['toe']['land_reg_charge'] = 20;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 100000, 200000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_100_200', 385);
        $quote['toe']['land_reg_charge'] = 30;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 200000, 300000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_fee_200_300', 425);
        $quote['toe']['land_reg_charge'] = 40;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 300000, 400000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_fee_300_400', 425);
        $quote['toe']['land_reg_charge'] = 40;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 400000, 500000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_400_500', 425);
        $quote['toe']['land_reg_charge'] = 40;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 500000, 600000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_500_600', 475);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 600000, 700000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_600_700', 475);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 700000, 800000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_700_800', 475);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 800000, 900000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_fee_800_900', 475);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 900000, 1000000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_900_1000', 475);
        $quote['toe']['land_reg_charge'] = 60;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 1000000, 2000000)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_1000_2000', -1);
        $quote['toe']['land_reg_charge'] = 125;
      }
      if (_quotemaker_inrange($variables['toe-propertyValue'], 2000000, -1)) {
        $quote['toe']['fee'] = variable_get('qv_nor_toe_fee_fee_2000_up', -1);
        $quote['toe']['land_reg_charge'] = 125;
      }
    }
    $quote['toe']['total_ex_vat'] = $quote['toe']['fee'] + $quote['toe']['tt_fee'] + $quote['toe']['admin_fee'] + $quote['toe']['onlineidfee'];
    $quote['toe']['total_vat'] = _quotemaker_vat($quote['toe']['total_ex_vat']);
  }

  if ($variables['product'] == 4) {
    // Remortgage.
    $quote['remortgage']['tt_fee'] = 0;
    $quote['remortgage']['onlineidfee'] = 0;
    $quote['remortgage']['admin_fee'] = 0;
    $quote['remortgage']['oc_fee'] = 0;
    $quote['remortgage']['search_fee'] = 0;
    $quote['remortgage']['fee'] = 0;
    $quote['remortgage']['land_reg_charge'] = 0;

    $quote['remortgage']['admin_fee'] = variable_get('qv_remortgage_admin_fee', 0);
    $quote['remortgage']['onlineidfee'] = variable_get('qv_remortgage_id_fee', 0);
    $quote['remortgage']['tt_fee'] = variable_get('qv_remortgage_tt_fee', 0);
    $quote['remortgage']['oc_fee'] = variable_get('qv_remortgage_oc_fee', 7);
    $quote['remortgage']['search_fee'] = variable_get('qv_remortgage_search_fee', 20);
    $quote['remortgage']['fee'] = 0;
    $quote['remortgage']['land_reg_charge'] = variable_get('qv_remortgage_land_fee', 40);
    if (_quotemaker_inrange($variables['rem-propertyValue'], 0, 50000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_000_050', 199);
      $quote['remortgage']['land_reg_charge'] = 20;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 50000, 100000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_050_100', 199);
      $quote['remortgage']['land_reg_charge'] = 20;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 100000, 200000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_100_200', 245);
      $quote['remortgage']['land_reg_charge'] = 30;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 200000, 300000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_fee_200_300', 245);
      $quote['remortgage']['land_reg_charge'] = 40;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 300000, 400000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_fee_300_400', 245);
      $quote['remortgage']['land_reg_charge'] = 40;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 400000, 500000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_400_500', 245);
      $quote['remortgage']['land_reg_charge'] = 40;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 500000, 600000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_500_600', 299);
      $quote['remortgage']['land_reg_charge'] = 60;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 600000, 700000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_600_700', 299);
      $quote['remortgage']['land_reg_charge'] = 60;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 700000, 800000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_700_800', 299);
      $quote['remortgage']['land_reg_charge'] = 60;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 800000, 900000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_fee_800_900', 299);
      $quote['remortgage']['land_reg_charge'] = 60;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 900000, 1000000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_900_1000', 299);
      $quote['remortgage']['land_reg_charge'] = 60;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 1000000, 2000000)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_1000_2000', 359);
      $quote['remortgage']['land_reg_charge'] = 125;
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 2000000, -1)) {
      $quote['remortgage']['fee'] = variable_get('qv_remort_fee_fee_2000_up', -1);
      $quote['remortgage']['land_reg_charge'] = 125;
    }

    // Remort search indemnity.
    if (_quotemaker_inrange($variables['rem-propertyValue'], 0, 250000)) {
      $quote['remortgage']['search_fee'] = '25.25';
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 250001, 500000)) {
      $quote['remortgage']['search_fee'] = '38.36';
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 500001, 750000)) {
      $quote['remortgage']['search_fee'] = '58.55';
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 750001, 1000000)) {
      $quote['remortgage']['search_fee'] = '70.67';
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 1000001, 1250000)) {
      $quote['remortgage']['search_fee'] = '90.86';
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 1250001, 1500000)) {
      $quote['remortgage']['search_fee'] = '111.05';
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 1500001, 1750000)) {
      $quote['remortgage']['search_fee'] = '131.24';
    }
    if (_quotemaker_inrange($variables['rem-propertyValue'], 1750001, -1)) {
      $quote['remortgage']['search_fee'] = '151.43';
    }

    $quote['remortgage']['total_ex_vat'] = $quote['remortgage']['fee'] + $quote['remortgage']['tt_fee'] + $quote['remortgage']['admin_fee'] + $quote['remortgage']['onlineidfee'];
    $quote['remortgage']['total_vat'] = _quotemaker_vat($quote['remortgage']['total_ex_vat']);
  }

  return $quote;
  }

  /**
   *
   */
  function _quotemaker_inrange($value, $bottom, $top) {
    if ($bottom != (-1) && $top != (-1)) {
      if ($bottom < $value && $value <= $top) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    elseif ($bottom != (-1) && $top == (-1)) {
      if ($bottom < $value) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    elseif ($bottom == (-1) && $top != (-1)) {
      if ($value <= $top) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   *
   */
  function _quotemaker_vat($value) {
    return $value * 0.2;
  }
