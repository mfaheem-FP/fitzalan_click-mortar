(function ($) {
  $(document).ready(function() {
    var ignoreName = false;
    $('#edit-name').keyup(function(event) {build_url(ignoreName)});
    $('#edit-primeterms').keyup(function(event) {build_url(ignoreName)});
    $('#edit-name').blur(function() {build_url(ignoreName)});
    $('#edit-primeterms').blur(function() {build_url(ignoreName)});
    $('#edit-ignore-name').click(function() {if ($(this).is(':checked')) ignoreName = true; else ignoreName = false; build_url(ignoreName);});
    /*$(window).load(function() {
      var mainoffset = $('#block-system-main').offset();
      var mapsoffset = $('#navtax-maps').offset();
      var mainheight = $('#block-system-main').css('height');
      var mapsheight = $('#navtax-maps').css('height');
      
      var mainbottom = Math.round(mainoffset.top) + parseInt(mainheight);
      var mapsbottom = Math.round(mapsoffset.top) + parseInt(mapsheight);
      
      var mapsheightnew = parseInt(mapsheight) + (mainbottom - mapsbottom);
      alert(mainbottom);
      $('#navtax-maps .bordered-inner').css('height', Math.round(mapsheightnew - 30) + 'px');
    })*/
    
  });
  
  function build_url(ignoreName) {
    var name = $('#edit-name').val().replace(/\s/g, "-");
    var termsRaw = $('#edit-primeterms').val().split(',');
    var urls = '';
    var thisurl = '';
    while (termsRaw.length) {
      thisurl = termsRaw.shift().toLowerCase()
      if (thisurl.substring(0,1) == ' ') thisurl = thisurl.substring(1);
      thisurl = thisurl.replace(/\s/g, "-");
      if (!ignoreName) thisurl += '-' + name.toLowerCase();
      urls += ', ' + thisurl.replace(/\s/g, "");
    }
    $('#edit-urls').val(urls.substring(1));
  }

})(jQuery);