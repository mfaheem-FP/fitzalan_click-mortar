<?php

namespace Drupal\quote_builder\Validator;

/**
 * Class ValidatorRequired.
 *
 * @package Drupal\quote_builder\Validator
 */
class ValidatorRequired extends BaseValidator {

  /**
   * {@inheritdoc}
   */
  public function validates($value) {
    return is_array($value) ? !empty(array_filter($value)) : !empty($value);
  }

}
