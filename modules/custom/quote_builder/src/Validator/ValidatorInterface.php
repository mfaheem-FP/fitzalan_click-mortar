<?php

namespace Drupal\quote_builder\Validator;

/**
 * Interface ValidatorInterface.
 *
 * @package Drupal\quote_builder\Validator
 */
interface ValidatorInterface {

  /**
   * Returns bool indicating if validation is ok.
   */
  public function validates($value);

  /**
   * Returns error message.
   */
  public function getErrorMessage();

}
