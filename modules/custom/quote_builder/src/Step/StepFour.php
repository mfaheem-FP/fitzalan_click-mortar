<?php

namespace Drupal\quote_builder\Step;

use Drupal\quote_builder\Button\StepFourPreviousButton;
use Drupal\quote_builder\Button\StepFourNextButton;
use Drupal\quote_builder\Validator\ValidatorRegex;
use Drupal\quote_builder\Validator\ValidatorRequired;

/**
 * Class StepFour.
 *
 * @package Drupal\quote_builder\Step
 */
class StepFour extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_FOUR;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      new StepFourPreviousButton(),
      new StepFourNextButton(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {

    $form['s_property_value'] = [
      '#type' => 'textfield',
        '#title' => t('What is the price of the property you are selling?'),
        '#placeholder' => t('£265,000'),
        '#description' => t('If you don\'t yet have an exact figure, please give an estimate'),
      '#required' => FALSE,
        '#default_value' => isset($this->getValues()['s_property_value']) ? $this->getValues()['s_property_value'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [
      's_property_value',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [
      's_property_value' => [
        new ValidatorRequired("Tell us the value of the property you are selling."),
//        new ValidatorRegex(t("I don't think this is a valid URL..."), '/(ftp|http|https):\/\/(.*)/'),
      ],
    ];
  }

}
