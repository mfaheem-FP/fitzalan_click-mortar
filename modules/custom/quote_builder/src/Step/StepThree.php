<?php

namespace Drupal\quote_builder\Step;

use Drupal\quote_builder\Button\StepThreePreviousButton;
use Drupal\quote_builder\Button\StepThreeNextButton;
use Drupal\quote_builder\Validator\ValidatorRegex;
use Drupal\quote_builder\Validator\ValidatorRequired;

/**
 * Class StepThree.
 *
 * @package Drupal\quote_builder\Step
 */
class StepThree extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_THREE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      new StepThreePreviousButton(),
      new StepThreeNextButton(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {

    $form['s_tenure'] = [
      '#type' => 'radios',
      '#title' => t('What is the type of property you are selling?'),
      '#required' => FALSE,
      '#options' => [
         '1' => t('Freehold'),
         '2' => t('Leasehold'),
         '3' => t('Share of freehold'),
        ],
        '#default_value' => isset($this->getValues()['services']) ? $this->getValues()['services'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [
      's_tenure',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [
      's_tenure' => [
        new ValidatorRequired("Tell us what type of property you are selling."),
      ],
    ];
  }

}
