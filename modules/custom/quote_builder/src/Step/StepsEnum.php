<?php

namespace Drupal\quote_builder\Step;

/**
 * Class StepsEnum.
 *
 * @package Drupal\quote_builder\Step
 */
abstract class StepsEnum {

  /**
   * Steps used in form.
   */
  const STEP_ONE = 1;
  const STEP_TWO = 2;
  const STEP_THREE = 3;
  const STEP_FOUR = 4;
  const STEP_FIVE = 5;
  const STEP_SIX = 6;
  const STEP_SEVEN = 7;
  const STEP_EIGHT = 8;
  const STEP_FINALIZE = 9;

  /**
   * Return steps associative array.
   *
   * @return array
   *   Associative array of steps.
   */
  public static function toArray() {
    return [
      self::STEP_ONE => 'step-one',
      self::STEP_TWO => 'step-two',
      self::STEP_THREE => 'step-three',
      self::STEP_FOUR => 'step-four',
        self::STEP_FIVE => 'step-five',
        self::STEP_FIVE => 'step-six',
        self::STEP_FIVE => 'step-seven',
        self::STEP_FIVE => 'step-eight',
      self::STEP_FINALIZE => 'step-finalize',
    ];
  }

  /**
   * Map steps to it's class.
   *
   * @param int $step
   *   Step number.
   *
   * @return bool
   *   Return true if exist.
   */
  public static function map($step) {
    $map = [
      self::STEP_ONE => 'Drupal\\quote_builder\\Step\\StepOne',
      self::STEP_TWO => 'Drupal\\quote_builder\\Step\\StepTwo',
      self::STEP_THREE => 'Drupal\\quote_builder\\Step\\StepThree',
      self::STEP_FOUR => 'Drupal\\quote_builder\\Step\\StepFour',
        self::STEP_FIVE => 'Drupal\\quote_builder\\Step\\StepFive',
        self::STEP_SIX => 'Drupal\\quote_builder\\Step\\StepSix',
        self::STEP_SEVEN => 'Drupal\\quote_builder\\Step\\StepSeven',
        self::STEP_EIGHT => 'Drupal\\quote_builder\\Step\\StepEight',
      self::STEP_FINALIZE => 'Drupal\\quote_builder\\Step\\StepFinalize',
    ];

    return isset($map[$step]) ? $map[$step] : FALSE;
  }

}
