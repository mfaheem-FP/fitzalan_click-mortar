<?php

namespace Drupal\quote_builder\Step;

use Drupal\quote_builder\Button\StepTwoNextButton;
use Drupal\quote_builder\Button\StepTwoPreviousButton;
use Drupal\quote_builder\Validator\ValidatorRequired;

/**
 * Class StepTwo.
 *
 * @package Drupal\quote_builder\Step
 */
class StepTwo extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_TWO;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      new StepTwoPreviousButton(),
      new StepTwoNextButton(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {
    $form['s_mortgage'] = [
      '#type' => 'radios',
      '#title' => t('Is there a mortgage on the property?'),
      '#required' => FALSE,
      '#options' => [
          '1' => t('Yes'),
          '2' => t('No'),
      ],
      '#default_value' => isset($this->getValues()['s_mortgage']) ? $this->getValues()['s_mortgage'] : [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [
      's_mortgage',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [
      's_mortgage' => [
        new ValidatorRequired("Tell us if is there a mortgage on the property."),
      ],
    ];
  }

}
