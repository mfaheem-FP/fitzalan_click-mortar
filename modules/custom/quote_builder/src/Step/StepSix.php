<?php

namespace Drupal\quote_builder\Step;

use Drupal\quote_builder\Button\StepSixPreviousButton;
use Drupal\quote_builder\Button\StepSixNextButton;
use Drupal\quote_builder\Validator\ValidatorRegex;
use Drupal\quote_builder\Validator\ValidatorRequired;

/**
 * Class StepSix.
 *
 * @package Drupal\quote_builder\Step
 */
class StepSix extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_SIX;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      new StepSixPreviousButton(),
      new StepSixNextButton(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {

    $form['s_tenure1'] = [
      '#type' => 'radios',
      '#title' => t('What is the type of property you are selling?'),
      '#required' => FALSE,
      '#options' => [
         '1' => t('Freehold'),
         '2' => t('Leasehold'),
         '3' => t('Share of freehold'),
        ],
        '#default_value' => isset($this->getValues()['services']) ? $this->getValues()['services'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [
      's_tenure1',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [
      's_tenure1' => [
        new ValidatorRequired("Tell us what type of property you are selling."),
      ],
    ];
  }
}
