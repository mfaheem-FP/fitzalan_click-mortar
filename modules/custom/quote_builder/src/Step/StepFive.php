<?php

namespace Drupal\quote_builder\Step;

use Drupal\quote_builder\Button\StepFivePreviousButton;
use Drupal\quote_builder\Button\StepFiveNextButton;
use Drupal\quote_builder\Validator\ValidatorRegex;
use Drupal\quote_builder\Validator\ValidatorRequired;

/**
 * Class StepFive.
 *
 * @package Drupal\quote_builder\Step
 */
class StepFive extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_FIVE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      new StepFivePreviousButton(),
      new StepFiveNextButton(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {

    $form['s_property_postcode'] = [
      '#type' => 'textfield',
      '#title' => t('What is the postcode of the property you’re selling?'),
      '#placeholder' => t('e.g. RG4'),
      '#required' => FALSE,
      '#default_value' => isset($this->getValues()['s_property_postcode']) ? $this->getValues()['s_property_postcode'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [
      's_property_postcode',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [
      's_property_postcode' => [
        new ValidatorRequired("Postcode field is required."),
      ],
    ];
  }
}
