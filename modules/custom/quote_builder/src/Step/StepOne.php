<?php

namespace Drupal\quote_builder\Step;

use Drupal\quote_builder\Button\StepOneNextButton;
use Drupal\quote_builder\Validator\ValidatorRequired;

/**
 * Class StepOne.
 *
 * @package Drupal\quote_builder\Step
 */
class StepOne extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_ONE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      new StepOneNextButton(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {
    $form['services'] = [
      '#type' => 'radios',
      '#title' => t("Which service can we quote you for?"),
      '#required' => FALSE,
      '#options' => [
        '2' => t('Purchase'),
        '1' => t('Sale'),
        '5' => t('Sale & Purchase'),
        '3' => t('Remortgage'),
        '4' => t('Transfer of Equity')
       ],
      '#default_value' => isset($this->getValues()['services']) ? $this->getValues()['services'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames() {
    return [
      'services',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsValidators() {
    return [
      'services' => [
        new ValidatorRequired("Please select the servies you require."),
      ],
    ];
  }

}
