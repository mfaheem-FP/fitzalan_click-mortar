<?php

namespace Drupal\quote_builder\Step;

/**
 * Class StepFinalize.
 *
 * @package Drupal\quote_builder\Step
 */
class StepFinalize extends BaseStep {

  /**
   * {@inheritdoc}
   */
  protected function setStep() {
    return StepsEnum::STEP_FINALIZE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildStepFormElements() {

    $form['details']['person_full_name'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => t('Full name'),
        '#placeholder' => t('Full name'),
        '#attributes' => array('class' => array('full-name-mask'))
    ];

    $form['details']['person_phone'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => t('Telephone'),
        '#placeholder' => t('Telephone'),
        '#attributes' => array('class' => array('phone-mask'))
    ];

    $form['details']['person_email'] = [
        '#type' => 'textfield',
        '#title' => t('Email address'),
        '#placeholder' => t('Email address'),
        '#required' => TRUE,
        '#description' => t('Your quote is sent to this address'),
        '#attributes' => array('class' => array('email-mask'))
    ];


    $form['completed'] = [
      '#markup' => t('<p class="fs-subtext msform-quotes">We’re just getting your fixed fee quote now…</p>
      <p class="fs-subtext msform-quotes">We do not have hidden fees – what you see is what you pay</p>
      <p class="fs-subtext msform-quotes">A member of our conveyancing team will be in touch to help</p>
      <p class="fs-subtext msform-quotes">If you instruct with Homeward Legal, your solicitor will call you within the hour</p>
      <p class="fs-subtext msform-quotes">Should your transaction fall through, you will be protected by our No Completion, No Fee Guarantee</p>
      <p class="fs-subtext msform-quotes">Our friendly and professional team are always on hand to answer your questions</p>
      <p class="fs-subtext msform-quotes">We are upfront about third party costs, and you’ll see them in your quote</p>
      <p class="fs-subtext msform-quotes">Homeward Legal has hundreds of excellent reviews on TrustPilot and Google – you can trust us</p>
      <p class="fs-subtext msform-quotes">Homeward Legal is open 7 days a week, including bank holidays, to help you along the way</p>
'),
    ];

    return $form;
  }

}
