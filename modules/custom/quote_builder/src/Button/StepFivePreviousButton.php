<?php

namespace Drupal\quote_builder\Button;

use Drupal\quote_builder\Step\StepsEnum;

/**
 * Class StepFivePreviousButton.
 *
 * @package Drupal\quote_builder\Button
 */
class StepFivePreviousButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'previous';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Previous'),
      '#goto_step' => StepsEnum::STEP_FOUR,
      '#skip_validation' => TRUE,
    ];
  }
}
