<?php

namespace Drupal\quote_builder\Button;

use Drupal\quote_builder\Step\StepsEnum;

/**
 * Class StepOneNextButton.
 *
 * @package Drupal\quote_builder\Button
 */
class StepOneNextButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey($key = 'next') {
    return $key;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Next'),
      '#goto_step' => StepsEnum::STEP_TWO,
    ];
  }
}
