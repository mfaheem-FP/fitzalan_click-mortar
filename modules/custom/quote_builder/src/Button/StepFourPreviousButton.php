<?php

namespace Drupal\quote_builder\Button;

use Drupal\quote_builder\Step\StepsEnum;

/**
 * Class StepThreePreviousButton.
 *
 * @package Drupal\quote_builder\Button
 */
class StepFourPreviousButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'previous';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Previous'),
      '#goto_step' => StepsEnum::STEP_THREE,
      '#skip_validation' => TRUE,
    ];
  }

}
