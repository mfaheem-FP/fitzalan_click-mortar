<?php

namespace Drupal\quote_builder\Button;

use Drupal\quote_builder\Step\StepsEnum;

/**
 * Class StepThreeFinishButton.
 *
 * @package Drupal\quote_builder\Button
 */
class StepNextButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'next';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
        '#type' => 'submit',
        '#value' => t('Next'),
        '#goto_step' => StepsEnum::STEP_NEXT,
    ];
  }
}
