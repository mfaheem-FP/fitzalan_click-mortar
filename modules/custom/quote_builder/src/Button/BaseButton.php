<?php

namespace Drupal\quote_builder\Button;

/**
 * Class BaseButton.
 *
 * @package Drupal\quote_builder\Button
 */
abstract class BaseButton implements ButtonInterface {

  /**
   * {@inheritdoc}
   */
  public function ajaxify() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitHandler() {
    return FALSE;
  }

}
