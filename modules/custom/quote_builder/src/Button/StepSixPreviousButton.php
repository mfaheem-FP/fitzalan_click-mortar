<?php

namespace Drupal\quote_builder\Button;

use Drupal\quote_builder\Step\StepsEnum;

/**
 * Class StepSixPreviousButton.
 *
 * @package Drupal\quote_builder\Button
 */
class StepSixPreviousButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'previous';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Previous'),
      '#goto_step' => StepsEnum::STEP_FIVE,
      '#skip_validation' => TRUE,
    ];
  }
}
