<?php

namespace Drupal\quote_builder\Button;

use Drupal\quote_builder\Step\StepsEnum;

/**
 * Class StepThreeFinishButton.
 *
 * @package Drupal\quote_builder\Button
 */
class StepFinishButton extends BaseButton {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'finish';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'submit',
      '#value' => t('Finish'),
      '#goto_step' => StepsEnum::STEP_FINALIZE,
      '#submit_handler' => 'submitValues',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitHandler() {
    return 'submitIntake';
  }

}
