/**
 * @file
 */

(function ($) {

    $(document).ready(function () {
    // Configure validation
    $('#msform').validate({ // initialize plugin.
                ignore:":not(:visible)",
                rules: {
                    sp_p_property_value    : {number : true, required: true },
                    sp_p_property_postcode : {required: true },
                    sp_s_property_value       : {number : true, required: true },
                    sp_s_property_postcode : {required: true },
                    p_property_value       : {number : true, required: true },
                    p_property_postcode    : {required: true },
                    s_property_value       : {number : true, required: true },
                    s_property_postcode    : {required: true },
                    r_property_value       : {number : true, required: true },
                    r_property_postcode    : {required : true},
                    t_property_value       : {number : true, required: true },
                    t_property_postcode    : {required : true },
                    personal_fname         : {required: true},
                    personal_phone         : {required: true},
                    personal_email         : {required : true, email:true},
                },
    });

var current_fs, next_fs, previous_fs; // Fieldsets
var left, opacity, scale; // Fieldset properties which we will animate
var animating; // Flag to prevent quick multi-click glitches.
var progressPercent = 0;
var totalSteps = 0;
var percentagePerStep = 0;
var activePercent = 0;
var serviceType = "";
var deviceType = '';

if ($(window).width() <= 641) {
   deviceType = 'small';
}
else {
   deviceType = 'large';
}

// Quote spinner on loading tab.
var quotes = $(".msform-quotes");
var quoteIndex = -1;

function showNextQuote() {
    ++quoteIndex;
    quotes.eq(quoteIndex % quotes.length)
        .fadeIn(1000)
        .delay(2000)
        .fadeOut(1000, showNextQuote);
}

showNextQuote();
startJourney()

function startJourney(){

    setTimeout(function () {
        switch (getQueryParams().product) {
            case undefined: //console.log("do nothing");

                break;

            case "sale": //console.log("triggering sale");
                $("#selectSale").trigger("click");
                var serviceType = 'sale';

                break;

            case "purchase": //console.log("triggering purchase");
                $("#selectPurchase").trigger("click");

                var serviceType = 'purchase';
                break;

            case "sale_purchase": //console.log("triggering sale_purchase");
                var serviceType = 'sale_purchase';
                $("#selectSalePurchase").trigger("click");

                break;

            case "remortgage": //console.log("triggering remortgage");
                $("#selectRemortgage").trigger("click");

                var serviceType = 'remortgage';
                break;

            case "transfer": //console.log("triggering transfer");
                $("#selectTransfer").trigger("click");

                var serviceType = 'transfer';
                break;

            default:
                //console.log("do nothing default");
                break;

        }
    }, 1);

}

function getQueryParams(){
    try {
        url = window.location.href;
        query_str = url.substr(url.indexOf('?') + 1, url.length - 1);
        r_params = query_str.split('&');
        params = {}
        for (i in r_params) {
            param = r_params[i].split('=');
            params[ param[0] ] = param[1];
        }
        return params;
    }
    catch (e) {
       return {};
    }
}

var selectService = function () {

    var service = $(this).data('service');
    $('.selection-container').hide();

    activePercent += percentagePerStep;
    $('#msform .progress_title').hide();
    $('#msform .progress__percentage').show();
    $('.progress__percentage').css("width", activePercent + "%");
      $('.progress__percentage').html(activePercent + "%&nbsp;");

      current_fs = $(this).closest("fieldset");

      $("html, body").animate({scrollTop: $("#msform").offset().top - 44}, "slow");

    switch (service) {
        case "purchase":
            // setheader();
            purchaseJourney();
            break;

        case "sale":
            // setheader();
            saleJourney();
            break;

        case "sale_purchase":
            // setheader();
            salePurchaseJourney();
            break;

        case "remortgage":
            // setheader();
            remortgageJourney();
            break;

        case "transfer":
            // setheader();
            transferJourney();
            break;

        default:
    }
};

var nextClickFunc = function (event) {

        switch ($(this).attr('id')) {
            case "t_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "sp_s_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "p_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "s_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "t_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            default:
                current_fs = $(this).closest("fieldset");
                next_fs = $(this).closest('fieldset').next();
    }

    if ($(this).hasClass("allow-default-behaviour")) {
       // Allow default behaviour
       // event.preventDefault();
       // event.stopPropagation();
    }
else {
        event.preventDefault();
        event.stopPropagation();
    }

    current_fs = $(this).closest("fieldset");
    next_fs = $(this).closest('fieldset').next();

    // Fix form height to prevent content overlap.
    next_fs_height = next_fs.height();

    if (next_fs_height == null) {
        next_fs_height = 200;
    }
      $('#msform').css("height" , next_fs_height + 100);

    loadNextTab();
}

var nextKeyFunc = function (event) {

    if ($(this).hasClass("allow-default-behaviour")) {
       // Allow default behaviour.
    }
else {
        event.preventDefault();
        event.stopPropagation();
    }

    var myLoadNextTab = true;

    var x = event.keyCode;
      if (x === 10 || x === 13) {

        switch ($(this).attr('id')) {
            case "t_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "sp_s_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "p_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "s_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "r_property_postcode":
                current_fs = $(this).closest("fieldset");
                next_fs = $('#msform .fieldset-container.personal-container fieldset');
                break;

            case "personal_fname":
            case "personal_phone":
            case "personal_email":

                if ($("#msform").valid()) {
                    myLoadNextTab = false;
                    $('.personal-container fieldset').css('display','none');
                    $('#msform .fieldset-container.rquip-container fieldset').show();

                    activePercent += percentagePerStep;
                      if (activePercent >= 97) {
                        activePercent = 100;
                      }
                      updatePercentage();

                    next_fs_height = next_fs.height();
                    $('#msform').css("height" , "300px");

                    url = '/rquip_async/get_quote';

                    // Send the data using post.
                    var posting = $.post(url, $('#msform').serialize());

                    // Put the results in a div.
                    posting.done(function (data) {

                        $("#msform #rquip_async_container_extras").css('display' , 'block');
                        $("#msform #rquip_async_spinner").hide();

                        history.pushState('', 'Get A Quote', "your-quote/?my-" + serviceType + "-quote");
                        $("#rquip_async_container").empty().append(data);
                          $('#msform').css("height" , $('#msform .fieldset-container.rquip-container fieldset').height() + 100);
                    });
                };

                break;

            default:
                current_fs = $(this).closest("fieldset");
                next_fs = $(this).closest('fieldset').next();
        }

        // Fix form height to prevent content overlap.
        next_fs_height = next_fs.height();

        if (next_fs_height == null) {
            next_fs_height = 200;
        }
          $('#msform').css("height" , next_fs_height + 100);
          if (myLoadNextTab) {
              loadNextTab();
          }

      }
else {
          // Wrong key, ignore.
      }

}

var showPersonal = function showPersonalDetailsTab(){

    current_fs = $(this).closest("fieldset");

    next_fs = $('#msform .fieldset-container.personal-container fieldset');

    // Fix form height to prevent content overlap.
    next_fs_height = next_fs.height();
      $('#msform').css("height" , next_fs_height + 100);

    loadNextTab();
}

var showfinal = function (event) {

    event.preventDefault();

    if ($("#msform").valid()) {

        $('.personal-container fieldset').hide();
        $('#msform .fieldset-container.rquip-container fieldset').show();

        activePercent += percentagePerStep;
          if (activePercent >= 97) {
            activePercent = 100;
          }
          updatePercentage();

        next_fs_height = next_fs.height();
        $('#msform').css("height" , "300px");

        url = '/rquip_async/get_quote';

        // Send the data using post.
        var posting = $.post(url, $('#msform').serialize());

        // Put the results in a div.
        posting.done(function (data) {

            $("#msform #rquip_async_container_extras").css('display' , 'block');
            $("#msform #rquip_async_spinner").hide();

            history.pushState('', 'Get A Quote', "your-quote?my-" + serviceType + "-quote");

            $("#rquip_async_container").empty().append(data);

              $('#msform').css("height" , $('#msform .fieldset-container.rquip-container fieldset').height() + 100);

        });

    };

    return false;

};

$(".show-personal-details").click(showPersonal);
$("#msform input").keyup(nextKeyFunc);
$(".next").click(nextClickFunc);
$(".submit-final").click(showfinal);
$(".selectService").click(selectService);

$(".redoService").click(function () {
    $('.selection-container').show();
    $('#msform .sp_journey_head').css('display', 'none');
});

function updatePercentage(){
    $('.progress__percentage').css("width", activePercent + "%");
      $('.progress__percentage').html(activePercent + "%&nbsp;");
}

function loadNextTab(){

        if (deviceType === 'small') {
            $("html, body").animate({scrollTop: $("#msform").offset().top - 44}, "slow");
        }

          if ($("#msform").valid()) {

            if (animating) {
return false;
            }
            animating = true;

              activePercent += percentagePerStep;
              if (activePercent >= 97) {
                activePercent = 100;
              }
              updatePercentage();

            next_fs.show();
            current_fs.hide();

            if (serviceType === 'sale_purchase') {
                if (activePercent === 40) {
                    $('#msform .sp_journey_head').html('About the property you are buying');
                }
                if (activePercent === 50) {
                    $('#msform .sp_journey_head').html('About the property you are selling');
                }
                if (activePercent === 90) {
                    $('#msform .sp_journey_head').css('display', 'none');
                }
            }

            // Hide the current fieldset with style.
            current_fs.animate({opacity: 0}, {

                step: function (now, mx) {
                    // As the opacity of current_fs reduces to 0 - stored in "now"
                    // 1. scale current_fs down to 80%.
                    scale = 1 - (1 - now) * 0.2;
                    // 2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    // 3. increase opacity of next_fs to 1 as it moves in.
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});

                },
                duration: 800,
                complete: function () {

                    current_fs.hide();
                    animating = false;
              current_fs.css({'position' : 'absolute'}); // Relative.

                },
                // This comes from the custom easing plugin.
                easing: 'easeInOutBack'
            });

        }
else {
            // Form not valid.
        }

    }

$(".previous").click(function () {
    if (animating) {
return false;
    }
    animating = true;

      current_fs = $(this).closest("fieldset");
      previous_fs = $(this).closest("fieldset").prev();

    activePercent -= percentagePerStep;
      updatePercentage();

    // Show the previous fieldset.
    previous_fs.show();
    current_fs.hide();

    // Hide the current fieldset with style.
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            // As the opacity of current_fs reduces to 0 - stored in "now"
            // 1. scale previous_fs from 80% to 100%.
            scale = 0.8 + (1 - now) * 0.2;
            // 2. take current_fs to the right(50%) - from 0%.
            left = ((1 - now) * 50) + "%";
            // 3. increase opacity of previous_fs to 1 as it moves in.
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        // This comes from the custom easing plugin.
        easing: 'easeInOutBack'
    });

});

$(".previous-different-fieldset").click(function () {
    if (animating) {
return false;
    }
    animating = true;

      current_fs = $(this).closest("fieldset");
      previous_fs = $('#msform .widget-selected').closest("fieldset");

    activePercent -= percentagePerStep;
      updatePercentage();

    // Show the previous fieldset.
    previous_fs.show();
    current_fs.hide();

    // Hide the current fieldset with style.
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            // As the opacity of current_fs reduces to 0 - stored in "now"
            // 1. scale previous_fs from 80% to 100%.
            scale = 0.8 + (1 - now) * 0.2;
            // 2. take current_fs to the right(50%) - from 0%.
            left = ((1 - now) * 50) + "%";
            // 3. increase opacity of previous_fs to 1 as it moves in.
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        // This comes from the custom easing plugin.
        easing: 'easeInOutBack'
    });

});

function purchaseJourney(){

    serviceType = 'purchase';
    history.pushState('', 'Get A Quote', "quotebuilder?product=" + serviceType);

    totalSteps = 8 - 1;
    percentagePerStep = Math.round(100 / totalSteps);
    activePercent = percentagePerStep;
    updatePercentage();

    next_fs = $('#msform .fieldset-container.purchase-container fieldset');

    var myJourney = $('#msform .fieldset-container.purchase-container fieldset')
    .first()
    .show()
    .css({
        'left' : '0px',
           'opacity' : '1'
    });
}

function saleJourney(){
    serviceType = 'sale';
    history.pushState('', 'Get A Quote', "quotebuilder?product=" + serviceType);

    totalSteps = 7 - 1;
    percentagePerStep = Math.round(100 / totalSteps);
    activePercent = percentagePerStep;
    updatePercentage();

    next_fs = $('#msform .fieldset-container.sale-container fieldset');

    $('#msform .fieldset-container.sale-container fieldset')
    .first()
    .show()
    .css({
        'left' : '0px',
           'opacity' : '1'
    });
}

function salePurchaseJourney(){
    serviceType = 'sale_purchase';
    history.pushState('', 'Get A Quote', "quotebuilder?product=" + serviceType);
    totalSteps = 11 - 1;
    percentagePerStep = Math.round(100 / totalSteps);
    activePercent = percentagePerStep;
    updatePercentage();

    next_fs = $('#msform .fieldset-container.sale_purchase-container fieldset');

    $('#msform .sp_journey_head').css('display', 'block');

    $('#msform .fieldset-container.sale_purchase-container fieldset')
    .first()
    .show()
    .css({
        'left' : '0px',
           'opacity' : '1'
    });
}

function transferJourney(){
    serviceType = 'transfer';
    history.pushState('', 'Get A Quote', "quotebuilder?product=" + serviceType);
    totalSteps = 6 - 1;
    percentagePerStep = Math.round(100 / totalSteps);
    activePercent = percentagePerStep;
    updatePercentage();

    next_fs = $('#msform .fieldset-container.transfer-container fieldset');

    $('#msform .fieldset-container.transfer-container fieldset')
    .first()
    .show()
    .css({
        'left' : '0px',
           'opacity' : '1'
    });
}

function remortgageJourney(){
    serviceType = 'remortgage';
    history.pushState('', 'Get A Quote', "quotebuilder?product=" + serviceType);
    totalSteps = 5 - 1;
    percentagePerStep = Math.round(100 / totalSteps);
    activePercent = percentagePerStep;
    updatePercentage();

    next_fs = $('#msform .fieldset-container.remortgage-container fieldset');

        $('#msform .fieldset-container.remortgage-container fieldset')
        .first()
        .show()
        .css({
            'left' : '0px',
            'opacity' : '1'
        });
    }
    });
})(jQuery);
