(function ($) {
  $(document).ready(function() {
    
  $('#navigation').before('<div id="menu-burger">&#9776; Menu</div>');
  $('#menu-burger').click(function() {
    if($('#navigation li').is(':visible')) {
      $('#navigation li').hide();
    }
    else {
      $('#navigation li').show();
    }
  });
    
    var menuDrop = new Array();
    menuDrop['menu-400'] = '#basic-drop-menu-contact-wrapper';
    menuDrop['menu-431'] = '#basic-drop-menu-reviews-wrapper';
    menuDrop['menu-432'] = '#basic-drop-menu-regulate-wrapper';
    menuDrop['menu-428'] = '#basic-drop-menu-blogs-wrapper';
    menuDrop['menu-463'] = '#basic-drop-menu-about-wrapper';
    menuDrop['menu-427'] = '#basic-drop-menu-services-wrapper';
    //menuDrop['mid-contact'] = '#basic-drop-menu-contact-wrapper';
    //menuDrop['mid-reviews'] = '#basic-drop-menu-reviews-wrapper';
    //menuDrop['mid-node107'] = '#basic-drop-menu-regulate-wrapper';
    //menuDrop['mid-blogs'] = '#basic-drop-menu-blogs-wrapper';
    //menuDrop['mid-about-us'] = '#basic-drop-menu-about-wrapper';
    //menuDrop['mid-our-services'] = '#basic-drop-menu-services-wrapper';
    var menu = '';
    var dropItem = '';

      $("#navigation #primary li").hover(
          function () {
            if ($(window).width() > 959) {
              menu = $(this).attr('class').split(' ');
              dropItem = menuDrop[menu[0]];
              $(dropItem).show();
            }
          },
          function () {
            if ($(window).width() > 959) {
              $(dropItem).hide();
            }
          }
      );
      
      $(".basic-drop-menu-item-wrapper").hover(
        function () {
          if ($(window).width() > 959) {
            $(this).css('display', 'block');
          }
        },
        function () {
          if ($(window).width() > 959) {
            $(this).css('display', 'none');
          }
        }
      );
    
  });
})(jQuery);